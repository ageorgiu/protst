class AddQuickRegistrationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :quick_registration, :boolean, default: true
  end
end
