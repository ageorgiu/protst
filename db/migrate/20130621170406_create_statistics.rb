class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.integer :content_id
      t.string :requested_at
      t.string :path

      t.timestamps
    end
  end
end
