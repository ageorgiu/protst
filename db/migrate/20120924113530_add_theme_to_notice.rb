# -*- encoding : utf-8 -*-
class AddThemeToNotice < ActiveRecord::Migration
  def change
    add_column :notices, :theme, :string
  end
end
