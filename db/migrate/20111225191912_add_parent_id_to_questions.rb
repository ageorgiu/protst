# -*- encoding : utf-8 -*-
class AddParentIdToQuestions < ActiveRecord::Migration
  def self.up
    add_column :questions, :parent_id, :integer
    add_index :questions, :parent_id
  end

  def self.down
    remove_index :questions, :parent_id
    remove_column :questions, :parent_id
  end
end
