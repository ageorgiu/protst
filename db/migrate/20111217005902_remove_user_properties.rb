# -*- encoding : utf-8 -*-
class RemoveUserProperties < ActiveRecord::Migration

  class << self

    def accos_columns
      %w{city education_level job_area job_expirience job_position education_type}
    end

    def up
      drop_table :user_properties
      accos_columns.each{ |column| rename_column :users, column, "#{column}_id" }
    end

    def down
      create_table "user_properties", :force => true do |t|
        t.integer  "user_id",     :default => 0, :null => false
        t.integer  "property_id", :default => 0, :null => false
        t.datetime "created_at"
        t.datetime "updated_at"
      end
      accos_columns.each{ |column| rename_column :users, "#{column}_id", column }
    end

  end
end
