class CreateSendedNotices < ActiveRecord::Migration
  def change
    create_table :sended_notices do |t|
      t.references :user
      t.references :notice
      t.text :text

      t.timestamps
    end
    add_index :sended_notices, :user_id
    add_index :sended_notices, :notice_id
  end
end
