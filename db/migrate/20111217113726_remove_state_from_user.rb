# -*- encoding : utf-8 -*-
class RemoveStateFromUser < ActiveRecord::Migration
  def self.up
    add_column :users, :custom, :boolean
    User.reset_column_information
    User.where(:state=>'moreinfo').update_all(:custom=>true)
    remove_column :users, :state
  end

  def self.down
    add_column :users, :state, :string
    User.reset_column_information
    User.where(:custom=>true).update_all(:state=>"moreinfo")
    User.where(:custom=>false).update_all(:state=>'registered')
    remove_column :users, :custom
  end
end
