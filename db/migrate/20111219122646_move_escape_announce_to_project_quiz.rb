# -*- encoding : utf-8 -*-
class MoveEscapeAnnounceToProjectQuiz < ActiveRecord::Migration
  def self.up
    add_column :project_quizzes, :escape_announce, :boolean
    remove_column :quizzes, :escape_anons
  end

  def self.down
    add_column :quizzes, :escape_anons, :boolean
    remove_column :project_quizzes, :escape_announce
  end
end
