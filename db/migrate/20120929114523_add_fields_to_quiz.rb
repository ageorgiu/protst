class AddFieldsToQuiz < ActiveRecord::Migration
  def change
    add_column :quizzes, :selection, :string
    add_column :quizzes, :skip_instruction, :boolean
    add_column :quizzes, :anons, :text
    add_column :quizzes, :instructions, :text
    add_column :quizzes, :after_text, :text
  end
end
