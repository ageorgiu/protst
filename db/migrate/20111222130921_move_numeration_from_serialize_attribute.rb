# -*- encoding : utf-8 -*-
class MoveNumerationFromSerializeAttribute < ActiveRecord::Migration
  def self.up
    add_column :questions, :numeration, :string
    Questions::Question.reset_column_information
    Questions::Question.answerable.find_each do |question|
      question.numeration = question.content.delete(:numeration)
      question.update_record_without_timestamping
    end
  end

  def self.down
    remove_column :questions, :numeration, :string
  end
end
