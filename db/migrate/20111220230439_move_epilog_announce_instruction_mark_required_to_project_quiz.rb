# -*- encoding : utf-8 -*-
class MoveEpilogAnnounceInstructionMarkRequiredToProjectQuiz < ActiveRecord::Migration
  def self.up
    add_column :project_quizzes, :announce, :text
    add_column :project_quizzes, :instruction, :text
    add_column :project_quizzes, :mark_required, :boolean
    add_column :project_quizzes, :epilog, :text
    ProjectQuiz.reset_column_information
    ProjectQuiz.includes(:quiz).each do |project_quiz|
      project_quiz.announce = project_quiz.quiz.announce
      project_quiz.instruction = project_quiz.quiz.instruction
      project_quiz.mark_required = project_quiz.quiz.mark_required
      project_quiz.epilog = project_quiz.quiz.epilog
      project_quiz.update_record_without_timestamping
    end
    remove_column :quizzes, :announce
    remove_column :quizzes, :instruction
    remove_column :quizzes, :mark_required
    remove_column :quizzes, :epilog
  end

  def self.down
    remove_column :project_quizzes, :announce
    remove_column :project_quizzes, :instruction
    remove_column :project_quizzes, :mark_required
    remove_column :project_quizzes, :epilog
    add_column :quizzes, :announce, :text
    add_column :quizzes, :instruction, :text
    add_column :quizzes, :mark_required, :boolean
    add_column :quizzes, :epilog, :text
  end
end
