# -*- encoding : utf-8 -*-
class RemoveRawFromAnswer < ActiveRecord::Migration
  def self.up
  	add_column :answers, :deleted_at, :time	
    Answer.update_all("content=raw", "content is null and raw is not null")
    remove_column :answers, :raw
  end

  def self.down
    add_column :answers, :raw, :integer
  end
end
