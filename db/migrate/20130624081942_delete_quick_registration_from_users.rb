class DeleteQuickRegistrationFromUsers < ActiveRecord::Migration
  def up
  	remove_column :users, :quick_registration
  	add_column :users, :quick_registration, :boolean, default: false
  end

  def down
  end
end
