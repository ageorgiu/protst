class AddBufferedToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :buffered, :boolean, default: false
  end
end
