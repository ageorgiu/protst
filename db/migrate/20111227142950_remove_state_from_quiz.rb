# -*- encoding : utf-8 -*-
class RemoveStateFromQuiz < ActiveRecord::Migration
  def self.up
    add_column :quizzes, :calculating, :boolean, :default=>true
    Quiz.reset_column_information
    Quiz.where(:state=>'calculating').update_all(:calculating=>true)
    remove_column :quizzes, :state
  end

  def self.down
    add_column :quizzes, :state, :string, :default=>"calculating"
    Quiz.reset_column_information
    Quiz.where("calculating != true").update_all(:state=>"fixed")
    remove_column :quizzes, :calculating
  end
end
