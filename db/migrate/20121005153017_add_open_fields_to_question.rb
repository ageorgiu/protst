class AddOpenFieldsToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :inverse, :boolean    
    add_column :questions, :size, :string
  end
end
