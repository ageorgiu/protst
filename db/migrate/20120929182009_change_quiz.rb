class ChangeQuiz < ActiveRecord::Migration
  def up
  	remove_column :quizzes, :selection
    remove_column :quizzes, :skip_instruction
    remove_column :quizzes, :anons
    remove_column :quizzes, :instructions
    remove_column :quizzes, :after_text
    add_column :quizzes, :mark_required, :boolean
    add_column :quizzes, :escape_anons, :boolean
    add_column :quizzes, :announce, :text
    add_column :quizzes, :instruction, :text
    add_column :quizzes, :epilog, :text
    add_column :quizzes, :fixed_values, :boolean
    add_column :quizzes, :custom_paging, :boolean
    add_column :quizzes, :limit, :integer
  end

  def down
  end
end
