# -*- encoding : utf-8 -*-
class RemoveTypeFromQuiz < ActiveRecord::Migration
  def self.up
    remove_column :quizzes, :kind
    rename_column :quizzes, :type, :kind
  end

  def self.down
    rename_column :quizzes, :kind, :type
    add_column :quizzes, :kind, :string, :default=>"0"
  end
end
