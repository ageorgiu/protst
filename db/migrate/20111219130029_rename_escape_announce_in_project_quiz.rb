# -*- encoding : utf-8 -*-
class RenameEscapeAnnounceInProjectQuiz < ActiveRecord::Migration
  def self.up
    rename_column :project_quizzes, :escape_announce, :escape_instruction
  end

  def self.down
    rename_column :project_quizzes, :escape_instruction, :escape_announce
  end
end
