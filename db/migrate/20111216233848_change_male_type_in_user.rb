# -*- encoding : utf-8 -*-
class ChangeMaleTypeInUser < ActiveRecord::Migration
  def self.up
    add_column :users, :gender, :string
    User.reset_column_information
    User.where(:sex=>0).update_all(:gender=>'none')
    User.where(:sex=>1).update_all(:gender=>'male')
    User.where(:sex=>2).update_all(:gender=>'female')
    remove_column :users, :sex
  end

  def self.down
    add_column :users, :sex, :integer
    User.reset_column_information
    User.where(:gender=>'none').update_all(:sex=>0)
    User.where(:gender=>'male').update_all(:sex=>1)
    User.where(:gender=>'female').update_all(:sex=>2)
    remove_column :users, :gender
  end
end
