# -*- encoding : utf-8 -*-
class RenameLimitInQuiz < ActiveRecord::Migration
  def self.up
    rename_column :quizzes, :limit, :limited_in_time
  end

  def self.down
    rename_column :quizzes, :limited_in_time, :limit
  end
end
