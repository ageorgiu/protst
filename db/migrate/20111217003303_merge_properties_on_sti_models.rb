# -*- encoding : utf-8 -*-
class MergePropertiesOnStiModels < ActiveRecord::Migration
  
  class << self

    def property_types
      %w{EducationLevel EducationType JobArea JobExpirience City JobPosition}
    end

    def up
      add_column :properties, :type, :string
      property_types.each_with_index do |class_name, index|
        Property.where(:role=>(index+1)).update_all("type='#{class_name}'")
      end
      remove_column :properties, :role
    end

    def down
      add_column :properties, :role, :integer
      property_types.each_with_index do |class_name, index|
        Property.where(:type=>class_name).update_all("role=#{index+1}")
      end
    end

  end
end
