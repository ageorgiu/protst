# -*- encoding : utf-8 -*-
class CorrectNamesInIntepretation < ActiveRecord::Migration
  def self.up
    rename_column :interpretations, :stan_from, :sten_from
    rename_column :interpretations, :stan_to, :sten_to
  end

  def self.down
    rename_column :interpretations, :sten_to, :stan_to
    rename_column :interpretations, :sten_from, :stan_from
  end
end
