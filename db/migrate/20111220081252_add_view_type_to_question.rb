# -*- encoding : utf-8 -*-
class AddViewTypeToQuestion < ActiveRecord::Migration
  def self.up
    add_column :questions, :view_type, :string
  end

  def self.down
    remove_column :questions, :view_type
  end
end
