# -*- encoding : utf-8 -*-
class AddViewToAnswer < ActiveRecord::Migration
  def self.up
    add_column :answers, :view, :text
  end

  def self.down
    remove_column :answers, :view
  end
end
