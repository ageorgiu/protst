# -*- encoding : utf-8 -*-
class AddValueToProfileCharacteristic < ActiveRecord::Migration
  def self.up
    add_column :profile_charachteristics, :value, :integer, :default => 9
  end

  def self.down
    remove_column :profile_charachteristics, :value
  end
end
