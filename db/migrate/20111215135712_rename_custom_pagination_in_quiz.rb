# -*- encoding : utf-8 -*-
class RenameCustomPaginationInQuiz < ActiveRecord::Migration
  def self.up
    rename_column :quizzes, :custom_paging, :custom_pagination
  end

  def self.down
    rename_column :quizzes, :custom_pagination, :custom_paging
  end
end
