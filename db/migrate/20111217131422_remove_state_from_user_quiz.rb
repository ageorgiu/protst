# -*- encoding : utf-8 -*-
class RemoveStateFromUserQuiz < ActiveRecord::Migration
  def self.up
    add_column :user_quizzes, :deleted_at, :time
    add_column :user_quizzes, :complete, :boolean
    UserQuiz.reset_column_information
    UserQuiz.where(:state=>'complete').update_all(:complete=>true)
    remove_column :user_quizzes, :state
  end

  def self.down
    add_column :user_quizzes, :state, :string
    UserQuiz.reset_column_information
    UserQuiz.update_all(:state=>'available')
    UserQuiz.where(:complete=>true).update_all(:state=>'complete')
    remove_column :user_quizzes, :complete
  end
end
