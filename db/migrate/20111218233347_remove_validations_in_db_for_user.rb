# -*- encoding : utf-8 -*-
class RemoveValidationsInDbForUser < ActiveRecord::Migration
  def self.up
    change_column :users, :login, :string, :null=>true
    change_column :users, :password, :string, :null=>true
    change_column :users, :project_id, :integer, :null=>true
    change_column :users, :name, :string, :null=>true
    change_column :users, :surname, :string, :null=>true
    change_column :users, :email, :string, :null=>true
    change_column :users, :phone, :string, :null=>true
    change_column :users, :job_area_id, :integer, :null=>true
    change_column :users, :restricted, :boolean, :null=>true
    change_column :users, :job_expirience_id, :integer, :null=>true
    change_column :users, :job_position_id, :integer, :null=>true
    change_column :users, :city_id, :integer, :null=>true
    change_column :users, :education_level_id, :integer, :null=>true
    change_column :users, :education_type_id, :integer, :null=>true
  end

  def self.down
  end
end
