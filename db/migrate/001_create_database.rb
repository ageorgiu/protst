# -*- encoding : utf-8 -*-
class CreateDatabase < ActiveRecord::Migration
  def self.up
    create_table "answers", :force => true do |t|
      t.integer  "user_id",     :default => 0, :null => false
      t.integer  "question_id", :default => 0, :null => false
      t.integer  "value"
      t.text     "content"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "raw",         :default => 0
    end

    add_index "answers", ["question_id"], :name => "index_answers_on_question_id"
    add_index "answers", ["user_id"], :name => "index_answers_on_user_id"

    create_table "contexts", :force => true do |t|
      t.integer  "quiz_id",    :default => 0, :null => false
      t.string   "title",                     :null => false
      t.text     "content"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "factors", :force => true do |t|
      t.integer  "quiz_id",    :default => 0,   :null => false
      t.string   "title",                       :null => false
      t.integer  "parent_id",  :default => 0,   :null => false
      t.integer  "lft",        :default => 0,   :null => false
      t.integer  "rgt",        :default => 0,   :null => false
      t.integer  "kind"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.float    "mean",       :default => 0.0, :null => false
      t.float    "variance",   :default => 0.0, :null => false
      t.integer  "weight",     :default => 0,   :null => false
    end

    create_table "interpretations", :force => true do |t|
      t.integer  "factor_id",  :default => 0, :null => false
      t.integer  "context_id", :default => 0, :null => false
      t.integer  "stan_from",  :default => 0, :null => false
      t.integer  "stan_to",    :default => 0, :null => false
      t.text     "content"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "notices", :force => true do |t|
      t.integer  "owner_id",   :default => 0, :null => false
      t.string   "title",                     :null => false
      t.text     "content"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "pages", :force => true do |t|
      t.string   "title",                        :null => false
      t.integer  "weight",     :default => 0,    :null => false
      t.integer  "parent_id",  :default => 0,    :null => false
      t.string   "special",                      :null => false
      t.boolean  "visible",    :default => true, :null => false
      t.text     "content"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "profile_charachteristics", :force => true do |t|
      t.integer  "quiz_id"
      t.integer  "min_value"
      t.integer  "max_value"
      t.text     "interpretation"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "profile_charachteristics", ["quiz_id"], :name => "index_profile_charachteristics_on_quiz_id"

    create_table "project_quizzes", :force => true do |t|
      t.integer  "project_id"
      t.integer  "quiz_id"
      t.integer  "context_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "show_report"
    end

    add_index "project_quizzes", ["project_id", "quiz_id", "context_id"], :name => "index_project_quizzes_on_project_id_and_quiz_id_and_context_id"

    create_table "projects", :force => true do |t|
      t.string   "title",                           :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "allow_anonim"
      t.boolean  "default",      :default => false
      t.text     "style"
    end

    create_table "properties", :force => true do |t|
      t.string   "title",                     :null => false
      t.integer  "role",       :default => 0, :null => false
      t.integer  "weight",     :default => 0, :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "question_factors", :force => true do |t|
      t.integer  "question_id", :default => 1
      t.integer  "factor_id",   :default => 1
      t.integer  "role",        :default => 1, :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "question_factors", ["factor_id"], :name => "index_question_factors_on_factor_id"
    add_index "question_factors", ["question_id"], :name => "index_question_factors_on_question_id"

    create_table "questions", :force => true do |t|
      t.string   "type",                           :null => false
      t.integer  "parent_id",   :default => 0,     :null => false
      t.integer  "weight",      :default => 0,     :null => false
      t.integer  "quiz_id",     :default => 0,     :null => false
      t.text     "content"
      t.text     "comment"
      t.text     "instruction"
      t.text     "description"
      t.integer  "value"
      t.string   "title",       :default => "0",   :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "inverse",     :default => false, :null => false
      t.boolean  "required",    :default => true
      t.boolean  "profile",     :default => false
    end

    add_index "questions", ["quiz_id"], :name => "index_questions_on_quiz_id"

    create_table "questions_instructions", :force => true do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "questions_levels", :force => true do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "questions_variants", :force => true do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "quizzes", :force => true do |t|
      t.string   "title",                                :null => false
      t.string   "kind",              :default => "0",   :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.text     "announce"
      t.text     "instruction"
      t.string   "state",                                :null => false
      t.boolean  "custom_paging",     :default => false, :null => false
      t.integer  "page_size",         :default => 5,     :null => false
      t.boolean  "forward"
      t.boolean  "mark_required"
      t.text     "epilog"
      t.string   "type"
      t.integer  "limit"
      t.boolean  "custom_numeration"
      t.boolean  "escape_anons"
    end

    create_table "sessions", :force => true do |t|
      t.string   "session_id", :null => false
      t.text     "data"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
    add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

    create_table "settings", :force => true do |t|
      t.string   "title",                                   :null => false
      t.string   "setting",                                 :null => false
      t.text     "comment"
      t.text     "value"
      t.string   "special",           :default => "string", :null => false
      t.string   "data_file_name"
      t.string   "data_content_type"
      t.integer  "data_file_size"
      t.datetime "data_updates_at"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "tests", :force => true do |t|
      t.string   "title",                       :null => false
      t.string   "kind",       :default => "0", :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "user_properties", :force => true do |t|
      t.integer  "user_id",     :default => 0, :null => false
      t.integer  "property_id", :default => 0, :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "user_properties", ["property_id"], :name => "index_user_properties_on_property_id"
    add_index "user_properties", ["user_id"], :name => "index_user_properties_on_user_id"

    create_table "user_quizzes", :force => true do |t|
      t.integer  "user_id",    :default => 0, :null => false
      t.integer  "quiz_id",    :default => 0, :null => false
      t.string   "state",                     :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "users", :force => true do |t|
      t.string   "login",                              :null => false
      t.string   "password",                           :null => false
      t.integer  "project_id",      :default => 0,     :null => false
      t.string   "name",                               :null => false
      t.string   "surname",                            :null => false
      t.string   "patronymic",                         :null => false
      t.integer  "sex",             :default => 0,     :null => false
      t.date     "birthday"
      t.string   "email",                              :null => false
      t.string   "phone",                              :null => false
      t.string   "address",                            :null => false
      t.integer  "job_area",        :default => 0,     :null => false
      t.integer  "job_expirience",  :default => 0,     :null => false
      t.integer  "job_position",    :default => 0,     :null => false
      t.integer  "city",            :default => 0,     :null => false
      t.integer  "education_level", :default => 0,     :null => false
      t.integer  "education_type",  :default => 0,     :null => false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "restricted",      :default => false, :null => false
      t.string   "state",                              :null => false
    end
  end

  def self.down
  end
end
