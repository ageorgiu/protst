# -*- encoding : utf-8 -*-
class CleanQuestionTable < ActiveRecord::Migration
  def self.up
    remove_column :questions, :parent_id
    remove_column :questions, :comment
    remove_column :questions, :description
    remove_column :questions, :value
    remove_column :questions, :title
    remove_column :questions, :inverse
  end

  def self.down
    add_column :questions, :parent_id, :integer
    add_column :questions, :comment, :text
    add_column :questions, :description, :text
    add_column :questions, :value, :integer
    add_column :questions, :title, :string
    add_column :questions, :inverse, :boolean
  end
end
