# -*- encoding : utf-8 -*-
class ChangeContextToReport < ActiveRecord::Migration
  def self.up
    rename_table :contexts, :reports
    rename_column :interpretations, :context_id, :report_id
    rename_column :project_quizzes, :context_id, :report_id
  end

  def self.down
    rename_table :reports, :contexts
    rename_column :interpretations, :report_id, :context_id
    rename_column :project_quizzes, :report_id, :context_id
  end
end
