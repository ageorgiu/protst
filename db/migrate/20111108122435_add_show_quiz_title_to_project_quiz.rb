# -*- encoding : utf-8 -*-
class AddShowQuizTitleToProjectQuiz < ActiveRecord::Migration
  def self.up
    add_column :project_quizzes, :show_quiz_title, :boolean
  end

  def self.down
    remove_column :project_quizzes, :show_quiz_title
  end
end
