# -*- encoding : utf-8 -*-
class AddProjectReferenceToNotice < ActiveRecord::Migration
  def change
    add_column :notices, :project_id, :integer
  end
end
