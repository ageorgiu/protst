# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130624081942) do

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "answers", :force => true do |t|
    t.integer  "user_id",     :default => 0, :null => false
    t.integer  "question_id", :default => 0, :null => false
    t.integer  "value"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.time     "deleted_at"
    t.text     "view"
  end

  add_index "answers", ["question_id"], :name => "index_answers_on_question_id"
  add_index "answers", ["user_id"], :name => "index_answers_on_user_id"

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "contexts", :force => true do |t|
    t.integer  "quiz_id",    :default => 0, :null => false
    t.string   "title",                     :null => false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "factors", :force => true do |t|
    t.integer  "quiz_id",    :default => 0,   :null => false
    t.string   "title",                       :null => false
    t.integer  "parent_id",  :default => 0,   :null => false
    t.integer  "lft",        :default => 0,   :null => false
    t.integer  "rgt",        :default => 0,   :null => false
    t.integer  "kind"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "mean",       :default => 0.0, :null => false
    t.float    "variance",   :default => 0.0, :null => false
    t.integer  "weight",     :default => 0,   :null => false
  end

  create_table "interpretations", :force => true do |t|
    t.integer  "factor_id",  :default => 0, :null => false
    t.integer  "report_id",  :default => 0, :null => false
    t.integer  "sten_from",  :default => 0, :null => false
    t.integer  "sten_to",    :default => 0, :null => false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notices", :force => true do |t|
    t.integer  "owner_id",   :default => 0, :null => false
    t.string   "title",                     :null => false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "theme"
    t.integer  "project_id"
  end

  create_table "pages", :force => true do |t|
    t.string   "title",                        :null => false
    t.integer  "weight",     :default => 0,    :null => false
    t.integer  "parent_id",  :default => 0,    :null => false
    t.string   "special",                      :null => false
    t.boolean  "visible",    :default => true, :null => false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profile_charachteristics", :force => true do |t|
    t.integer  "quiz_id"
    t.integer  "min_value"
    t.integer  "max_value"
    t.text     "interpretation"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "value",          :default => 9
  end

  add_index "profile_charachteristics", ["quiz_id"], :name => "index_profile_charachteristics_on_quiz_id"

  create_table "project_quizzes", :force => true do |t|
    t.integer  "project_id"
    t.integer  "quiz_id"
    t.integer  "report_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "show_report"
    t.boolean  "show_quiz_title"
    t.boolean  "escape_instruction"
    t.text     "announce"
    t.text     "instruction"
    t.boolean  "mark_required"
    t.text     "epilog"
  end

  add_index "project_quizzes", ["project_id", "quiz_id", "report_id"], :name => "index_project_quizzes_on_project_id_and_quiz_id_and_context_id"

  create_table "projects", :force => true do |t|
    t.string   "title",                           :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "allow_anonim"
    t.boolean  "default",      :default => false
    t.text     "style"
  end

  create_table "properties", :force => true do |t|
    t.string   "title",                     :null => false
    t.integer  "weight",     :default => 0, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
  end

  create_table "question_factors", :force => true do |t|
    t.integer  "question_id", :default => 1
    t.integer  "factor_id",   :default => 1
    t.integer  "role",        :default => 1, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "question_factors", ["factor_id"], :name => "index_question_factors_on_factor_id"
  add_index "question_factors", ["question_id"], :name => "index_question_factors_on_question_id"

  create_table "questions", :force => true do |t|
    t.string   "type",                           :null => false
    t.integer  "weight",      :default => 0,     :null => false
    t.integer  "quiz_id",     :default => 0,     :null => false
    t.text     "content"
    t.text     "instruction"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "required",    :default => true
    t.boolean  "profile",     :default => false
    t.string   "view_type"
    t.string   "numeration"
    t.integer  "parent_id"
    t.boolean  "buffered",    :default => false
    t.boolean  "inverse"
    t.string   "size"
  end

  add_index "questions", ["parent_id"], :name => "index_questions_on_parent_id"
  add_index "questions", ["quiz_id"], :name => "index_questions_on_quiz_id"

  create_table "questions_instructions", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "questions_levels", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "questions_variants", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quizzes", :force => true do |t|
    t.string   "title",                                :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "custom_pagination", :default => false, :null => false
    t.integer  "page_size",         :default => 5,     :null => false
    t.boolean  "forward"
    t.string   "kind"
    t.integer  "limited_in_time"
    t.boolean  "custom_numeration"
    t.boolean  "calculating",       :default => true
    t.boolean  "mark_required"
    t.boolean  "escape_anons"
    t.text     "announce"
    t.text     "instruction"
    t.text     "epilog"
    t.boolean  "fixed_values"
    t.boolean  "custom_paging"
    t.integer  "limit"
  end

  create_table "reports", :force => true do |t|
    t.integer  "quiz_id",    :default => 0, :null => false
    t.string   "title",                     :null => false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sended_notices", :force => true do |t|
    t.integer  "user_id"
    t.integer  "notice_id"
    t.text     "text"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "project_id"
  end

  add_index "sended_notices", ["notice_id"], :name => "index_sended_notices_on_notice_id"
  add_index "sended_notices", ["user_id"], :name => "index_sended_notices_on_user_id"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "settings", :force => true do |t|
    t.string   "title",                                   :null => false
    t.string   "setting",                                 :null => false
    t.text     "comment"
    t.text     "value"
    t.string   "special",           :default => "string", :null => false
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.datetime "data_updates_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statistics", :force => true do |t|
    t.integer  "content_id"
    t.string   "requested_at"
    t.string   "path"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "tests", :force => true do |t|
    t.string   "title",                       :null => false
    t.string   "kind",       :default => "0", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_quizzes", :force => true do |t|
    t.integer  "user_id",    :default => 0, :null => false
    t.integer  "quiz_id",    :default => 0, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.time     "deleted_at"
    t.boolean  "complete"
  end

  create_table "users", :force => true do |t|
    t.string   "login"
    t.string   "password"
    t.integer  "project_id",         :default => 0
    t.string   "name"
    t.string   "surname"
    t.string   "patronymic",                            :null => false
    t.date     "birthday"
    t.string   "email"
    t.string   "phone"
    t.integer  "job_area_id",        :default => 0
    t.integer  "job_expirience_id",  :default => 0
    t.integer  "job_position_id",    :default => 0
    t.integer  "city_id",            :default => 0
    t.integer  "education_level_id", :default => 0
    t.integer  "education_type_id",  :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "restricted",         :default => false
    t.string   "gender"
    t.boolean  "custom"
    t.integer  "role"
    t.boolean  "quick_registration", :default => false
  end

end
