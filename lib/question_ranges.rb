# -*- encoding : utf-8 -*-
module QuestionRanges

  def humanize_answer(answer_ids=nil)
    answer_ids = answer_ids.to_s.split ','
    if answer_ids.present?
      answer_ids.map{ |id| variants[id.to_i]}.join(AppConfig.variant_splitter)
    else
      super(answer_ids)
    end
  end
  
end
