# -*- encoding : utf-8 -*-
module NoticeRenderer
	class Template < Mustache

		def set_user(user)
			@user = user
		end

		def full_name
			"#{@user.surname} #{@user.name} #{@user.patronymic}"
		end

		def fore_middle_name
			"#{@user.name}, #{@user.patronymic}"
		end

		def created_at
			created = @user.created_at || Time.now
			created.strftime("%Y-%m-%d %H:%M")
		end

		def project_name
			@user.project.nil? ? "ПРОЕКТ 1" : @user.project.title
		end

		def password
			"#{@user.password}"
		end

		def login
			"#{@user.login}"
		end

		def sign_in_link(value)
			"<a href='http://prof-career.ru/process/index/#{@user.id}?key=#{@user.key}'>#{value}</a>"
		end

	end
end