# -*- encoding : utf-8 -*-
#encoding: utf-8
module QuizKinds

  def self.included(quiz_class)

    quiz_class.class_eval do

      class << self

        def kinds
          {
            'test'=>'Психометрический тест',
            'interview'=>'Опрос',
            'belbin'=>"Психометрический тест (ранг = фактор)"
          }
        end

        def kind_values
          kinds.keys
        end

        def kinds_for_select
          kinds.map{|kind, view| [view, kind]}
        end

      end

      kind_values.each do |quiz_kind|
        define_method("#{quiz_kind}?") do
          self.kind == quiz_kind
        end

        define_method("allowed_questions") do
          case kind
          when 'belbin'
            [Questions::Divider, Questions::Instruction, Questions::BelbinRange, Questions::BelbinItem]
          else
            [Questions::Divider, Questions::Instruction, Questions::Variant,
              Questions::Scale, Questions::Open, Questions::Range]
          end
        end
      end

    end

  end

end
