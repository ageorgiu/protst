require 'spreadsheet'
class ExportQuizzesJob < Struct.new(:quiz_id, :statistic_id)
	def perform
		# statistic = Statistic.create(content_id: quiz_id)
		time = Time.now
		path = "public/reports/quizzes_all_projects_#{time.strftime("%Y_%m_%d_%H_%M")}.xls"
		url = "/reports/quizzes_all_projects_#{time.strftime("%Y_%m_%d_%H_%M")}.xls"
		book = Spreadsheet::Workbook.new

		quiz = Quiz.find(quiz_id)

		quiz.projects.each do |project|
			index = 0
			headers = ["#{I18n.t 'activerecord.attributes.user.surname'}",
				"#{I18n.t 'activerecord.attributes.user.name'}",
				"#{I18n.t 'activerecord.attributes.user.patronymic'}",
				"Email",
				"#{I18n.t 'activerecord.attributes.user.login'}",
				"#{I18n.t 'activerecord.attributes.user.password'}",
				"#{I18n.t 'activerecord.attributes.user.project'}",
				"#{I18n.t 'activerecord.attributes.user.gender'}",
				"#{I18n.t 'activerecord.attributes.user.birthday'}",
				"#{I18n.t 'activerecord.attributes.user.education_level'}",
				"#{I18n.t 'activerecord.attributes.user.education_type'}",
				"#{I18n.t 'activerecord.attributes.user.job_position'}",
				"#{I18n.t 'activerecord.attributes.user.job_area'}",
				"#{I18n.t 'activerecord.attributes.user.job_expirience'}",
				"#{I18n.t 'activerecord.attributes.user.city'}",
				"#{I18n.t 'activerecord.attributes.user.phone'}",
				"#{I18n.t 'activerecord.attributes.user.data'}"]
		  headers_count = headers.count
			worksheet = book.create_worksheet name: "#{project.title}"

			if quiz.belbin?
			  belbin_headers = []
			  belbin_questions = []
			  belbin_factors = []
			  quiz.answerable_questions.each_with_index.map do |question, q_index|
			    belbin_headers   << Array.new(question.belbin_items.count, "#{q_index+1}.#{question.instruction}")
			    belbin_questions << question.belbin_items.map { |item| item.instruction }
			    belbin_factors   << question.belbin_items.map { |item| item.factor.title }
			  end
			  
			  
			  headers += belbin_headers.flatten
			  worksheet.row(index).concat headers
			  index += 1
			  
			  
			  belbin_questions_headers = (Array.new(headers_count, "") + belbin_questions).flatten
			  worksheet.row(index).concat belbin_questions_headers
			  index += 1
			  
			  belbin_factors_headers = (Array.new(headers_count, "") + belbin_factors).flatten
			  worksheet.row(index).concat belbin_factors_headers
			else
			  headers += quiz.answerable_questions.values_of(:instruction).each_with_index.map{|title, q_index| "#{q_index+1}.#{title}".gsub(/[\n\r]/, "")}
			  worksheet.row(index).concat headers
			end

			project.users.find_each do |user|
				user_quiz = UserQuiz.get(user, quiz)

				if user_quiz and user_quiz.complete?
				  row = [
				    user.surname,
				    user.name,
				    user.patronymic,
				    user.email,
				    user.login,
				    user.password,
				    user.project_title,
				    user.humanized_gender,
				    user.birthday,
				    user.education_level_title,
				    user.education_type_title,
				    user.job_position_title,
				    user.job_area_title,
				    user.job_expirience_title,
				    user.city_title,           
				    user.phone,
				    user_quiz.updated_at.to_s
				  ]         
				  user_info_size = row.count
				   
				  if quiz.belbin?
				    row += quiz.answerable_questions.map do |answerable_question|
				      if answer = answerable_question.answer_for(user)
				        answer.belbin_points_array.compact
				      else
				        "-"
				      end
				    end
				    
				    worksheet.row(index+1).concat row.flatten
				    index += 1
				  else
				    row += quiz.answerable_questions.map do |answerable_question|
				      if answer = answerable_question.answer_for(user)
				        answer.view
				      else
				        "-"
				      end
				    end
				    
				    worksheet.row(index+1).concat row.flatten
				    index += 1
				  end
				end
			end


		end

		book.write path
		statistic = Statistic.find statistic_id
		statistic.update_attributes path: url

		# statistic = Statistic.new(content_id: quiz_id, path: url, requested_at: time.to_s)
		# statistic.save!
	end
end