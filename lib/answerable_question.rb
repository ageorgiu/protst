# -*- encoding : utf-8 -*-
module AnswerableQuestion

  def answer_for(user)
    answers.find_by_user_id(user)
  end

  def variance

    uid_list  = quiz.user_quizzes.completed.values_of(:user_id)
    values    = answers.scored.where(:user_id=>uid_list).values_of(:value)

    Calculation.mean_and_variance(values).last
  end

  def generate_answer(user, answer = nil)
    # binding.pry     
    answer_g = answer
    value_hash = {:content=>answer_g, :value=>score_answer(answer_g), :view=>humanize_answer(answer_g)}
    if answer = answer_for(user)
      answer.update_attributes(value_hash)
    else
      answer = answers.create(value_hash.merge(:user=>user))
    end
    # binding.pry
    answer.view = answer.question.type == "Questions::BelbinItem" || answer.question.type == "Questions::BelbinRange" ? answer.question.humanize_answer(answer_g) : answer.question.content[:variants][answer_g.to_i-1][:value]
    answer.save
    answer
  end

  def required
    profile? or super
  end

  def humanize_answer(answer)    
    answer.present? ? answer : "Не успел ответить"
  end

  def score_answer(answer);end
end
