# -*- encoding : utf-8 -*-
#encoding: utf-8
module AppConfig
  
  class << self
    def setting
      return @setting if @setting
      raw_config = File.read("#{Rails.root}/config/app_config.yml")
      @setting = YAML.load(raw_config)[Rails.env].symbolize_keys
    end

    def scale_parity
      {1  => 'Нечетная', 2  => 'Четная'}
    end

    def variant_splitter
      "\n"
    end

    def start_year
      1940
    end

    def weight_splitter
      '%'
    end

    def weight_regexp
      Regexp.new("#{weight_splitter}\\d+$")
    end

    def variant_view
      {'radio'=>"Кнопки", 'list'=>"Список"}
    end

    def user_genders
      %w{male female}
    end

  end

end
