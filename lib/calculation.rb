# -*- encoding : utf-8 -*-
module Calculation

  class << self

    def mean_and_variance(values)

      if values.size > 0
        mean = values.sum.to_f/values.size
      else
        mean = 0
      end

      if values.size > 1
        # variance = Math.sqrt(values.map{ |v| (v-self.mean)**2 }.instance_eval{ inject { |s,e| s + e } } / (users.size - 1) )
        variance = Math.sqrt(values.map{ |value| (value-mean)**2 }.sum.to_f / values.size)
      else
        variance = 0
      end
      # binding.pry
      [mean, variance]
    end

    def zvalue(value, variance, mean)
      variance == 0 ? 0 : (value - mean)/variance
    end

    def sten(zvalue)
      sten = zvalue*2 + 5.5
      sten = 1 if sten < 1
      sten = 10 if sten > 10

      sten
    end

    def alpha(questions_count, answer_variances, variance)
      return 0 if questions_count < 2
      (questions_count.to_f/(questions_count - 1))*(1-answer_variances/(variance ** 2))
    end

  end

end
