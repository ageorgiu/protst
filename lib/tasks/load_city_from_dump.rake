#encoding: utf-8
namespace :dump do

  desc "load from mysql's dump cities"
  task :load_cities=>:environment do
    File.open('db/dump.sql') do |file|
      count = 0
      while(line=file.gets) do
        Property.create_city(line[/[А-Яа-я]+/])
        count +=1
        puts "#{count} uploaded" if (count%500==0)
      end
      puts "#{count} uploaded."
    end
  end

end