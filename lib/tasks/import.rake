require "csv"

namespace :import do
  task :answers => :environment do
    puts "It works!"
    CSV.foreach("answers.csv", headers: true) do |row|
      @update = row.to_hash
      
      @new = Hash.new

      @new["id"] = @update["id"]
      @new["user_id"] = @update["user_id"]
      @new["question_id"] = @update["question_id"]
      @new["value"] = @update["value"]
      @new["content"] = @update["raw"]
      @new["created_at"] = @update["created_at"]
      @new["updated_at"] = @update["updated_at"]
      @new["deleted_at"] = @update["deleted_at"]
      @new["view"] = "Some value..."
      
      @answer = Answer.new(@new)
      @answer.save

      # puts "#{@new}, "
    end
  end
end