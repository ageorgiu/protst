namespace :update_struct do
	task :change_type => [:environment] do
   	Property.where(:type => "JobExpirience").update_all("type"=>"temp")
    Property.where(:type => "JobArea").update_all("type"=>"JobExpirience")
	 	Property.where(:type =>"temp").update_all("type"=>"JobArea")
  end

  task :change_view => [:environment] do
  	Questions::Question.all.each do |question|
  		question.view_type = question.view_type == "radio" ? 2 : 1
  		question.save
  		puts "#{question.view_type}"
  	end
  end

  task :fix_answer_view => [:environment] do
    #TODO change elaborate_value on Answer model before doing this task
    Answer.all.each do |answer|
      # question = Questions::Question.find(answer.question_id)
      # view = question.content[:variants][answer.content.to_i-1][:value]
      view = answer.question.content[:variants][answer.content.to_i-1][:value]

      answer.update_attributes(view: view)
      # answer.update
    end
  end
end