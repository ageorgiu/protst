namespace :updates do
  task :new_spliter_for_variant, [:old_spliter] => [:environment] do |t, args|
    if args[:old_spliter].present?
      old_spliter = (args[:old_spliter] == 'coma') ? ',' : args[:old_spliter]
      Questions::Variant.find_each do |vq|
        vq.variants = vq.variants.gsub(old_spliter, Questions::Variant::VARIANT_SPLITER)
        vq.save
      end
    else
      puts 'Put old splitter before'
    end
  end

  task :default_project => [:environment] do
    User.update_all("project_id = #{Project.default.try(:id)}","project_id = NULL OR project_id = 0")
  end

  task :type_of_quiz =>:environment do
    type = Quiz.kind_values.first
    Quiz.update_all(:type=>type)
    puts "#{Quiz.count} quizzes set to #{type}"
  end

end
