namespace :update_struct do
  
  task :fix_inverse => :environment do
    arr = {
      336 => true,   # 19
      334 => false,  # 100
      335 => false,  # 10
      337 => false,  # 40
      338 => true,   # 76
      339 => false,  # 99
      340 => true,   # 70
      341 => false,  # 54
      342 => true,   # 87
      344 => false,  # 5
      345 => false,  # 13
      346 => true,   # 30
      347 => false,  # 21
      348 => true,   # 43
      349 => true,   # 95
      350 => false,  # 81
      306 => false   # 68
    }
    arr.each do |k, v|
      q = Questions::Question.find(k)
      q.inverse_new = v
      q.save
    end
  end

  task :fix_inverse_all => :environment do
    Questions::Question.where(type: "Questions::Variant").each do |question|
      question.inverse_new = false
      if question.content.present?
        question.inverse_new = question.content[:inverse] if question.content[:inverse].present?
      end
      question.save!
      # inverse = question.content[:enverse]
    end
  end

  task :all=>:environment do
    %w{range_sequence_format range_variants_presentation 
        move_size_of_open update_variant answers attach_to_default_project}.each do |task|
        puts "#{task} start"
        Rake::Task["update_struct:#{task}"].execute
        puts "#{task} is done"
      end
  end


  task :range_sequence_format => :environment do
    Questions::Range.find_each do |question|
      next if question.right_sequences.blank? or question.right_sequences.sample.is_a?(Hash)
      question.content[:right_sequences] = question.content[:right_sequences].map do |item|
        {:value=>item.last, :sequence=>item.first}
      end
      question.save
    end
  end

  task :range_variants_presentation => :environment do
    Questions::Range.find_each do |question|
      next if question.content[:variants].is_a?(Array)
      question.content[:variants] = question.content[:variants].split(Questions::Range::VARIANT_SPLITER).map(&:strip)
      question.save
    end
  end

  task :move_size_of_open => :environment do
    # Questions::Open.find_each{|question| question.update_attributes(:size=>question.comment)}
  end

  task :update_variant => :environment do
    ActiveRecord::Base.record_timestamps = false
    Questions::Variant.find_each do |question|
      variants = question.variants
      if variants.is_a? String
        question.variants = variants
        new_keys = {1=>'list', 2=>'radio'}
        question.view_type = new_keys[question.content.delete(:view_type)]
        question.save
      end
    end
    ActiveRecord::Base.record_timestamps = true
  end

  task :answers=>:environment do
    # [Questions::Variant, Questions::Range].each do |klass|
    #   klass.find_each(:batch_size=>50) do |question|
    #     question.answers.group(:content)[:content].each do |content|
    #       question.answers.where(:content=>content).update_all(:view=>question.humanize_answer(content))
    #     end
    #   end
    # end
  end

  task :attach_to_default_project =>:environment do
    User.where("project_id not in (?)", Project.all).update_all(:project_id=>Project.default.id)
  end

  task :reset_variants => :environment do    
    Questions::Question.where(type: "Questions::Variant").each do |q|
      if q.content[:variants].is_a?(String)
        t = q.content[:variants]
        q.variants = t.force_encoding('utf-8')
        q.update_record_without_timestamping
      end
    end
  end

  task :set_variants => :environment do    
    Questions::Question.where(type: "Questions::Variant").each do |q|
      q.answers.each do |a|        
        begin
          a.view = q.content[:variants][a.value - 1][:value]
          a.update_record_without_timestamping
        rescue
          
        end
      end
    end
  end


end