namespace :admin do

  desc ''
  task :stub_user => :environment do
    user = AdminUser.create!(:email => 'admin@example.com', :password => 'password', :password_confirmation => 'password')
    user.confirm!
  end

end