# -*- encoding : utf-8 -*-
module RelatedQuestionModels

  def self.included(klass)

    if klass.has_answer?

      klass.class_eval do

        include AnswerableQuestion #allow call defined method as super

        has_many    :answers, :foreign_key=>"question_id", :dependent => :destroy
        has_many    :question_factors, :foreign_key=>"question_id", :dependent=>:destroy
        has_many    :factors, :through => :question_factors

        typed_serialize :content, Hash

        validates_presence_of :instruction

        alias_method :factors_list, :factor_ids
        alias_method :factors_list=, :factor_ids=
        alias_method :has_answer_for?, :answer_for
        alias_method :required?, :required

      end

    end

  end

end
