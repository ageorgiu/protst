# -*- encoding : utf-8 -*-
module QuestionTypes

  def self.included(klass)
    class << klass

      {
        :instruction => %w{Questions::Instruction},
        :delimiter => %w{Questions::Divider},
        :partional => %w{Questions::BelbinItem}
      }.each do |type_name, collection|
        define_method("#{type_name}_names") {collection}
        define_method("#{type_name}?") { collection.include?(self.to_s) }
      end

      def non_answerable_names
        instruction_names+delimiter_names+partional_names
      end

      def has_answer?
        !(instruction_names+delimiter_names).include?(self.to_s)
      end

    end

    klass.scope(:answerable, klass.where("`#{klass.table_name}`.`type` NOT IN (?)", klass.non_answerable_names))
    klass.scope(:viewed, klass.where("`#{klass.table_name}`.`type` NOT IN (?)", (klass.delimiter_names+klass.partional_names)))
    klass.scope(:editable, klass.where("`#{klass.table_name}`.`type` NOT IN (?)", klass.partional_names))
    klass.scope(:required, klass.answerable.where("`#{klass.table_name}`.`profile` = ? OR `#{klass.table_name}`.`required` = ?", true, true))
    klass.scope(:unrequired, klass.answerable.where("`#{klass.table_name}`.`profile` != ? and `#{klass.table_name}`.`required` != ?", true, true))
    klass.scope(:not_answered_by, lambda{|user| klass.answerable.where("`#{klass.table_name}`.id not in (select answers.question_id from answers where answers.user_id = ?)",user)} )
  end

  def has_answer?
    self.class.has_answer?
  end

end
