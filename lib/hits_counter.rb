module HitsCounter

  def add_hit(time_deleted)
    class << self
      def record_timestamps; false; end
    end

    self.update_attributes(:deleted_at => time_deleted)

    class << self
      remove_method :record_timestamps
    end
  end

end