# -*- encoding : utf-8 -*-
module QuestionSizes

  def self.included(klass)
    klass.class_eval do

      serialized_reader :size, :in=>:content

      before_validation :check_size

      def self.size_collection
        {"большой"=>'400*300', 'маленький'=>'200*100', :default=>'200*100'}
      end
    end
  end

  def size=(value)
    value = value.to_s.downcase(:ru)
    normalized_value = if value =~ /\A\d+\*\d+$/
      value
    else
      self.class.size_collection[value] || self.class.size_collection[:default]
    end
    self.content[:size] = normalized_value
  end

  def width
    size[/\A\d+/].to_i
  end

  def height
    size[/\d+$/].to_i
  end

  private

    def check_size
      self.size ||= self.class.size_collection[:default]
    end

end
