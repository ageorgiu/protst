# -*- encoding : utf-8 -*-
module QuestionVariants

  def self.included(klass)
    klass.class_eval do
      serialized_reader :variants, :real_variants, :in=>:content
      validates_presence_of :variants
    end
  end

  def variants=(value)
    content[:real_variants] = value
    content[:variants] = process_variant_input(value)
  end

  def process_variant_input(value)
    value.to_s.split(AppConfig.variant_splitter).map(&:strip).select(&:present?)
  end

end
