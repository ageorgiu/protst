# -*- encoding : utf-8 -*-
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/rspec'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}


require 'capybara/rspec'

begin
  Capybara.register_driver :selenium_chrome do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
rescue
  puts %q{
    For full feature testing add selenium chrome driver
    First download file from http://code.google.com/p/chromium/downloads/list
    paste in /usr/bin and make exucatable, or change Capybara.default_driver to :selenium in spec/spec_helper
  }
end

RSpec.configure do |config|

  config.include(MailerMacros)
  config.include(CapybaraMacros)

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.

  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.filter_run :focus => true
  config.run_all_when_everything_filtered = true

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    DatabaseCleaner.start
    FactoryGirl.create(:project, :default=>true)
  end
  Capybara.javascript_driver = :webkit

  config.after(:each) do
    DatabaseCleaner.clean
  end

  config.use_transactional_fixtures = false
end
