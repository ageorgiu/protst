# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Report do

  it{should belong_to(:quiz)}
  it{should have_many(:interpretations).dependent(:destroy)}
  it{should have_many(:project_quizzes).dependent(:destroy)}

  it 'should return intepretation of factor with sten' do
    report = FactoryGirl.create(:report)
    factor = FactoryGirl.create(:factor)
    sten = 4.5
    report.find_interpretation(factor, sten).should_not be
    FactoryGirl.create(:interpretation, :factor=>factor,:sten_to=>8, :sten_from=>5, :report=>report)
    report.find_interpretation(factor,sten).should_not be
    FactoryGirl.create(:interpretation, :factor=>factor,:sten_to=>8, :sten_from=>4, :report=>report)
    report.find_interpretation(factor,sten).should be
  end

end
# == Schema Information
#
# Table name: reports
#
#  id         :integer(4)      not null, primary key
#  quiz_id    :integer(4)      default(0), not null
#  title      :string(255)     not null
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#

