# -*- encoding : utf-8 -*-
require 'spec_helper'

describe User do

  it{should belong_to(:city)}
  it{should belong_to(:education_level)}
  it{should belong_to(:job_area)}
  it{should belong_to(:job_expirience)}
  it{should belong_to(:job_position)}
  it{should belong_to(:education_type)}

  it 'calculate age on date' do
    user = Factory :user, :birthday=>32.years.ago
    user.age.should == 32
  end

  it 'should has incomplete quiz' do
    user = Factory :user
    3.times do 
      user.quizzes << FactoryGirl.create(:quiz)
    end
    user.should have_incomplete_quizzes
    UserQuiz.update_all(:complete=>true)
    user.user_quizzes.reload
    user.should_not have_incomplete_quizzes
  end

  it 'generate valid user' do
    user = User.generate
    user.should be_valid
  end

  it 'should provide human gender' do
    user = User.new(:gender=>'none')
    user.humanized_gender.should == 'Не определен'
    user.gender = 'male'
    user.humanized_gender.should == 'Мужской'
    user.gender = 'female'
    user.humanized_gender.should == 'Женский'
  end

  it 'should have collection of project quizzes' do
    user = FactoryGirl.create(:user)
    user.project_quizzes.should have(0).items
    FactoryGirl.create(:project_quiz, :project=>user.project)
    user.project.reload
    user.project_quizzes.should have(1).item
  end

end# == Schema Information
#
# Table name: users
#
#  id                 :integer(4)      not null, primary key
#  login              :string(255)
#  password           :string(255)
#  project_id         :integer(4)      default(0)
#  name               :string(255)
#  surname            :string(255)
#  patronymic         :string(255)     not null
#  birthday           :date
#  email              :string(255)
#  phone              :string(255)
#  job_area_id        :integer(4)      default(0)
#  job_expirience_id  :integer(4)      default(0)
#  job_position_id    :integer(4)      default(0)
#  city_id            :integer(4)      default(0)
#  education_level_id :integer(4)      default(0)
#  education_type_id  :integer(4)      default(0)
#  created_at         :datetime
#  updated_at         :datetime
#  restricted         :boolean(1)      default(FALSE)
#  gender             :string(255)
#  custom             :boolean(1)
#

