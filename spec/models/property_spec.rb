# -*- encoding : utf-8 -*-
require 'spec_helper'

shared_examples_for 'Property' do

  it{should have_many(:users)}
  it{should allow_value("normal length").for(:title)}
  it{should_not allow_value('s').for(:title)}

end

describe City do

  it_should_behave_like 'Property'

end

describe JobArea do

  it_should_behave_like 'Property'

end

describe JobExpirience do

  it_should_behave_like 'Property'

end

describe JobPosition do

  it_should_behave_like 'Property'

end

describe EducationLevel do

  it_should_behave_like 'Property'

end

describe EducationType do

  it_should_behave_like 'Property'

end# == Schema Information
#
# Table name: properties
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)     not null
#  weight     :integer(4)      default(0), not null
#  created_at :datetime
#  updated_at :datetime
#  type       :string(255)
#

