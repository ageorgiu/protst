# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ProfileCharachteristic do

  specify{ FactoryGirl.create(:profile_charachteristic) }

  it 'should check edge values' do
    pc = Factory.build(:profile_charachteristic, :min_value=>10, :max_value=>1)
    pc.should_not be_valid
    pc.errors[:edge].should be_present
  end

end 
# == Schema Information
#
# Table name: profile_charachteristics
#
#  id             :integer(4)      not null, primary key
#  quiz_id        :integer(4)
#  min_value      :integer(4)
#  max_value      :integer(4)
#  interpretation :text
#  created_at     :datetime
#  updated_at     :datetime
#  value          :integer(4)      default(9)
#

