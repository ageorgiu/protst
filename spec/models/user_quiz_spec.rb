# -*- encoding : utf-8 -*-
require 'spec_helper'

describe UserQuiz do

  it 'return questions that not answered but user' do
    user_quiz = FactoryGirl.create(:user_quiz)
    user_quiz.unanswered_questions.should have(0).items
    quiz = user_quiz.quiz
    user = user_quiz.user
    3.times{FactoryGirl.create(:open_question, :quiz=>quiz, :profile=>true)}
    2.times{FactoryGirl.create(:scale_question, :quiz=>quiz, :required=>true)}
    FactoryGirl.create(:question_variant, :quiz=>quiz, :profile=>false, :required=>false)
    FactoryGirl.create(:instruction_question, :quiz=>quiz, :profile=>true, :required=>true)
    user_quiz.quiz.reload
    user_quiz.unanswered_questions.should have(6).items
    question = Questions::Open.first
    FactoryGirl.create(:answer, :question=>question, :user=>user)
    another_user= FactoryGirl.create(:user)
    another_question = Questions::Scale.first
    FactoryGirl.create(:answer, :question=>another_question, :user=>another_user)
    user_quiz.quiz.reload
    user_quiz.unanswered_questions.should have(5).items
  end

  it 'run quiz for user' do
    user = FactoryGirl.create(:user)
    quiz = FactoryGirl.create(:quiz)
    user_quiz = UserQuiz.run(user, quiz)
    user_quiz.should be_persisted
    UserQuiz.run(user, quiz).should == user_quiz
  end

  context 'completes' do
    before :each do
      @user = FactoryGirl.create(:user)
      @quiz = FactoryGirl.create(:quiz)
      FactoryGirl.create(:project_quiz, :project=>@user.project, :quiz=>@quiz)
      @user_quiz = FactoryGirl.create(:user_quiz, :user=>@user, :quiz=>@quiz)
      5.times{@quiz.questions << Factory.build(:open_question, :quiz=>nil)}
    end

    it 'with normal quiz and required questions' do
      Questions::Question.update_all(:required=>true)
      @quiz.update_attribute(:limited_in_time, nil)
      unanswered_question = @user_quiz.complete!
      unanswered_question.should have(5).items
      unanswered_question.each do |question|
        question.generate_answer(@user)
      end
      @user_quiz.complete!.should be_empty
      @user_quiz.should be_complete
      last_email.to.should include(AppConfig.setting[:admin_email])
    end

    it 'with normal quiz not required answers' do
      @quiz.update_attribute(:limited_in_time, nil)
      Questions::Question.update_all(:required=>false, :profile=>false)
      Answer.count.should == 0
      @user_quiz.complete!.should be_empty
      Answer.count.should == 5
      @user_quiz.should be_complete
    end

    it 'with limited by time' do
      Questions::Question.limit(3).update_all(:profile=>true)
      @quiz.update_attribute(:limited_in_time, true)
      Answer.count.should == 0
      @user_quiz.complete!.should be_empty
      Answer.count.should == 5
      @user_quiz.should be_complete
    end

  end

end
# == Schema Information
#
# Table name: user_quizzes
#
#  id         :integer(4)      not null, primary key
#  user_id    :integer(4)      default(0), not null
#  quiz_id    :integer(4)      default(0), not null
#  created_at :datetime
#  updated_at :datetime
#  complete   :boolean(1)
#

