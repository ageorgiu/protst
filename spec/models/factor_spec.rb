# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Factor do

  it{should belong_to(:quiz)}
  it{should belong_to(:parent)}

  it{should have_many(:question_factors).dependent(:destroy)}
  it{should have_many(:questions).through(:question_factors)}
  it{should have_many(:children)}

  it{should validate_presence_of(:quiz)}
  it{should validate_presence_of(:title)}

  it 'have many respondents' do
    quiz = FactoryGirl.create(:quiz)
    factor = FactoryGirl.create(:factor, :quiz=>quiz)
    user = FactoryGirl.create(:user)
    factor.respondents.should be_empty
    question = FactoryGirl.create(:open_question, :quiz=>quiz)
    factor.questions << question
    factor.respondents.should be_empty
    FactoryGirl.create(:answer, :question=>question, :user=>user)
    factor.respondents.should be_empty
    FactoryGirl.create(:complete_user_quiz, :user=>user, :quiz=>quiz)
    factor.respondents.should have(1).item
  end

  context 'calculate' do

    before :each do
      quiz = FactoryGirl.create(:quiz)
      @factor = FactoryGirl.create(:factor, :quiz=>quiz)
      3.times{@factor.questions << FactoryGirl.create(:question_variant, :quiz=>quiz)}
      2.times do |i|
        user = FactoryGirl.create(:user)
        FactoryGirl.create(:complete_user_quiz, :user=>user, :quiz=>quiz)
        @factor.value(user).should == 0
        @factor.questions.each_with_index do |question, index|
          binding.pry
          FactoryGirl.create(:answer, :question=>question, :user=>user, :value=>(1+index)*(1+i))
        end
      end
      @factor.elaborate_statistics
    end

    it 'value for user and users', :focus do
      # default scope id desc
      # binding.pry
      @factor.value(User.first).should == 12.0
      @factor.value(User.last).should == 6.0
      @factor.values(User.all).should == [6.0,12.0]
    end

    it 'statistic(variance and mean)' do
      @factor.mean.should == 9
      @factor.variance.round(2).should == 9
      another_factor = Factor.new(:variance=>324, :mean=>23)
      another_factor.elaborate_statistics
      another_factor.mean.should == 0
      another_factor.variance.should == 0
    end

    it 'zvalue' do
      @factor.zvalue(User.first).round(3).should == 0.333
      @factor.zvalue(User.last).round(3).should == -0.333
    end

    it 'sten' do
      @factor.sten(User.first).round(2).should == 6.17
      @factor.sten(User.last).round(2).should == 4.83
    end

    it 'alpha' do
      variance_sum = Questions::Question.all.map(&:variance).sum
      @factor.alpha.should == (3.0/2)*(1-variance_sum/(@factor.variance**2))
    end
  end

end
# == Schema Information
#
# Table name: factors
#
#  id         :integer(4)      not null, primary key
#  quiz_id    :integer(4)      default(0), not null
#  title      :string(255)     not null
#  parent_id  :integer(4)      default(0), not null
#  lft        :integer(4)      default(0), not null
#  rgt        :integer(4)      default(0), not null
#  kind       :integer(4)
#  created_at :datetime
#  updated_at :datetime
#  mean       :float           default(0.0), not null
#  variance   :float           default(0.0), not null
#  weight     :integer(4)      default(0), not null
#

