# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Questions::Variant do
  
  it_should_behave_like "answerable question", :question_variant

  it{should validate_presence_of(:variants)}

  it 'should clear weight in variants value' do
    value = 'вариант вопроса'
    value2 = 'another'
    weight = 344
    question = FactoryGirl.create(:question_variant, :variants=>"something
    #{value2}
    new_variant
    #{value}%#{weight}
    last variant")
    question.list_variants[3].should == value
    question.variants.should have(5).items
    question.variants[3][:value].should == weight
    question.score_answer('3').should == weight
    question.score_answer('1').should == 2
  end

  context 'score answer' do
    before :each do
      @question = FactoryGirl.create(:question_variant, :variants=>"
        one
        two
        three
        four
      ", :inverse=>false)
    end

    it 'score answer by postion' do
      answer = "1"
      @question.score_answer(answer).should == 2
      @question.update_attributes(:inverse=>true)
      @question.score_answer(answer).should == 3
    end

    it "answer doesn't present in variant" do
      answer = "5"
      @question.score_answer(answer).should_not be
    end

    it 'score with weight' do
      @question.variants = %q{
        one%123
        two%343
        three%14
        four
        ten%15
      }
      @question.score_answer("4").should == 15
      @question.score_answer("3").should == 4
    end

  end

  specify{ FactoryGirl.create(:question_variant).variants.last[:value].should_not be}

  specify {FactoryGirl.create(:question_variant).humanize_answer("").should == "Не успел ответить"}

end# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

