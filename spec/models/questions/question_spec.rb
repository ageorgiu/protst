# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Questions::Question do

  it{should validate_presence_of(:quiz)}

  it 'should divide subclass on group' do
    Questions::Question.scopes[:answerable].should be
  end

  it 'should get answer for user' do
    question = Factory :open_question
    another_question = Factory :open_question
    user = Factory :user
    4.times{Factory :answer, :user=>user, :question=>question}
  end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

