# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Questions::BelbinRange do

  it_should_behave_like 'answerable question', :belbin_range

  it{should have_many(:belbin_items).dependent(:destroy)}

  context 'process user input' do
    before :each do
      @quiz = FactoryGirl.create(:belbin_quiz)
      3.times{FactoryGirl.create(:child_factor, :quiz=>@quiz)}
      @question = FactoryGirl.create(:belbin_range, :quiz=>@quiz)
      %w{One Two Three}.each do |instruction|
        @question.belbin_items << Questions::BelbinItem.new(:instruction=>instruction)
      end
      @ids = Questions::BelbinItem.values_of(:id)
    end

    it 'humanize answer' do
      @question.humanize_answer("#{@ids[1]},#{@ids[0]},#{@ids[2]}").should == "One - 2\nTwo - 3\nThree - 1"
      @question.humanize_answer("#{@ids[2]},#{@ids[1]},#{@ids[0]}").should == "One - 1\nTwo - 2\nThree - 3"
      @question.humanize_answer("").should == "Не успел ответить"
    end

    context 'generate answer' do

      before :each do
        @user = FactoryGirl.create(:user)
      end

      it 'normal scenario' do
        answer = "#{@ids[1]},#{@ids[0]},#{@ids[2]}"
        answer = @question.generate_answer(@user, answer)
        answer.view.should == "One - 2\nTwo - 3\nThree - 1"
        Answer.count.should == 4
        sub_answer = Answer.find_by_question_id(@ids.first)
        sub_answer.view.should == 'One - 2'
        sub_answer.value.should == 2
      end

      it 'auto generate' do
        answer = @question.generate_answer(@user)
        Answer.count.should == 4
        Answer.all.all?{|answer| answer.view== "Не успел ответить"}.should be_true
      end

    end

  end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

