# -*- encoding : utf-8 -*-
require 'spec_helper'


describe Questions::BelbinItem do

  it_should_behave_like "answerable question", :belbin_item

  it {should belong_to(:belbin_range)}

  context "process input" do

    before :each do
      @question = FactoryGirl.create(:belbin_item, :instruction=>"You score")
    end

    it 'should humanize answer' do
      @question.humanize_answer("23,22,434,#{@question.id}").should == "#{@question.instruction} - 1"
      @question.humanize_answer("23,#{@question.id}").should == "#{@question.instruction} - 1"
      @question.humanize_answer("99,211,23232").should == "99,211,23232"
    end

    it 'should score input' do
      @question.score_answer("223,322,21,#{@question.id}").should == 1
      @question.score_answer("#{@question.id},324,32,21").should == 4
      @question.score_answer("32423,3422,432").should == nil
    end

  end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

