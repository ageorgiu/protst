# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Questions::Range do

  it_should_behave_like "answerable question", :range_question

  context 'score answer' do

    it 'allow multiply right sequence' do
      right_sequences = {
        1 => {'value'=>4, 'sequence'=>%q{
          value1
          value2
          value3
        }},
        2 => {'value'=>5, 'sequence'=>%q{
          value3
          value2
          value1
        }}
      }
      variants = %q{
        value1
        value2
        value3
      }
      question = FactoryGirl.create(:range_question, :right_sequences=>right_sequences, :variants=>variants)
      question.score_answer('0,1,2').should == right_sequences[1]['value']
      question.score_answer('2,1,0').should == right_sequences[2]['value']
    end

    it 'answer is invalid' do
      question = FactoryGirl.create(:range_question)
      answer ="3423,32,21"
      question.score_answer(answer).should == nil
      answer = "foos,dso,a"
      question.score_answer(answer).should == nil
      answer = ""
      question.score_answer(answer).should == nil
      answer = '22 3232 21'
      question.score_answer(answer).should == nil
    end

  end

  context "process input values" do

    it 'should clean variants on whitespaces' do
      question = FactoryGirl.create(:range_question, :variants=>"   some value
      another sksksk  
      
      something  

      ")
      question.variants.should == ['some value', 'another sksksk' , 'something']
    end

    it 'reject empty values' do
      right_sequences = {
        1 => {"sequence"=>"", 'value'=>4},
        23 => {"sequence"=>"value4
          value3
          value2", 'value'=>5
        },
        2 => {'value'=>"", "sequence"=>"value4
          value3
          value2"
        }
      }
      question = FactoryGirl.create(:range_question, :right_sequences=>right_sequences)
      question.right_sequences.should have(1).item
    end

  end

  it 'should generate answer' do
    variants = 'One
    Three
    Two'
    question = FactoryGirl.create(:range_question, :variants=>variants)
    question.humanize_answer('0,2,1').should == "One\nTwo\nThree"
  end

  it 'should create question without params' do
    question = Questions::Range.create
    question.variants.should be_present
    question.right_sequences.should be_present
    question.instruction.should be_present
  end


end# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

