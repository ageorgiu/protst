# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Questions::Scale do
  
  it_should_behave_like "answerable question", :scale_question

  it 'should generate proper instruction' do
    scale_question = FactoryGirl.create(:scale_question, :left_instruction=>"провал", 
      :right_instruction=>"успех", :length=>2)
    scale_question.instruction.should == "Если провал -2, а успех 2"
  end

end# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

