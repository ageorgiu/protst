# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Questions::Open do

  it_should_behave_like "answerable question", :open_question

  it 'should allow text size' do
    oq = FactoryGirl.create(:open_question, :size=>'БоЛьшой')
    oq.size.should == Questions::Open.size_collection['большой']
  end

  it 'must have width and heigh' do
    height = 23
    width = 40
    oq = FactoryGirl.create(:open_question, :size=>"#{width}*#{height}")
    oq.width.should == width
    oq.height.should == height
  end

  it 'should fall with incorrect size format' do
    [nil, 'Произвольный', '2332x222', 'wdw2333*22'].each do |example|
      open_question = FactoryGirl.create(:open_question, :size=>example)
      open_question.size.should == Questions::Open.size_collection[:default]
    end
  end

  it 'should humanize answer' do
    q = Questions::Open.new
    test_answer = %Q{Little text about nothing}
    q.humanize_answer(test_answer).should == test_answer
  end

end# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

