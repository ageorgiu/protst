# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Quiz do

  subject{ FactoryGirl.create(:quiz) }

  it{should have_many(:questions).dependent(:destroy)}
  it{should have_many(:project_quizzes).dependent(:destroy)}
  it{should have_many(:projects).through(:project_quizzes)}
  it{should have_many(:reports)}
  it{should have_many(:factors).dependent(:destroy)}
  it{should have_many(:user_quizzes).dependent(:destroy)}
  it{should have_many(:completed_user_quizzes)}
  it{should have_many(:completed_users).through(:completed_user_quizzes)}
  it{should have_many(:profile_charachteristics).dependent(:destroy)}

  it{should validate_presence_of(:title)}
  it{should validate_presence_of(:kind)}
  it{should allow_value('test').for(:kind)}
  it{should_not allow_value('foo_type').for(:kind)}
  it{Quiz.create.should be_true}

  it 'should return collection of answers' do
    user = FactoryGirl.create(:user)
    5.times{FactoryGirl.create :open_question, :quiz=>subject}
    Questions::Open.all.each do |question|
      FactoryGirl.create(:answer, :user=>user, :question=>question)
    end
    subject.answers_for(user).should have(5).items
  end

  it 'add weight on adding' do
    3.times{subject.questions << Questions::Open.new}
    subject.questions.last.weight.should == 2
  end

  context "provide various pagination" do

    before :each do
      3.times{subject.questions << Questions::Instruction.new}
      subject.questions << Questions::Divider.new
      5.times do
        subject.questions << Questions::Variant.new
      end
      subject.questions << Questions::Divider.new
      4.times{subject.questions << Questions::Range.new}
    end

    it 'should paginate as limited in time' do
      subject.update_attribute(:limited_in_time, true)
      subject.num_quiz_pages.should == 12
      subject.page_questions(2).should include(Questions::Instruction.last)
      subject.page_questions(3).should include(Questions::Variant.first)
      subject.page_questions(-1).should == []
      subject.page_questions(12).should == []
      subject.page_questions(11).should have(1).item
    end

    it 'should paginate with custom pagination' do
      subject.update_attribute(:custom_pagination, true)
      subject.num_quiz_pages.should == 3
      subject.page_questions(0).should have(3).items
      subject.page_questions(1).should have(5).items
      subject.page_questions(2).should have(4).items
      subject.page_questions(-1).should == []
      subject.page_questions(3).should == []
    end

    it 'should paginanate by normal condition' do
      subject.update_attributes(:custom_pagination=>:false, :page_size=>5)
      subject.num_quiz_pages.should == 3
      subject.page_questions(0).should have(5).items
      subject.page_questions(2).should have(2).items
      subject.page_questions(-1).should == []
    end

  end

  it 'is default case' do
    stens = [2,3,4,2,2,2,3,3,4,7,4,5,6,6,6,2,2]
    FactoryGirl.create(:profile_charachteristic, :min_value=>4, :quiz_id=>subject.id)
    pc = FactoryGirl.create(:profile_charachteristic, :max_value=>3, :quiz_id=>subject.id, :interpretation=>'Some words that don\'t make sense')
    subject.get_profile_charachteristic(stens).should == pc.interpretation
  end

  it 'should return first result with two charachteristics' do
    stens = [2,3,4,2,2,2,3,3,4,7,4,5,6,6,6,2,2]
    pc1 = FactoryGirl.create(:profile_charachteristic, :min_value=>1, :value => 8, :quiz_id=>subject.id)
    pc2 = FactoryGirl.create(:profile_charachteristic, :min_value=>1, :quiz_id=>subject.id, :interpretation=>'Some words that don\'t make sense')
    subject.get_profile_charachteristic(stens).should == pc1.interpretation
  end

  it 'is interpretation without chrachteristic' do
    stens = [3,2,3,4,4,2,2,4]
    subject.get_profile_charachteristic(stens).should be_nil
  end

  it "is interpretation when major sten's group dos't present" do
    stens = [2,4,5,2,4,5,6,2,4]
    FactoryGirl.create(:profile_charachteristic, :max_value=>4, :quiz_id=>subject.id)
    FactoryGirl.create(:profile_charachteristic, :min_value=>5, :quiz_id=>subject.id)
    subject.get_profile_charachteristic(stens).should be_nil
  end

  it 'is interpretation when some gup don\'t have stens' do
    stens = [23,44,223,433]
    FactoryGirl.create(:profile_charachteristic, :quiz_id=>subject.id)
    subject.get_profile_charachteristic(stens).should be_nil
  end

end
# == Schema Information
#
# Table name: quizzes
#
#  id                :integer(4)      not null, primary key
#  title             :string(255)     not null
#  created_at        :datetime
#  updated_at        :datetime
#  custom_pagination :boolean(1)      default(FALSE), not null
#  page_size         :integer(4)      default(5), not null
#  forward           :boolean(1)
#  kind              :string(255)
#  limited_in_time   :integer(4)
#  custom_numeration :boolean(1)
#  calculating       :boolean(1)      default(TRUE)
#

