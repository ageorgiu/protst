# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Interpretation do

  it{should belong_to(:factor)}
  it{should belong_to(:report)}

  it{should validate_numericality_of(:sten_from)}
  it{should validate_numericality_of(:sten_to)}

  it 'should check range of sten' do
    intepretation = Interpretation.new(:sten_from=>5, :sten_to=>3)
    intepretation.should_not be_valid
    intepretation.errors.full_messages.should include("Минимальное значение стена больше максимального")
  end

end
# == Schema Information
#
# Table name: interpretations
#
#  id         :integer(4)      not null, primary key
#  factor_id  :integer(4)      default(0), not null
#  report_id  :integer(4)      default(0), not null
#  sten_from  :integer(4)      default(0), not null
#  sten_to    :integer(4)      default(0), not null
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#

