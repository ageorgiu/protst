# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Project do

  it 'should return statistic about quizzes' do
    project = FactoryGirl.create(:project)
    count_of_users = 14
    count_of_users.times{FactoryGirl.create(:user, :project=>project)}
    count_of_quizzes = 2
    count_of_quizzes.times do
      report = FactoryGirl.create(:report)
      project.project_quizzes.create(:report=>report, :quiz=>report.quiz)
    end
    completed_tests = 2
    completed_tests.times do |i|
      user = project.users[i]
      project.quizzes.each do |quiz|
        FactoryGirl.create(:complete_user_quiz, :user=>user, :quiz=>quiz)
      end
    end
    started_tests = 3
    started_tests.times do |i|
      user = project.users[-1*(i+1)]
      project.quizzes.each do |quiz|
        FactoryGirl.create(:user_quiz, :user=>user, :quiz=>quiz)
      end
    end
    statistic = project.statistic
    statistic.should have(count_of_quizzes).items
    statistic.first[:all].should == completed_tests+started_tests
    statistic.last[:completed].should == completed_tests
    statistic.last[:people].should == count_of_users
  end

  it 'should be undeleted if default' do
    project = FactoryGirl.create(:project, :default=>true)
    project.destroy.should be_false
    Project.default.should be
  end

end
# == Schema Information
#
# Table name: projects
#
#  id           :integer(4)      not null, primary key
#  title        :string(255)     not null
#  created_at   :datetime
#  updated_at   :datetime
#  allow_anonim :boolean(1)
#  default      :boolean(1)      default(FALSE)
#  style        :text
#

