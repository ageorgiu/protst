# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Answer do
  
  it{should belong_to(:user)}
  it{should belong_to(:question)}

  it{should validate_presence_of(:user)}
  it{should validate_presence_of(:question)}

end# == Schema Information
#
# Table name: answers
#
#  id          :integer(4)      not null, primary key
#  user_id     :integer(4)      default(0), not null
#  question_id :integer(4)      default(0), not null
#  value       :integer(4)
#  content     :text
#  created_at  :datetime
#  updated_at  :datetime
#  view        :text
#

