# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ProjectQuiz do

  it{should belong_to(:project)}
  it{should belong_to(:quiz)}
  it{should belong_to(:report)}


  it{should validate_presence_of(:project)}
  it{should validate_presence_of(:quiz)}
  it{should validate_presence_of(:report)}

  it 'should validate report quiz' do
    quiz = FactoryGirl.create(:quiz)
    report = FactoryGirl.create(:report)
    project = FactoryGirl.create(:project)
    project_quiz = ProjectQuiz.new(:quiz=>quiz, :project=>project, :report=>report)
    project_quiz.should_not be_valid
    project_quiz.errors[:report].should include("относится к другому опроснику")
    project_quiz.report.quiz = quiz
    project_quiz.should be_valid
  end

  it 'should validates uniquess of combitation project quiz' do
    quiz = FactoryGirl.create(:quiz)
    project = FactoryGirl.create(:project)
    report = FactoryGirl.create(:report, :quiz=>quiz)
    ProjectQuiz.create!(:quiz=>quiz, :project=>project, :report=>report)
    another_project_quiz = ProjectQuiz.create(:quiz=>quiz, :project=>project, :report=>report)
    another_project_quiz.should_not be_valid
    another_project_quiz.errors[:quiz_id].should include("уже назначен этому проекту")
  end

end
# == Schema Information
#
# Table name: project_quizzes
#
#  id                 :integer(4)      not null, primary key
#  project_id         :integer(4)
#  quiz_id            :integer(4)
#  report_id          :integer(4)
#  created_at         :datetime
#  updated_at         :datetime
#  show_report        :boolean(1)
#  show_quiz_title    :boolean(1)
#  escape_instruction :boolean(1)
#  announce           :text
#  instruction        :text
#  mark_required      :boolean(1)
#  epilog             :text
#

