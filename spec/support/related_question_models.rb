# -*- encoding : utf-8 -*-
shared_examples_for "answerable question" do |factory_name|

  it{should have_many(:answers).dependent(:destroy)}
  it{should have_many(:question_factors).dependent(:destroy)}
  it{should have_many(:factors).through(:question_factors)}

  it 'should mass create factors' do
    5.times{FactoryGirl.create(:factor)}
    test_array = Factor.first(3).map(&:id)
    question = FactoryGirl.create(factory_name, :factors_list => test_array)
    question.question_factors.reload
    question.should have(3).question_factors
    question.should have(3).factors
    question.factors_list.should have(3).items
  end

  it 'should retrive answer for user' do
    question = FactoryGirl.create(factory_name)
    user = FactoryGirl.create(:user)
    answer = FactoryGirl.create(:answer, :user=>user, :question=>question)
    question.should have_answer_for(user)
    question.answer_for(user).should == answer
  end

  it 'should have custom numeration' do
    question = FactoryGirl.create(factory_name)
    question.numeration.should be_nil
    question.numeration = '123'
    question.numeration.should == '123'
  end

  it 'should be required if profile' do
    question = Factory.build(factory_name, :profile=>false, :required=>false)
    question.should_not be_required
    question.profile = true
    question.should be_required
  end

  it 'should process nil answer' do
    question = Factory.build(factory_name)
    question.humanize_answer(nil).should == "Не успел ответить"
  end

  it 'generate answer for user' do
    question = FactoryGirl.create(factory_name)
    user = FactoryGirl.create(:user)
    answer = question.generate_answer(user)
    answer.should be_persisted
    another_answer = question.generate_answer(user, "new not nil value")
    another_answer.should == answer
    another_answer.view.should_not == answer.view
  end

  it 'should not calculate variance' do
    quiz = FactoryGirl.create(:quiz)
    question = FactoryGirl.create(factory_name, :quiz=>quiz)
    question.variance.should == 0
    FactoryGirl.create(:answer, :question=>question)
    question.variance.should == 0
    2.times do |i|
      user = FactoryGirl.create(:user)
      FactoryGirl.create(:complete_user_quiz, :user=>user, :quiz=>quiz)
    end
    question.variance.should == 0
  end

  it 'should calculate variance' do
    quiz = FactoryGirl.create(:quiz)
    question = FactoryGirl.create(factory_name, :quiz=>quiz)
    8.times do |index|
      user = FactoryGirl.create(:user)
      FactoryGirl.create(:complete_user_quiz, :user=>user, :quiz=>quiz)
      FactoryGirl.create(:answer, :question=>question, :user=>user, :value=>(index+3))
    end
    question.variance.round(2).should == 5.25
    Answer.find_by_value(8).update_attribute(:value, 0)
    question.variance.round(2).should == 9.25
  end

end

shared_examples_for "delimitter question" do |factory_name|

  it{should_not have_many(:answers)}
  it{should_not have_many(:question_factors)}
  it{should_not have_many(:factors)}

end
