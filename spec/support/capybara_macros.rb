# -*- encoding : utf-8 -*-
module CapybaraMacros

  def loggin_as_person(user)
    visit '/'
    within 'div.forms' do
      fill_in 'login', :with=>user.login
      fill_in 'password', :with=>user.password
      submit_form
    end
  end

  def next_quiz_page
    find("a.next").click
  end

  def complete_quiz
    find("a.complete").click
  end

  def fill_register_form(user_stub, options={})
    within "div.forms" do
      unless options[:remote]
        fill_in 'user[login]', :with=>user_stub.login
        fill_in 'user[password]', :with =>user_stub.password
        fill_in 'user[password_confirmation]', :with=>user_stub.password
      end
      unless user_stub.restricted?
        fill_in 'user[name]', :with=>user_stub.name
        fill_in 'user[surname]', :with=>user_stub.surname
        fill_in 'user[patronymic]', :with=>user_stub.patronymic
      end
      choose "user_gender_#{user_stub.gender}"
      fill_in 'user[email]', :with=>user_stub.email
      fill_in 'user[phone]', :with=>user_stub.phone
      fill_in 'user[birthday]', :with=>user_stub.birthday.to_s
      fill_in 'city_title', :with=>user_stub.city_title
    end
    find("div.autocomplete div").click
    within "div.forms" do
      select user_stub.education_level_title, :from=>'user_education_level_id'
      select user_stub.education_type_title, :from=>'user_education_type_id'
      select user_stub.job_area_title, :from=>'user_job_area_id'
      select user_stub.job_position_title, :from=>'user_job_position_id'
      select user_stub.job_expirience_title, :from=>'user_job_expirience_id'
      click_on 'Готово'
    end
  end

  def submit_form
    find("input[type=submit], button[type=submit]").click
  end

end
