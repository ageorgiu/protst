# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :page do |page|
    page.title 'template'
    page.content 'some content'
    page.special 'special'
  end
end
