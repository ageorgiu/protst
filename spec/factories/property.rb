# -*- encoding : utf-8 -*-
FactoryGirl.define do

  [:city, :education_level, :job_area, :job_expirience, 
    :job_position, :education_type].each do |property_type|
    factory property_type do
      weight 1
      sequence(:title){|n| "#{property_type.to_s.classify} ##{n}"}
    end    
  end

end
