# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :question, :class=>Questions::Question do
    required true
    association :quiz
  end

  factory :question_variant, :parent=>:question, :class=>Questions::Variant do
    instruction 'Your score is'
    variants "one
              two
              three
              four"
    view_type "radio"
  end

  factory :range_question, :parent=>:question, :class=>Questions::Range do
    instruction 'Make right order'
    variants 'two
              three
              four
              one'
    right_sequences({ :one=>{'sequence'=>%q{
                                              one
                                              two
                                              three
                                              four
                                            },
                                            'value'=>"3"
                                          }
                    })
  end

  factory :belbin_range, :parent=>:question, :class=>Questions::BelbinRange do
    instruction 'Range in your way'
    variants = {}
  end

  factory :belbin_item, :parent=>:question, :class=>Questions::BelbinItem do
    sequence(:instruction){|n| "You choose variant ##{n+1}"}
  end

  factory :scale_question, :parent=>:question, :class=>Questions::Scale do
    length 4
    left_instruction "Bad"
    right_instruction "Good"
  end

  factory :open_question, :parent=>:question, :class=>Questions::Open do
    instruction 'Write little text about ...'
    size '300*200'
  end

  factory :instruction_question, :parent=>:question, :class=>Questions::Instruction do
    instruction 'This text help you pass quiz'
  end

  factory :divider_question, :parent=>:question, :class=>Questions::Divider do
  end

end
