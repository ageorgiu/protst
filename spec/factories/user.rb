# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :user do
    association :project
    sequence(:login) {|n| "login_#{n}"}
    password 'secret'
    name 'John'
    surname 'Patters'
    patronymic "Jalkoby"
    gender AppConfig.user_genders.first
    birthday 23.year.ago.to_date
    sequence(:email) { |n| "email#{n}@example.com" }
    phone "12212122"
    association :job_area
    association :job_expirience
    association :job_position
    association :city
    association :education_level
    association :education_type
    restricted false
  end

end
