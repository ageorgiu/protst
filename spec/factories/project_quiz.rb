# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :project_quiz do
    association :project
    association :quiz
    after_build do |project_quiz|
      project_quiz.report = FactoryGirl.create(:report, :quiz=>project_quiz.quiz)
    end
    announce 'Some text about quiz'
    mark_required true
    instruction 'tips about complete quiz'
    epilog 'You end our test. So run it again with more dependencies.'
    show_report false
    escape_instruction true
    show_quiz_title false
  end

end
