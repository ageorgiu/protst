# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :interpretation do
    association :factor
    association :report
    sten_from 1
    sten_to 7
  end

end
