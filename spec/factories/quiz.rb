# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :quiz do
    title 'Quiz from factory'
    kind 'interview'
    custom_pagination false
  end

  factory :belbin_quiz, :parent=>:quiz do
    title 'Self-perception quiz of Belbin'
    kind 'belbin'
  end
  
end
