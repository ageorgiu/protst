# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :answer do

    value :value
    content 'Sample answer text'
    association :user

  end

end
