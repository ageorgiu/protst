# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :project do |p|
    p.title 'Test project'
    p.allow_anonim false
    p.default false
    p.style ".quiz-title
              {
                color:#239923;
                font-size:24px;
                font-style:italic;
                font-weight:bold;
              }
              .instruction
              {
                font-size:14px;
                text-indent:25px;
                color:#444;
              }
              .button
              {
                color:#2365AA;
                cursor:pointer;
              }

              .text
              {
                font-size:14px;
                color:#232323;
              }
              li.variant
              {
                font-size:12px;
                font-family: Tahoma, Times;
                text-decoration:underline;
              }"
  end
end