# -*- encoding : utf-8 -*-
FactoryGirl.define do
  
  factory :user_quiz do
    association :user
    association :quiz
  end

  factory :complete_user_quiz, :parent=>:user_quiz do
    complete true
  end

end
