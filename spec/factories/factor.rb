# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :factor do
    association :quiz
    title "Factor name"
  end

  factory :factor_group, :parent=>:factor do
    parent_id 0
  end

  factory :child_factor, :parent=>:factor do
    association :parent, :factory=>:factor_group
  end
end
