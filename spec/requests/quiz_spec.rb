# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'User in quiz partition:' do

  before :each do
    @project = FactoryGirl.create(:project)
    @user = FactoryGirl.create(:user, :project=>@project)
  end

  context 'view' do

    before :each do
      3.times do
        quiz = FactoryGirl.create(:quiz)
        report = FactoryGirl.create(:report, :quiz=>quiz)
        ProjectQuiz.create(:project=>@project, :quiz=>quiz, :report=>report)
      end
      loggin_as_person(@user)
    end

    scenario 'available quizzes' do
      visit quiz_root_path
      page.should have_selector("div.quiz", :count=>3)
      page.should have_content(Quiz.first.title)
      page.should have_selector(".start_quiz")
    end

    scenario 'passed and other quizzes' do
      FactoryGirl.create(:complete_user_quiz, :user=>@user, :quiz=>Quiz.last)
      visit quiz_root_path
      page.should have_selector("div.quiz", :count=>3)
      page.should have_selector(".start_quiz", :count=>2)
      page.should have_selector(".result", :count=>1)
      page.should have_selector(".clear_result", :count=>1)
    end

    scenario 'as remote and with forward quiz' do
      Quiz.first.update_attribute(:forward, true)
      visit remote_path
      page.should have_selector("div.quiz", :count=>3)
      page.should have_selector(".start_quiz_new_window", :count=>1)
    end

  end

  context 'process as normal user' do
    before :each do
      @quiz = FactoryGirl.create(:quiz)
      @report = FactoryGirl.create(:report, :quiz=>@quiz)
      @project_quiz = ProjectQuiz.create!(:project=>@project, :quiz=>@quiz, :report=>@report)
      loggin_as_person(@user)
      visit quiz_root_path
    end

    context "start quiz" do

      scenario 'with instruction' do
        @project_quiz.update_attribute(:escape_instruction, false)
        first("a.start_quiz").click
        page.current_path.should == quiz_instruction_path(@quiz)
        page.should have_content(@project_quiz.instruction)
        UserQuiz.get(@user, @quiz).should_not be
        first("a.run_quiz").click
        page.current_path.should == quiz_run_path(@quiz)
        UserQuiz.get(@user, @quiz).should be
      end

      scenario 'without instruction' do
        @project_quiz.update_attribute(:escape_instruction, true)
        first("a.start_quiz").click
        page.current_path.should == quiz_run_path(@quiz)
        UserQuiz.get(@user, @quiz).should be
      end

      scenario 'with completed quiz' do
        FactoryGirl.create(:complete_user_quiz, :user=>@user, :quiz=>@quiz)
        first("a.start_quiz").click
        page.current_path.should == quiz_root_path
        %w{instruction run complete}.each do |action|
          visit "/quiz/#{action}/#{@quiz.id}"
          page.current_path.should == quiz_root_path
          page.should have_content("Вы уже заполнили этот опрос")
        end
      end
    end

    context 'pass quiz', :driver=>:selenium_chrome do

      before :each do
        @quiz.questions << Factory.build(:instruction_question, :quiz=>nil,
          :instruction=>%q{Quiz was created for test purpose})
        3.times{@quiz.questions << Factory.build(:question_variant, :quiz=>nil, :view_type=>'radio')}
        @quiz.questions << Factory.build(:divider_question, :quiz=>nil)
        @quiz.questions << Factory.build(:instruction_question, :quiz=>nil,
          :instruction=>"Take a breath and prepare to couple of text question.
            They doesn't rated at all. Feel free when answer them")
        @quiz.questions << Factory.build(:open_question, :quiz=>nil)
        @quiz.questions << Factory.build(:divider_question, :quiz=>nil)
        3.times{@quiz.questions << Factory.build(:range_question, :quiz=>nil)}
        @quiz.questions << Factory.build(:divider_question, :quiz=>nil)
        2.times{@quiz.questions << Factory.build(:scale_question, :quiz=>nil)}
        @quiz.questions << Factory.build(:open_question, :quiz=>nil,
          :instruction=>%q{Left suggestions and questions about quiz and site})
        @quiz.update_attribute(:kind, 'test')
        @project_quiz.update_attributes(:escape_instruction=>true,
          :announce=>%q{That quiz you must pass without errors},
          :epilog=>"Thanks for ending test. We hope you like it.",
          :show_report=>true)#run quiz at first click
      end

      after :each do
        page.should_not have_selector("li.question")
        page.should_not have_content("Вы не  ответили на следующие вопросы:")
        page.current_path.should == quiz_result_path(@quiz)
        page.should have_content(@project_quiz.epilog)
      end

      context "normal scenario" do

        before :each do
          @quiz.update_attributes(:custom_pagination=>false, :limited_in_time=>false, :page_size=>6)
          first("a.start_quiz").click
          page.should have_selector("div#navigation")
          page.should have_selector("#navigation a.next")
          page.should have_selector("li.question", :count=>6)

          page.should have_selector("div.variant", :count=>3)
          page.should have_selector("div.instruction", :count=>2)
          page.should have_selector("div.open", :count=>1)
        end

        scenario 'all answered' do

          #first page

          all("div.variant").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          within "div.open" do
            fill_in "open-answer", :with=>%q{Ok. just simple text}
          end
          # hook to mouse leave
          page.execute_script %q{$('div.open textarea').mouseleave()}
          wait_until {sleep 1}

          next_quiz_page
          #page 2

          page.should have_selector("div.range", :count=>3)
          page.should have_selector("div.scale", :count=>2)
          page.should have_selector("div.open", :count=>1)

          all("div.range").each do |question_node|
            question_node.all("li.sortable-variant").last.drag_to question_node.first("li.sortable-variant")
            wait_until {sleep 1}
          end

          all("div.scale").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          within "div.open" do
            fill_in "open-answer", :with=>%q{Everything was done}
          end
          # hook to mouse leave
          page.execute_script %q{$('div.open textarea').mouseleave()}
          wait_until {sleep 1}

          complete_quiz

          # see answer score in question factory
          Answer.where(:question_id=>Questions::Range.values_of(:id)).all?{|answer| answer.value==3}.should be_true
          Answer.where(:question_id=>Questions::Variant.values_of(:id)).all?{|answer| (1..4).include?(answer.value)}.should be_true
          Answer.where(:question_id=>Questions::Scale.values_of(:id)).all?{|answer| (-4..4).include?(answer.value)}.should be_true

        end

        scenario 'some required question is not answered' do
          #first page
          next_quiz_page
          page.should have_selector("div.range", :count=>3)
          page.should have_selector("div.scale", :count=>2)
          page.should have_selector("div.open", :count=>1)

          all("div.range").each do |question_node|
            question_node.all("li.sortable-variant").last.drag_to question_node.first("li.sortable-variant")
            wait_until {sleep 1}
          end

          complete_quiz

          page.should have_content("Вы не  ответили на следующие вопросы:")
          page.should have_selector("li.question", :count=>7)

          all("div.variant").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          within "div.open" do
            fill_in "open-answer", :with=>%q{Ok. just simple text}
          end
          # hook to mouse leave
          page.execute_script %q{$('div.open textarea').mouseleave()}
          wait_until {sleep 1}

          all("div.scale").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          within "#question-#{Questions::Open.last.id} div.open" do
            fill_in "open-answer", :with=>%q{Everything was done}
          end
          # hook to mouse leave
          page.execute_script %q{$('div.open textarea').mouseleave()}
          wait_until {sleep 1}

          complete_quiz

          # see answer score in question factory
          Answer.where(:question_id=>Questions::Range.values_of(:id)).all?{|answer| answer.value==3}.should be_true
          Answer.where(:question_id=>Questions::Variant.values_of(:id)).all?{|answer| (1..4).include?(answer.value)}.should be_true
          Answer.where(:question_id=>Questions::Scale.values_of(:id)).all?{|answer| (-4..4).include?(answer.value)}.should be_true

        end

        scenario 'some question is not answered' do
          Questions::Open.update_all(:required=>false, :profile=>false)
          Questions::Scale.update_all(:required=>false, :profile=>false)
          #first page
          next_quiz_page
          page.should have_selector("div.range", :count=>3)
          page.should have_selector("div.scale", :count=>2)
          page.should have_selector("div.open", :count=>1)

          page.should have_selector("sup.marked", :count=>3)

          all("div.range").each do |question_node|
            question_node.all("li.sortable-variant").last.drag_to question_node.first("li.sortable-variant")
            wait_until {sleep 1}
          end

          complete_quiz

          page.should_not have_selector("div.open")

          page.should have_content("Вы не  ответили на следующие вопросы:")
          page.should have_selector("li.question", :count=>3)

          all("div.variant").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          complete_quiz

          # see answer score in question factory
          Answer.where(:question_id=>Questions::Range.values_of(:id)).all?{|answer| answer.value==3}.should be_true
          Answer.where(:question_id=>Questions::Variant.values_of(:id)).all?{|answer| (1..4).include?(answer.value)}.should be_true
          Answer.where(:question_id=>Questions::Scale.values_of(:id)).all?{|answer| answer.value.nil? }.should be_true
        end

      end

      context "custom pagination" do

        before :each do
          @quiz.update_attributes(:custom_pagination=>true, :limited_in_time=>false, :page_size=>6)
          first("a.start_quiz").click
          page.should have_selector("div#navigation")
          page.should have_selector("#navigation a.next")
          page.should have_selector("li.question", :count=>4)
          page.should have_selector("div.variant", :count=>3)
          page.should have_selector("div.instruction", :count=>1)
        end

        scenario 'everything answer' do
          all("div.variant").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          next_quiz_page

          page.should have_selector("li.question", :count=>2)
          page.should have_selector("div.instruction", :count=>1)
          page.should have_selector("div.open", :count=>1)

          within "div.open" do
            fill_in "open-answer", :with=>%q{Ok. just simple text}
          end
          # hook to mouse leave
          page.execute_script %q{$('div.open textarea').mouseleave()}
          wait_until {sleep 1}

          next_quiz_page

          page.should have_selector("li.question", :count=>3)
          page.should have_selector("div.range", :count=>3)

          all("div.range").each do |question_node|
            question_node.all("li.sortable-variant").last.drag_to question_node.first("li.sortable-variant")
            wait_until {sleep 1}
          end

          next_quiz_page

          page.should have_selector("li.question", :count=>3)
          page.should have_selector("div.scale", :count=>2)
          page.should have_selector("div.open", :count=>1)

          all("div.scale").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          within "div.open" do
            fill_in "open-answer", :with=>%q{Everything was done}
          end
          # hook to mouse leave
          page.execute_script %q{$('div.open textarea').mouseleave()}
          wait_until {sleep 1}

          complete_quiz

          Answer.where(:question_id=>Questions::Range.values_of(:id)).all?{|answer| answer.value==3}.should be_true
          Answer.where(:question_id=>Questions::Variant.values_of(:id)).all?{|answer| (1..4).include?(answer.value)}.should be_true
          Answer.where(:question_id=>Questions::Scale.values_of(:id)).all?{|answer| (-4..4).include?(answer.value)}.should be_true
        end

        scenario 'some profile question left' do
          Questions::Open.update_all(:profile=>true)
          Questions::Range.update_all(:profile=>true)

          all("div.variant").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          next_quiz_page

          page.should have_selector("li.question", :count=>2)
          page.should have_selector("div.instruction", :count=>1)
          page.should have_selector("div.open", :count=>1)

          next_quiz_page

          page.should have_selector("li.question", :count=>3)
          page.should have_selector("div.range", :count=>3)

          first("div.range").all("li.sortable-variant").last.drag_to first("div.range").first("li.sortable-variant")
          wait_until {sleep 1}
          next_quiz_page

          page.should have_selector("li.question", :count=>3)
          page.should have_selector("div.scale", :count=>2)
          page.should have_selector("div.open", :count=>1)

          all("div.scale").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          within "div.open" do
            fill_in "open-answer", :with=>%q{Everything was done}
          end
          # hook to mouse leave
          page.execute_script %q{$('div.open textarea').mouseleave()}
          wait_until {sleep 1}

          complete_quiz

          page.should have_content("Вы не  ответили на следующие вопросы:")
          page.should have_selector("li.question", :count=>3)
          page.should have_selector("div.range", :count=>2)
          page.should have_selector("div.open", :count=>1)

          within "div.open" do
            fill_in "open-answer", :with=>%q{Everything was done}
            # hook to mouse leave
            page.execute_script %q{$('div.open textarea').mouseleave()}
            wait_until {sleep 1}
          end

          complete_quiz

          page.should have_content("Вы не  ответили на следующие вопросы:")
          page.should have_selector("li.question", :count=>2)
          page.should have_selector("div.range", :count=>2)

          all("div.range").each do |question_node|
            question_node.all("li.sortable-variant").last.drag_to question_node.first("li.sortable-variant")
            wait_until {sleep 1}
          end

          complete_quiz

          Answer.where(:question_id=>Questions::Range.values_of(:id)).all?{|answer| answer.value==3}.should be_true
          Answer.where(:question_id=>Questions::Variant.values_of(:id)).all?{|answer| (1..4).include?(answer.value)}.should be_true
          Answer.where(:question_id=>Questions::Scale.values_of(:id)).all?{|answer| (-4..4).include?(answer.value)}.should be_true

        end

        scenario 'some questions is left' do
          Questions::Range.update_all(:required=>false, :profile=>false)
          Questions::Open.update_all(:required=>false, :profile=>false)

          next_quiz_page
          page.should have_selector("div.open sup.marked", :count=>1)
          next_quiz_page
          page.should have_selector("div.range sup.marked", :count=>3)
          next_quiz_page

          complete_quiz

          page.should have_content("Вы не  ответили на следующие вопросы:")
          page.should have_selector("li.question", :count=>5)
          page.should have_selector("div.scale", :count=>2)
          page.should have_selector("div.variant", :count=>3)

          all("div.scale").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          complete_quiz

          page.should have_content("Вы не  ответили на следующие вопросы:")
          page.should have_selector("li.question", :count=>3)
          page.should have_selector("div.variant", :count=>3)

          all("div.variant").each do |question_node|
            question_node.all("input[type=radio]").sample.click
            wait_until {sleep 1}
          end

          complete_quiz

          Answer.where(:question_id=>Questions::Range.values_of(:id)).all?{|answer| answer.value.nil?}.should be_true
          Answer.where(:question_id=>Questions::Variant.values_of(:id)).all?{|answer| (1..4).include?(answer.value)}.should be_true
          Answer.where(:question_id=>Questions::Scale.values_of(:id)).all?{|answer| (-4..4).include?(answer.value)}.should be_true

        end

      end

      context "limited in time" do

        before :each do
          #set minimum 2 second, becouse some answer maybe not sent
          @quiz.update_attribute(:limited_in_time, 2)
        end

        scenario 'everything answer' do
          first("a.start_quiz").click
          page.should have_selector("div#navigation")
          page.should have_selector("#navigation a.next")
          page.should have_selector("li.question", :count=>1)

          page.should have_selector("div.instruction", :count=>1)

          next_quiz_page
          3.times do
            all("div.variant input[type=radio]").sample.click
            wait_until {sleep 1}
            next_quiz_page
          end

          page.should have_selector("div.instruction", :count=>1)
          wait_until{sleep 2}

          within "div.open" do
            fill_in "open-answer", :with=>%q{Everything was done}
            # hook to mouse leave
            page.execute_script %q{$('div.open textarea').mouseleave()}
            wait_until {sleep 1}
          end

          next_quiz_page

          3.times do
            all("div.range li.sortable-variant").last.drag_to first("div.range li.sortable-variant")
            wait_until {sleep 1}
            next_quiz_page
          end

          2.times do
            all("div.scale input[type=radio]").sample.click
            wait_until {sleep 1}
            next_quiz_page
          end

          within "div.open" do
            fill_in "open-answer", :with=>%q{Everything was done}
            # hook to mouse leave
            page.execute_script %q{$('div.open textarea').mouseleave()}
            wait_until {sleep 1}
          end
          complete_quiz
        end

        scenario 'some required fields left unanswered' do
          first("a.start_quiz").click

          page.should have_selector("div#navigation")
          page.should have_selector("#navigation a.next")
          page.should have_selector("li.question", :count=>1)
          page.should have_selector("div.instruction", :count=>1)

          Questions::Question.answerable.update_all(:profile=>true, :required=>true)

          next_quiz_page

          page.should_not have_selector("#navigation a.previous")

          wait_until{sleep 30}
        end
      end
    end
  end

end
