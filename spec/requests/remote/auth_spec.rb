# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'User register in iframe', :driver=>:selenium_chrome do

  scenario 'success' do
    user_stub = Factory.build(:user)
    visit remote_path
    fill_register_form user_stub, :remote=>true
    page.current_path.should == quiz_root_path
    user = User.first
    user.project.should == Project.default
    user.city.should == user_stub.city
    user.name.should == user_stub.name
    user.gender.should == user_stub.gender
    user.should_not be_custom
  end

  scenario "for project", :js do
    another_project = FactoryGirl.create(:project)
    user_stub = Factory.build(:user)
    visit remote_path(:project_id=>another_project.id)
    fill_register_form user_stub, :remote=>true
    page.current_path.should == quiz_root_path
    user = User.first
    user.project.should == another_project
    user.city.should == user_stub.city
    user.name.should == user_stub.name
    user.gender.should == user_stub.gender
    user.should_not be_custom
  end

  scenario 'fails' do
    visit remote_path
    click_on 'Готово'
    page.current_path == remote_auth_process_registration_path
    page.should have_selector(".field-with-error label", :text=>'Имя')
    page.should have_selector(".field-with-error label", :text=>'Дата рождения')
    page.should have_selector(".field-with-error label", :text=>'Email')
  end

  scenario 'auto login' do
    Project.default.update_attribute(:allow_anonim, true)
    visit remote_path
    page.current_path.should == quiz_root_path
    user = User.first
    user.project.should be
    user.should be_custom
  end

end
