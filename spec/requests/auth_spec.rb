# -*- encoding : utf-8 -*-
require 'spec_helper'

feature "Auth stuff:", :driver=>:selenium_chrome do

  before :each do
    5.times do
      [:city, :education_level, :job_area, :job_expirience,
        :job_position, :education_type].each do |property_type|
        FactoryGirl.create(property_type)
      end
    end
  end

  context "User register on site" do

    scenario 'success' do
      user_stub = Factory.build(:user, :project=>nil)
      visit auth_register_path
      fill_register_form(user_stub)
      current_path.should == quiz_root_path
      page.should have_selector('div', :text=>'Поздравляем с регистрацией на сайте!!!')
      accepted = User.find_by_login(user_stub.login)
      accepted.project.should == Project.default
      accepted.birthday.should == user_stub.birthday
      accepted.patronymic.should == user_stub.patronymic
      accepted.city.should == user_stub.city
    end

    scenario 'fails' do
      visit auth_register_path
      within "div.forms" do
        click_on 'Готово'
      end
      current_path.should == auth_process_registration_path
      page.should have_selector(".field-with-error label", :text=>'Логин')
      page.should have_selector(".field-with-error label", :text=>'Пароль')
      page.should have_selector(".field-with-error label", :text=>'Имя')
      page.should have_selector(".field-with-error label", :text=>'Пол')
      page.should have_selector(".field-with-error label", :text=>'Email')
    end

    # capybara webkit fails session
    scenario 'for special project' do
      another_project = FactoryGirl.create(:project)
      user = Factory.build(:user, :project=>nil)
      visit auth_register_path(:project_id=>another_project.id)
      fill_register_form(user)
      User.first.project.should == another_project
    end

  end

  context 'user edit profile' do

    before :each do
      @user = FactoryGirl.create(:user)
      loggin_as_person(@user)
    end

    scenario 'success' do
      visit auth_profile_path
      user_stub = Factory.build(:user, :name=>'Mike', :email=>'mike_bix@gmail.com')
      within 'div.forms' do
        fill_in 'user[name]', :with=>user_stub.name
        fill_in 'user[email]', :with=>user_stub.email
        select JobPosition.values_of(:title)[4], :from=> 'user_job_position_id'
        click_on 'Готово'
      end
      page.current_path.should == auth_profile_path
      page.should_not have_selector(".field-with-error label")
      page.should have_content("Изменения сохранены")
      @user.reload
      @user.name.should == user_stub.name
      @user.email.should == user_stub.email
      @user.job_position.should == JobPosition.offset(4).first
    end

    scenario 'fails' do
      visit auth_profile_path
      within 'div.forms' do
        fill_in 'user[name]', :with=>''
        click_on 'Готово'
      end
      page.current_path.should == auth_update_profile_path
      page.should have_selector(".field-with-error label", :text=>'Имя')
    end

    scenario 'as restricted' do
      @user.update_attribute(:restricted, true)
      visit auth_profile_path
      new_email = 'simon@mail.ru'
      within 'div.forms' do
        fill_in 'user[email]', :with=>new_email
        click_on 'Готово'
      end
      page.current_path.should == quiz_root_path
      page.should have_content("Изменения сохранены")
      @user.reload
      @user.email.should == new_email
    end

  end

end
