# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'user pass belbin quiz', :driver=>:selenium_chrome do

  before :each do
    @quiz = FactoryGirl.create(:belbin_quiz, :custom_pagination=>true)
    @user = FactoryGirl.create(:user)
    @project_quiz = FactoryGirl.create(:project_quiz, :quiz=>@quiz, :project=>@user.project,
      :escape_instruction=>true, :show_report=>true)
    factor_group = FactoryGirl.create(:factor_group, :quiz=>@quiz)
    5.times{FactoryGirl.create(:child_factor, :quiz=>@quiz, :parent=>factor_group)}
    @quiz.questions << Questions::Instruction.new(:instruction=>"Slide variants in your priority")
    @quiz.factors.reload
    2.times do
      @quiz.questions << Factory.build(:belbin_range, :quiz=>nil, :required=>false, :profile=>true)
      belbin_range = @quiz.questions.last
      @quiz.factors.each do |factor|
        belbin_range.belbin_items << Questions::BelbinItem.new(:instruction=>"Variant ##{factor.id}", :factor_ids=>[factor.id])
      end
    end
    @quiz.questions << Questions::Divider.new
    2.times do
      @quiz.questions << Factory.build(:belbin_range, :quiz=>nil, :required=>false, :profile=>false)
      belbin_range = @quiz.questions.last
      @quiz.factors.each do |factor|
        belbin_range.belbin_items << Questions::BelbinItem.new(:instruction=>"Variant ##{factor.id}", :factor_ids=>[factor.id])
      end
    end
    @quiz.questions.reload
    3.times do
      user_quiz = FactoryGirl.create(:complete_user_quiz, :quiz=>@quiz)
      Questions::BelbinItem.values_of(:id).each do |question_id|
        FactoryGirl.create(:answer, :question_id=>question_id, :user=>user_quiz.user)
      end
    end

    loggin_as_person(@user)


  end

  scenario 'all questions answered' do
    visit quiz_root_path
    find("a.start_quiz").click
    page.should have_selector("li.question", :count=>3)
    page.should have_selector("div.instruction", :count=>1)
    page.should have_selector("div.belbin-range", :count=>2)
    all("div.belbin-range").each do |question_node|
      question_node.all("li.sortable-variant")[3].drag_to question_node.all("li.sortable-variant")[2]
      question_node.all("li.sortable-variant")[0].drag_to question_node.all("li.sortable-variant")[4]
      question_node.all("li.sortable-variant")[4].drag_to question_node.all("li.sortable-variant")[1]
      # 0,4,1,3,2
      wait_until {sleep 1}
    end

    next_quiz_page

    page.should have_selector('li.question', :count=>2)
    page.should have_selector('div.belbin-range', :count=>2)

    all("div.belbin-range").each do |question_node|
      question_node.all("li.sortable-variant")[2].drag_to question_node.all("li.sortable-variant")[0]
      question_node.all("li.sortable-variant")[1].drag_to question_node.all("li.sortable-variant")[3]
      question_node.all("li.sortable-variant")[3].drag_to question_node.all("li.sortable-variant")[4]
      # 2,1,3,4,0
      wait_until {sleep 1}
    end

    complete_quiz

    page.should_not have_selector("li.question")
    page.should have_content(@project_quiz.epilog)

    @quiz.reload

    @user.answers.reload

    first_range_items = Questions::BelbinRange.first.belbin_items
    answer = [first_range_items[0].id, first_range_items[4].id, first_range_items[1].id,
      first_range_items[3].id, first_range_items[2].id].join ','
    @user.answers.where(:question_id=>Questions::BelbinRange.first.id).first.content.should == answer

    factors = @quiz.factors
    first_factor_sten = factors.first.sten(@user).to_i.to_s
    page.should have_selector("#factor_#{factors.first.id} .sten", :text=>first_factor_sten)
    page.should have_selector(".sten", :count=>5)
  end


  scenario 'some questions left and see report' do
    visit quiz_root_path
    find("a.start_quiz").click
    page.should have_selector("li.question", :count=>3)
    page.should have_selector("div.instruction", :count=>1)
    page.should have_selector("div.belbin-range", :count=>2)

    next_quiz_page

    page.should have_selector('li.question', :count=>2)
    page.should have_selector('div.belbin-range', :count=>2)

    complete_quiz

    page.should have_selector("li.question", :count=>2)
    page.should have_selector("div.belbin-range", :count=>2)

    all("div.belbin-range").each do |question_node|
      question_node.all("li.sortable-variant")[2].drag_to question_node.all("li.sortable-variant")[0]
      question_node.all("li.sortable-variant")[1].drag_to question_node.all("li.sortable-variant")[3]
      question_node.all("li.sortable-variant")[3].drag_to question_node.all("li.sortable-variant")[4]
      # 2,1,3,4,0
      wait_until {sleep 1}
    end

    complete_quiz

    page.should_not have_selector("li.question")
  end

end
