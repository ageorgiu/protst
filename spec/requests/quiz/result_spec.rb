# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'User view previous quiz result when' do

  before :each do
    @quiz = FactoryGirl.create(:quiz)
    @user = FactoryGirl.create(:user)
    loggin_as_person @user
    @project_quiz = FactoryGirl.create(:project_quiz, :project=>@user.project, :quiz=>@quiz,
      :announce=>%q{Quiz was build for test purpose. Its make any sence and that all},
      :epilog=>%q{
        <p class="main">
          Поздравляю вы прошли тест. Теперь Ваши данные были отправлены на обработку в
          наш центр, где наши <s>рабы</s> сотрудники будут их внимательно изучать
        </p>
        <p class='links'>
          Мы в социальных сетях:
          <br>
          <a href="twitter.com/prof-career.ru">Twitter</a>
          <a href="facebook.com/prof-career.ru">Facebook</a>
          <a href="vk.com/prof-career.ru">Vkontakte</a>
        </p>
      }
    )
    @report = @project_quiz.report
    FactoryGirl.create(:complete_user_quiz, :user=>@user, :quiz=>@quiz)
  end

  after :each do
    page.current_path.should == "/quiz/result/#{@quiz.id}"
    page.should have_selector("p.main")
    page.should have_selector("p.links")
    page.should have_selector("p.links a", :count=>3)
  end

  context "quiz is interview" do

    before :each do
      @quiz.update_attributes(:kind=>"interview")
    end

    scenario 'view epilog' do
      visit '/quiz'
      find("a.result").click
    end
  end

  context "quiz is test" do
    before :each do
      @quiz.update_attributes(:kind=>"test")
    end

    scenario 'user view statistic' do
      4.times do
        [:open_question, :question_variant, :scale_question, :range_question].each do |question_type|
          @quiz.questions << Factory.build(question_type, :quiz=>nil)
        end
      end

      @quiz.factor_groups << Factory.build(:factor_group, :quiz=>nil, :title=>"Factor group")

      @quiz.factor_groups.each_with_index do |group, group_id|

        5.times do |factor_id|
          group.children << Factory.build(:factor, :quiz=>@quiz, :title=>"Factor ##{factor_id+group_id+1}")
        end

        group.children.each do |factor|
          Questions::Question.answerable.order("RAND()").limit(4).values_of(:id).each do |question|
            QuestionFactor.create!(:factor=>factor, :question_id=>question)
          end
        end

      end

      another_users = 5.times.map{|i| FactoryGirl.create(:complete_user_quiz, :quiz=>@quiz).user}

      Questions::Question.answerable.each do |question|
        (another_users + [@user]).each {|user| FactoryGirl.create(:answer, :question=>question, :user=>user, :value=>rand(10))}
      end

      @quiz.reload
      @quiz.elaborate_statistics
      @project_quiz.update_attribute(:show_report, true)
      visit '/quiz'
      find("a.result").click
      page.should have_selector("table.chartlist", :count=>1)
      page.should have_selector(".chartlist tr", :count=>6)
      page.should have_selector("td.row")
      factors = Factor.first.children
      last_sten_text = factors.last.sten(@user).to_i.to_s
      page.should have_selector("#factor_#{factors.last.id} .sten", :text=>last_sten_text)
      first_sten_images = (5.5-factors.first.sten(@user).to_i).abs*2
      page.should have_selector("#factor_#{factors.first.id} img", :count=>first_sten_images)
    end

    scenario "statistic are hided" do
      @project_quiz.update_attribute(:show_report, true)
      visit '/quiz'
      find("a.result").click
      page.should_not have_selector("table.chartlist")
    end
  end

end
