# -*- encoding : utf-8 -*-
require 'spec_helper'

feature "Unautorized user visit front site" do

  scenario 'view home page' do
    visit root_path
    page.should have_content("Добро пожаловать на сайт prof-career.ru !!!")
    find('div#auth').should be
  end

  scenario 'view registration form' do
    visit auth_register_path
    page.should have_content('Заполните анкету')
    page.should have_selector('input', :name=>'user[password]', :type=>'password')
    page.should have_selector('input', :type=>'radio', :name=>'user[gender]')
    page.should have_selector('select', :name=>'user[city_id]')
  end

  scenario 'view profile page' do
    visit auth_profile_path
    page.should have_content('Вы должны зарегистрироваться или войти под своим логином')
    page.current_path == auth_register_path
  end

end
