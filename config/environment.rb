# -*- encoding : utf-8 -*-
# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

ProfCareer::Application.initialize!

ActionView::Base.field_error_proc = Proc.new do |html_tag, instance| 
  %{<span class="field-with-error">#{html_tag}</span>}.html_safe 
end
