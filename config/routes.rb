# -*- encoding : utf-8 -*-
ProfCareer::Application.routes.draw do
  

  mount_sextant if Rails.env.development?

  #mount ImperaviRails::Engine => "/imperavi"

  mount Ckeditor::Engine => '/ckeditor'

  get "admin/login" => "admin#login"
  get "admin_users/sign_in" => "admin#login"
  get "admin/reset_password" => "admin#reset_password"
  post "admin/reset_password_save" => "admin#reset_password_save"
  
  devise_for :admin_users

  root :to => 'home#index'

  namespace :auth do
    post '/login'
    get '/logout'
    get '/register'
    post '/process_registration'
    get '/profile'
    put '/update_profile'
  end

  namespace :remote do
    get '/'=>:index
    namespace :auth do
      get '/register'
      post '/process_registration'
      get '/user_generation'
      post '/login'
      get :profile
      put :update_profile
    end
  end

  namespace :ajax do
    get '/cities'
  end

  namespace :quiz do
    root :to=>:index
    get '/instruction/:id' => :instruction, :as => :instruction
    get '/clear_result/:id' => :clear_result, :as => :clear_result
    get '/result/:id' => :result, :as => :result
    get '/run/:id' => :run, :as => :run
    get '/complete/:id' => :complete, :as => :complete
    match '/answer/:id/:question_id' => :answer, :as => :answer
    match '/view_report'
    match '/download_report'
  end

  get "/admin" => "admin#index", :as=>:admin
  get "/admin/login" => "admin#login", :as => :admin_login
  match '/admin/questions/:action/:id' => 'questions#index'
  match '/admin/questions/:action/:id.:format' => 'questions#index'

  namespace :admin do

# leave that *** after back end changes
    resources :users do
      member do
        get :profile
      end
    end
    resources :projects do
      member do
        get :codes
        get :settings
        get :quizes
        get :move_users
        post :apply_move_users
        get :notice_history
        get :send_all
        get :send_choosed
        get :send_uncompleted
        post :process_send
        get :excel_export
        get :reports
      end
      resources :notices do
        member do
          get :prev
          get :copy
          get :message_text
        end
        collection do
          get :preview
        end
      end
      resources :users do
        collection do
          post :import
          get :import_dialog
        end
        member do
          get :move_to_project
        end        
        resources :quizzes do
          member do
            get :reset
            get :show_answers
            get :show_statistics
            get :export_answers
            get :show_report
            get :export_report
          end
        end  
      end
      resources :quiz do        
      end
      collection do
        get :all_users
        get :users
      end
    end 

    resources :quizzes do
      resources :profile_charachteristic do        
      end
      resources :factors do
      end
      resources :reports do
      end
      member do
        get :settings
        get :statistics
        get :set_period
        get :excel_export
        get :excel_reports
      end
      resources :questions do        
        collection do
          post :save_weights
          post :save_buffering
          post :remove          
        end
        member do
          get :buffering
          get :move
        end
      end
    end

    resources :settings
    resources :pages

    namespace :users do
      match "/"=>:index
      match '/new'
      match '/edit/:id'=>:edit
      match '/update/:id'=>:update
      match '/delete/:id'=>:destroy
      match "/list"
    end

    namespace :quiz do
      match '/'=>:index
      match '/new'
      match '/delete/:id'=>:destroy
      match '/edit/:id'=>:edit
      match '/update/:id'=>:update
      match '/list'
      match '/import'
    end

    namespace :factor do
      match '/new/:id'=>:new
      match '/update/:id'=>:update
      match '/edit/:id'=>:edit
      match '/reorder'
      match '/questions/:id'=>:questions
      match '/delete/:id'=>:destroy
      match '/correlation/:id'=>:correlation
    end

    namespace :report do
      match '/new/:id'=>:new
      match '/update/:id'=>:update
      match '/edit/:id'=>:edit
      match '/delete/:id'=>:destroy
    end

    namespace :interpretation do
      match '/new'
      match '/update/:id'=>:update
      match '/delete/:id'=>:destroy
    end

    namespace :statistics do
      match '/' => :index
      match '/view_report'
      match '/answers'
      match '/user'
      match '/answers'
      match '/export'
      match '/clear_statistic'
    end

    namespace :notice do
      match '/edit'
      match '/create_or_update'
      match '/process_window'
      match '/delete/:id'=>:destroy
      match '/notice_window_content'
    end

    # namespace :pages do
    #   match '/' => :index
    #   match '/new'
    #   match '/edit/:id'=>:edit
    #   match '/update/:id'=>:update
    #   match '/delete/:id'=>:delete
    #   match '/reorder'
    # end

    namespace :properties do
      match '/' => :index
      match '/add' => :add, :as => :add
      match '/view'
      match '/edit/:id'=>:edit, :as => :edit
      match '/update/:id'=>:update, :as => :update
      match '/delete/:id' => :delete, :as => :delete
      match '/reorder/' => :reorder, :as => :reorder
      match '/cities/' => :cities, :as => :cities
      match '/reorder'
      match '/load' => :load, as: :load
      match '/ajax_list_properties/:id'=>:ajax_list_properties
    end

    # namespace :settings do
    #   match '/'=>:index
    #   match '/edit/:id'=>:edit
    #   match '/update/:id'=>:update
    # end

    namespace :userquiz do
      match '/list_json'
      match '/excel_export'
    end
  end

end
