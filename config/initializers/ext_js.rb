# -*- encoding : utf-8 -*-
class ExtJS

  PAGE_SIZE = 20

  # Подготовка к JSON представлению данных, с учетом того, что объект acts_as_tree
  def self.json_tree_root(item, &block)
    children = item.children.map do |child|
      node = {}

      node[:id]       = child.id
      node[:children] = json_tree_root(child, &block)   unless child.children.blank?
      node[:leaf]     = true                       if     child.children.blank?

      node.merge(block.call(child))
    end
    return children
  end

  def self.json_tree_rootless(items, &block)
    list = items.map do |i|
      node = {}

      node[:id]       = i.id
      node[:children] = json_tree_root(i, &block)   unless i.children.blank?
      node[:leaf]     = true                        if     i.children.blank?

      node.merge(block.call(i))

    end

    return list
  end

  def self.json_checkbox_tree(item, &block)
    children = item.children.map do |child|
      node = block.call(child)

      node[:id]       = child.id
      node[:leaf]     = true                               if     child.children.blank?
      node[:children] = json_checkbox_tree(child, block)   unless child.children.blank?

      node
    end
    return children
  end

  # Иногда удобно какой-то список объектов представить в виде листьев дерева
  # Без childов и так далее. Например - список настроек
  def self.json_tree_collection(items, &block)
    children = items.map do |i|
      node        = {}

      node[:id]   = i.id
      node[:leaf] = true

      node.merge(block.call(i))
    end
    return children
  end


  # JSON для ExtJS Grid, преобразует коллекцию целиком
  def self.json_grid_collection(collection, &block)
    result = {}
    result[:total] = collection.length
    result[:items] = collection.map do |i|
      node       = block.call(i)
      node[:id]  = i.id

      node
    end

    result
  end

  # Данные для грида с динамическими столбцами
  # fields массив описаний колонок вида "[ {:name =>, :header => } ]"
  def self.json_dynamic_grid_collection(collection, fields, &block)
    result = ExtJS.json_grid_collection(collection) { |i| block.call(i) }
    result[:metaData ] = {
        :totalProperty  => 'total',
        :root           => 'items',
        :id             => 'id',
        :fields         => fields.map { |f| { :name => f.name, :type => "string" }  }
    }
    result[:columns]    = fields.map { |f| { :header => f.heade, :dataName => f.name} }
    result
  end

  def self.caption(item, p)
    "<span id='#{item.class.to_s.downcase}-#{item.id}-#{p}'>#{item.send(p)}</span>".html_safe
  end
end
