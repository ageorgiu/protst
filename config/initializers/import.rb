# -*- encoding : utf-8 -*-
#encoding: utf-8
require 'spreadsheet'
require 'digest/md5'

module Import
  module Position
    NAME       = 3
    SURNAME    = 2
    DAY        = 5
    MONTH      = 6
    YEAR       = 7

    ANSWERS    = 17
  end

  def read_excel_data(file)
    users = []
    Spreadsheet.client_encoding = 'UTF-8'

    workbook  = Spreadsheet.open file
    worksheet = workbook.worksheet(0)

    worksheet.each do |row|
      begin
        user = User.create(
          :name              => Unicode::capitalize(row.at(Position::NAME).to_s.strip),
          :surname           => Unicode::capitalize(row.at(Position::SURNAME).to_s.strip),
          :login             => "#{row.at(Position::SURNAME).to_s.strip}_#{row.at(Position::NAME).to_s.strip}".translify.downcase,
          :password          => Digest::MD5.hexdigest(row.at(Position::NAME).to_s)[0..8],
          :sex               => extract_sex(row.at(4)),
          :birthday          => extract_birthday(row.at(Position::DAY),row.at(Position::MONTH),row.at(Position::YEAR)),
          :phone             => row.at(15).to_s,
          :email             => row.at(16).to_s.downcase,
          :project_id        => extract_project(row.at(0).to_s),
          :education_level   => Property.find_or_create_id_by_title(row.at(9).to_s, Property::EDUCATION_LEVEL),
          :education_type    => Property.find_or_create_id_by_title(row.at(10).to_s, Property::EDUCATION_TYPE),
          :job_area          => Property.find_or_create_id_by_title(row.at(12).to_s, Property::JOB_AREA),
          :job_position      => Property.find_or_create_id_by_title(row.at(13).to_s, Property::JOB_POSITION),
          :city              => Property.find_or_create_id_by_title(row.at(8).to_s, Property::CITY),
          :job_expirience    => extract_job_expirience(row.at(11).to_s, row.at(14).to_s)
        )
      rescue
        puts "Failed to create user #{user.name}"
        next
      end

      unless user.valid? then
         p user.errors
      else
        users << user
        process_answers(user,row)
      end
    end

    return users
  end
private
  def extract_project(title)
    return 0 if title.blank?
    project =  Project.find_or_create_by_title(title.capitalize(:ru))
    return project.id
  end

  def extract_sex(g)
    return User::UNKNOWN if g.blank?
    return User::MALE    if ["Муж","Мужской"].include?(g.capitalize(:ru))
    return User::FEMALE  if ["Жен","Женский"].include?(g.capitalize(:ru))
    return User::UNKNOWN
  end

  def extract_birthday(day,month,year)
    return nil if (day.blank? || month.blank? || year.blank?)
    month_idx = ["Январь","Февраль","Март","Апрель","Май", "Июнь",
      "Июль",  "Август", "Сентябрь","Октябрь","Ноябрь","Декабрь"].index(month.capitalize(:ru))
    return nil if month_idx.blank?
    month_idx += 1

    return Date.new(year.to_i, month_idx, day.to_i)
  end

  def extract_job_expirience(has_job_expirience, job_expirience)
    property_id = Property.find_or_create_id_by_title(job_expirience, Property::JOB_EXPIRIENCE)
    if(property_id == 0) then
      property_id = Property.find_or_create_id_by_title(has_job_expirience, Property::JOB_EXPIRIENCE)
    end
    return property_id
  end

  def process_answers(user,row)
    u_q        = UserQuiz.create( :user_id => user.id, :quiz_id => self.id  )
    has_fails  = false
    u_q.start!
    fails      = []
    questions = self.answerable_questions

    Answer.delete_all(['question_id IN (?) AND user_id = ?', questions.map{ |q| q.id }, user.id ])

    questions.each_with_index do |q,idx|
      str = row.at(Position::ANSWERS + idx).to_s
      begin
        Answer.create(
          :user_id => user.id, :question_id => q.id,
          :raw     => q.variant_value(str.capitalize(:ru))
        )
      rescue
        fails <<  "Failed at: #{user.login}, #{str}, #{Position::ANSWERS + idx }"
        has_fails = true
      end
    end
    unless has_fails
      u_q.complete!
    else
      p fails
    end
  end
end
