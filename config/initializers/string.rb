# -*- encoding : utf-8 -*-
String.class_eval do

  [:downcase, :capitalize, :upcase].each do |method_name|

    #like net after each operation with string return new class
    alias_method "original_#{method_name}", method_name

    define_method(method_name) do |*args|
      (args.first and args.first.is_a?(Symbol)) ? mb_chars.send(method_name).to_s : send("original_#{method_name}")
    end
  end

end
