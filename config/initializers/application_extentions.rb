# -*- encoding : utf-8 -*-
ActiveRecord::Base.instance_eval do

  def find_ordered_array(array)
    if array.is_a? Array
      unordered_array = find(array)
      array.inject([]) {|order_items, id| order_items << unordered_array.find{|item| item.id == id.to_i}}.compact
    else
      []
    end
  end

end


Array.class_eval do
  def json_grid_collection
    {:total=> size, :items=> collect{|item| item.to_json}}
  end
end
