# -*- encoding : utf-8 -*-

# $:.unshift(File.expand_path('./lib', ENV['rvm_path'])) 

require "rvm/capistrano" 

require 'bundler/capistrano'

require 'capistrano/ext/multistage'# добавит возможности создавать ветки

require 'capistrano_colors'#добавит радости от просмотра процесса публикации

default_run_options[:pty] = true

ssh_options[:forward_agent] = true

set :using_rvm, true

set :rvm_type, :system

set :scm, 'git'#указывает формат хранения кода

set :scm_verbose, true

set :git_enable_submodules, 1

set :deploy_via, :remote_cache

set :use_sudo, false

after "deploy:update", "deploy:export_symlink"
after "deploy:update", "deploy:cleanup"
after "deploy:update", "deploy:delayed"
after "deploy:update", "deploy:assets_symlink"
after "deploy:setup", "deploy:after_setup"

set :keep_releases, 4

set :bundle_without, [:test, :development]

set :default_stage, 'demo'

namespace :deploy do
  task :start do ; end 
  task :stop do ; end

  desc "Restart Application"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{current_release}/tmp/restart.txt"
  end

  desc "Output 100 last lines of the log"
  task :log, :roles => [:app, :db, :web] do
    run "tail #{current_path}/log/production.log -n 50"
  end

  desc "Create symlink for export directory"
  task :export_symlink, roles: [:app, :db, :web] do
    run "ln -nfs #{shared_path}/reports #{current_release}/public"
  end

  desc "Create symlink for assets directory"
  task :assets_symlink, roles: [:app, :db, :web] do
    run "ln -nfs #{shared_path}/assets #{current_release}/public"
  end

  desc "Start Delayed Jobs"
  task :delayed, :roles => [:app, :db, :web] do
    run "cd #{current_path}; RAILS_ENV=#{rails_env} script/delayed_job restart"
  end

  task :after_setup, roles: [:app, :db, :web] do
    run "mkdir #{shared_path}/reports"
  end
end
