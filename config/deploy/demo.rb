# -*- encoding : utf-8 -*-
set :branch, "new_stable"

set :application, 'quizconstruct-demo'
set :deploy_server,   "neon.locum.ru"

set :bundle_without,  [:development, :test]

set :user,            "hosting_ageorgiu"
set :login,           "ageorgiu"
set :use_sudo,        false
set :deploy_to,       "/home/#{user}/projects/#{application}"
set :unicorn_conf,    "/etc/unicorn/#{application}.#{login}.rb"
set :unicorn_pid,     "/var/run/unicorn/#{application}.#{login}.pid"
set :bundle_dir,      File.join(fetch(:shared_path), 'gems')
role :web,            deploy_server
role :app,            deploy_server
role :db,             deploy_server, :primary => true


set :rvm_ruby_string, "1.9.3"

set :rake,            "rvm use #{rvm_ruby_string} do bundle exec rake" 
set :bundle_cmd,      "rvm use #{rvm_ruby_string} do bundle"
set :repository,    "git@bitbucket.org:vladvinnikov/protst.git"

after "deploy:update_code", 'deploy:run_after_finalize_update'

before 'deploy:finalize_update', 'set_current_release'
task :set_current_release, :roles => :app do
  set :current_release, latest_release
end


# after('deploy:symlink', 'assets:precompile')
# 
# namespace :assets do
#   desc 'Precompile assets'
# 
#   task :precompile, :roles => :app do
# 
#     run "cd #{deploy_to}/current; rvm use #{rvm_ruby_string} do bundle exec rake assets:precompile"
# 
#   end
# end



set :unicorn_start_cmd, "(cd #{deploy_to}/current; rvm use #{rvm_ruby_string} do bundle exec unicorn_rails -Dc #{unicorn_conf})"

namespace :deploy do
  desc "Copy production database configuration"
  task :run_after_finalize_update, :roles => [:app, :db, :web] do
    #copy config
    run "cp #{release_path}/config/deploy/demo/database.yml #{release_path}/config/database.yml"
  end
  
  desc "Start application"
  task :start, :roles => :app do
    run unicorn_start_cmd
  end

  desc "Stop application"
  task :stop, :roles => :app do
    run "[ -f #{unicorn_pid} ] && kill -QUIT `cat #{unicorn_pid}`"
  end

  desc "Restart Application"
  task :restart, :roles => :app do
    run "[ -f #{unicorn_pid} ] && kill -USR2 `cat #{unicorn_pid}` || #{unicorn_start_cmd}"
  end
end