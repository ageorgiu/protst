# -*- encoding : utf-8 -*-
class Remote::AuthController < RemoteController
  expose(:project){session[:project_id].present? ? Project.find(session[:project_id]) : Project.default}
  
  def register
    # binding.pry
    if project.allow_anonim?
      redirect_to remote_auth_user_generation_path @remote_params
    end
  end

  def login
    # binding.pry
    user = User.find_by_login_and_password(params['login'],params['password'])
    if user
      store_person(user)
      redirect_to quiz_root_path @remote_params
    else
      redirect_to remote_auth_register_path, alert: 'Неправильный логин или пароль'
    end
  end

  def user_generation
    user = User.generate
    project.users << user
    store_person(user)
    redirect_to quiz_root_path @remote_params
  end

  def process_registration
    User.generate(params[:user], current_person)
    current_person.custom = false
    current_person.project = nil

    if project.users << current_person
      store_person(current_person)

      if project.notices.where(title: "registration").present?
        template = project.notices.where(title: "registration").first
        
        t = NoticeRenderer::Template.new
        t.template = template.content
        t.set_user(current_person)
        content = t.render()

        UserMail.register(current_person, template.theme, content).deliver
      end
      redirect_to quiz_root_path(@remote_params), flash: { notice: "#{I18n.t('remote.register')}" }
    else
      render :register
    end
  end

  def profile;end

  def update_profile
    if current_person.update_attributes(params[:user])
      redirect_to quiz_root_path(@remote_params), notice: 'Изменения сохранены'
    else
      render :profile
    end
  end

end
