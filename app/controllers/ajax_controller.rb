# -*- encoding : utf-8 -*-
class AjaxController < ApplicationController

  respond_to :json

  def cities
    cities = City.title_like(params[:query]).limit(10)
    respond_with({:query=>params[:query], :suggestions=>cities.map(&:title), :data=>cities.map(&:id)})
  end

end
