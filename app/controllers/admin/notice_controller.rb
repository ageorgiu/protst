# -*- encoding : utf-8 -*-
class Admin::NoticeController < AdminController

  def edit
  end

  def create_or_update
  end

  def process_window
    if(params['notice-scope'] == "save-or-create") then
      if (params[:id].blank?) then
        @notice = Notice.create(params[:notice])
      else
        @notice = Notice.find_by_id(params[:id])
        @notice.update_attributes(params[:notice])
      end
      render :update do |page|
        page.replace_html 'notice-message-content', :partial => 'editor'
      end
      return
    elsif params['notice-scope'] == "load-template" then
      if (params[:notice][:template].blank?) then
        @notice = Notice.new
      else
        @notice = Notice.find_by_id(params[:notice][:template])
      end
      render :update do |page|
        page.replace_html 'notice-message-content', :partial => 'editor'
      end
      return
    elsif params['notice-scope'] == "delivery" then
      user_ids = params[:users].try(:values) || []
      User.find(user_ids).each do |user|
        Notifier.invite(user,params[:notice][:title], params[:notice][:content]).deliver if user.email =~ /\A\w+@\w+\.\w+$/
        #Notifier.deliver_invite_copy(user,params[:notice][:title], params[:notice][:content])
      end
      render :update do |page|
        page << "noticeWindow.hide();"
      end
    end
  end

  def destroy
    @notice = Notice.find_by_id(params[:id])
    @notice.destroy
    @notice = nil
    render :update do |page|
        page.replace_html 'notice-message-content', :partial => 'editor'
    end
  end

  def notice_window_content
    if(params[:word].blank?) then
      @users = User.all(:order => 'surname, name')
    else
      @users = User.find(:all, :conditions => [ 'LOWER(name) LIKE ? OR LOWER(surname) LIKE ? OR LOWER(patronymic) LIKE ? OR LOWER(projects.title) LIKE ?',
        "#{params[:word]}%","#{params[:word]}%", "#{params[:word]}%", "#{params[:word]}%"
        ], :include => 'project', :order => 'surname, name')
    end
    project_id = (params[:project_id] || 0).to_i
    if project_id > 0 then
      @users = @users.select { |u| u.project_id == project_id }
    end
    @projects = Project.values_of(:title, :id)
    render :layout => false
  end
end
