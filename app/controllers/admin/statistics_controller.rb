# -*- encoding : utf-8 -*-
class Admin::StatisticsController < AdminController
  before_filter :init, :only => [ :index ]

  expose(:report) {Report.find(params[:report_id])}

  def index
  end

  def user
    @quiz           = Quiz.includes(:reports).find(params[:quiz_id])
    @user           = User.find(params[:user_id])
    @user_quiz      = UserQuiz.get(@user, @quiz)

    if (@user_quiz.complete?)
      @answers = @quiz.answers_for(@user)
      @unanswered_questions = @quiz.questions.not_answered_by(@user)
      render :update do |page|
        page.replace_html 'answers-area',    :partial => 'user_answers'
        page.replace_html 'statistics-area', :partial => 'user_statistics'
        page.replace_html 'report-area',     :partial => 'user_report'
      end
    else
      render :update do |page|
        page.replace_html 'answers-area',    ''
        page.replace_html 'statistics-area', ''
        page.replace_html 'report-area',     ''
      end
    end
  end

  def answers

    @quiz           = Quiz.find(params[:quiz_id])
    @user           = User.find(params[:user_id])
    @answers        = @quiz.answers_for(@user)
    @unanswered_questions = @quiz.questions.not_answered_by(@user)

  end

  def export
  end



  def view_report
    @quiz           = Quiz.find_by_id(params[:quiz_id])
    @user           = User.find_by_id(params[:user_id])
  end

  def clear_statistic
    Questions::Question.where(quiz_id: params[:quiz_id]).each do |question|
      Answer.where(user_id: params[:user_id], question_id: question.id).each do |a| a.destroy end
    end
    UserQuiz.where(quiz_id: params[:quiz_id], user_id: params[:user_id]).each do |q| q.destroy end
    
    redirect_to root_path
    return
  end
private
  def init
    @controller_info = {
      :section => 'statistics',
      :quiz    => ExtJS.json_tree_collection(Quiz.all) do |quiz|
        { :text => ExtJS.caption(quiz, 'title' ) }
      end,
      :project => ExtJS.json_tree_collection(Project.all) do |project|
        { :text => ExtJS.caption(project, 'title' ) }
      end
    }
  end

end
