# -*- encoding : utf-8 -*-
class Admin::UserquizController < AdminController

  include Admin::UserquizHelper

  respond_to :xls, :only=>:excel_export

  def list_json

    users = if params[:word].present?
      User.order_by_name.find_by_word(params[:word])
    elsif params[:project_id].present?
      User.order_by_name.by_project(params[:project_id])
    else
      User.order_by_name
    end

    columns = [
      { :header => "Кандидат" , :name => 'title'},
      { :header => "email",     :name => 'email'},
      { :header => "Проект",    :name => 'project'}
    ] + user_quizzes_columns


    users = json_dynamic_grid_collection(users, columns) do |user|
      user_info = {
        :project         => user.project.blank? ? '' : user.project.title,
        :title           => user.title,
        :email           => user.email
      }
      user_quizzes = user_quizzes_status(user)
      user_info.merge!(user_quizzes)

      user_info
    end

    render :json => users
  end

  def excel_export
    @quizzes = Quiz.all
    if params[:word]
      @users = User.includes(:project, :city, :education_level, :job_area, :job_expirience,
        :job_position, :education_type).find_by_word(params[:word])
    elsif params[:project_id]
      @users = User.includes(:project, :city, :education_level, :job_area, :job_expirience,
        :job_position, :education_type).where("users.project_id = ?", params[:project_id])
    else
      @users = User.includes(:project, :city, :education_level, :job_area, :job_expirience,
        :job_position, :education_type)
    end
    respond_with @quizzes
  end
end
