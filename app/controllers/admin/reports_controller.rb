# -*- encoding : utf-8 -*-
class Admin::ReportsController < AdminController
	respond_to :html, :js

	before_filter :find_quiz
	before_filter :fetch_collections	
	before_filter :find_factor, only: [:show, :update, :destroy]

	def index; end

	def new
		@report = Report.new
		respond_with(@report)
	end

	def create		
		@report = Report.new(params[:report])		
		@report.quiz = @quiz
		@report.save
		redirect_to admin_quiz_report_path(@quiz, @report), notice: "Отчет успешно добавлен!"
	end

	def update
		@report.update_attributes(params[:report])
		respond_with(@report)
	end

	def destroy
		@report.destroy
		redirect_to admin_quiz_reports_path(@quiz), notice: "Отчет успешно удален!"
	end

	private

	def find_quiz
		@quiz = Quiz.find(params[:quiz_id])
	end

	def find_factor
		@report = Report.find(params[:id])
	end

	def fetch_collections
		@quizzes = Quiz.all
		@reports = @quiz.reports
	end
end
