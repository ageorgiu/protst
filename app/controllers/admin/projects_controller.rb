# -*- encoding : utf-8 -*-
#encoding: utf-8
class Admin::ProjectsController < AdminController

  respond_to :html, :js

  before_filter :init, :except => [ :index, :new, :create, :users, :all_users ]
  before_filter :get_projects, :only => [:index, :show, :users, :all_users, :notice_history]
  before_filter :set_section

  def index
    
  end

  def reports
    render :action => "leave", :layout => false
  end

  def show
    @users = @project.users
  end

  def new
    @project = Project.new
    respond_with(@project)
  end

  def create
    @project = Project.create(params[:project])    
    @project.save if @project.valid?
    respond_with(@project)
  end

  def edit
    
  end

  def update
    # binding.pry
    @project.update_attributes(params[:project])
    respond_with(@project)
  end

  def destroy
    
  end

  def codes
    respond_with(@project)
  end

  def settings
    respond_with(@project)
  end

  def quizes
    respond_with(@project)
  end

  def all_users
    @active = 1
    @users = User.all
    @project = Project.default
    @all_users = true
    render "admin/users/index"
  end

  def users
    @active = 1
    render "admin/users/index"
  end
  
  def move_users
    @ids = params[:choosed]
    respond_with(@ids)    
  end

  def apply_move_users
    # binding.pry
    @new_proj = Project.find(params[:user][:project_id])
    @users = User.where(id: params[:user][:usr].map { |u| u[0].to_i })
    @users.update_all(project_id: @new_proj.id)
    respond_with(@users)
  end  

  def notice_history
    @project = Project.find(params[:id])
    @notices = @project.sended_notices.order("created_at desc")

    respond_with(@notices)
  end

  def send_all
    @users = @project.users
    @templates = @project.notices
    render :send_notice
  end

  def send_choosed
    # binding.pry
    @users = []
    @choosed = params[:choosed]
    @users = @choosed.collect { |c| User.find(c) } unless @choosed.nil?
    @templates = @project.notices
    render :send_notice
  end

  def send_uncompleted
    @users = @project.user_quizzes.uncompleted.collect { |uq| uq.user }
    @templates = @project.notices
    render :send_notice
  end

  def process_send
    # binding.pry
    @send = params[:send]
    template = Notice.find(@send[:template])
    user_ids = @send[:users]
    t = NoticeRenderer::Template.new
    t.template = template.content
    admin_email = Settings.get(:email)
    other_emails = @send[:admin_email].split(',')
    User.find(user_ids).each do |user|
      t.set_user(user)
      content = t.render()
      Notifier.invite(user, template.theme, content, template, @project).deliver
      Notifier.invite_copy_admin(admin_email, template.theme, content).deliver if @send[:send_copy] == "1"
      other_emails.each { |ae| Notifier.invite_copy_admin(ae, template.theme, content).deliver }
      # SendedNotice.create(user: user, project: @project, notice: template, text: "mail.body")
    end
    respond_with(@project)
  end

  def excel_export
    if params[:choosed].present?
      @users = @project.users.joins(:user_quizzes).where("user_quizzes.user_id in (?)", params[:choosed]).uniq
      @quizzes = @project.quizzes
    else
      @users = @project.users.joins(:user_quizzes).where("user_quizzes.complete = 1").uniq
      @quizzes = @project.quizzes.includes(:completed_users)
    end
    # binding.pry

    headers['Content-Type'] = "application/vnd.ms-excel"                                                                       
    headers['Content-Disposition'] = 'attachment; filename="users_'+ Time.now.strftime("%Y_%D_%H_%M") + '.xls"'                                                      
    headers['Cache-Control'] = ''
    
    render :layout =>false
  end

private 
  def init
    @project = Project.find(params[:id])
  end

  def get_projects
    @projects = Project.all
  end

  def set_section
    @section = "projects"
    @active = 2
  end

end
