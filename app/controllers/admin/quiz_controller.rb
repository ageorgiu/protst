# -*- encoding : utf-8 -*-
#encoding: utf-8
require 'ostruct'

class Admin::QuizController < AdminController
  include Admin::QuizHelper
#   expose(:quiz){Quiz.find(params[:id])}

#   before_filter :init, :only => [ :index ]

#   def index
#   end

#   def new
#     Quiz.create
#     redirect_to :action => 'index'
#   end

#   def destroy
#     quiz.destroy
#     redirect_to :action => 'index'
#   end

#   def edit
#     @questions = quiz.questions.editable
#     render :update do |page|
#       page.replace_html 'editor-tab', 'Редактор вопросов'
#       page.replace_html 'settings-area',    :partial => 'editor'
#       page.replace_html 'questions-area',   :partial => 'questions/questions_editor', :locals => { :editable => true }
#       page << "initUJS();"
#     end
#   end

#   def list
#     node = params[:node]
#     case node
#     when '0'             then list = quizzes
#     when /^\d+$/         then list = quiz_properties(node.to_i)                   # Выненсено в helper
#     when /factors\-\d+/  then list = quiz_factor_groups(node.split('-')[1].to_i)
#     when /factor\-\d+/   then list = quiz_factor_tree(node.split('-')[1].to_i)
#     when /reports\-\d+/ then list = quiz_reports(node.split('-')[1].to_i)
#     end
#     render :json => list
#   end



#   def update
#     flash[:message_saved] = quiz.update_attributes(params[:quiz]) ?  "Изменения сохранены #{Time.now.strftime("%H:%M")}" : quiz.errors.full_messages.join(' ')

#     render :update do |page|
#       page.replace_html 'settings-area', :partial => 'editor'
#       page.replace_html "quiz-#{quiz.id}-title",  quiz.title
#       page << "initUJS();"
#     end
#   end


#   def import
#     users = quiz.read_excel_data(params[:excel_data_file])
#     respond_to_parent do
#       render :update do |page|
#         page.replace_html 'import-status', "#{users.length} " + users.length.items("анекта","анкеты","анкет")
#       end
#     end
#   end
# private
#   def quizzes
#     quizzes = ExtJS.json_tree_collection(Quiz.all) do |quiz|
#       {
#         :text      => ExtJS.caption(quiz, 'title' ), :leaf => false,
#         :meta      => { :type => 'quiz', :id => quiz.id }
#       }
#     end
#     return quizzes
#   end

#   def quiz_factor_groups(quiz_id)
#     quiz    = Quiz.find_by_id(quiz_id)
#     factors = ExtJS.json_tree_rootless(quiz.factor_groups) { |f| factor_node(f) }
#     return factors
#   end

#   def quiz_factors_tree(factor_id)
#     factors = ExtJS.json_tree_root(Factor.find_by_id(factor_id)) { |f| factor_node(f) }
#   end

#   def quiz_reports(quiz_id)
#     quiz      = Quiz.find_by_id(quiz_id)
#     reports  = ExtJS.json_tree_collection(quiz.reports) { |c| report_node(c)}
#     return reports
#   end

#   def init
#     @controller_info = {
#       :section => "quiz"
#     }
#   end
end
