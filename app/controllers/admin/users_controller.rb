# -*- encoding : utf-8 -*-
#encoding: utf-8
require 'securerandom'
class Admin::UsersController < AdminController
  
  respond_to :html, :js

  before_filter :find_project, except: [:profile]
  before_filter :find_user, :only => [:edit, :update, :destroy, :move_to_project, :profile]
  before_filter :set_section

  def index
    # binding.pry
    @projects = Project.all
    @users = @project.users

    respond_to do |format|
      format.html
      format.csv {send_data @users.export_csv}
    end
  end

  def import_dialog
    
  end

  def import
    User.import(params[:file])
    @notice = '';
    redirect_to admin_project_users_path, notice: "#{@notice}"
  end
  
  def new
    @user = User.new
    @user.project = @project
    respond_with(@user)
  end
  
  def create
    @user = User.new(params[:user])
    @user.quick_registration = true
    @user.save if @user.valid?
    respond_with(@user)
  end

  def edit
    respond_with(@user)
  end

  def update 
    @user.update_attributes(params[:user])
    respond_with(@user)
  end

  def destroy    
    @del_id = @user.id
    @user.destroy
    respond_with(@del_id)
  end
  
  def move_to_project
    respond_with(@user)
  end

  def profile
    render layout: false    
  end

  private

  def set_section
    @section = params.has_key?(:section) ? params[:section] : "users"
  end

  def find_project
    @project = Project.find(params[:project_id])
    @active = 1
  end
  
  def find_user
    @user = User.find(params[:id])
  end

end
