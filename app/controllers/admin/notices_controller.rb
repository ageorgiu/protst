# -*- encoding : utf-8 -*-
class Admin::NoticesController < AdminController
	respond_to :html, :js

	before_filter :fill_variables
	before_filter :find_notice, only: [:edit, :update, :destroy, :copy, :prev]

	def index
		@projects = Project.all
		@notices = @project.notices
	end

	def new
		@is_project_section = params.has_key?(:section) ? params[:section] == "projects" : false
		@notice = Notice.new
		respond_with(@notice)
	end

	def create
		@is_project_section = params.has_key?(:section) ? params[:section] == "projects" : false
		@notice = Notice.new(params[:notice])
		@notice.project = @project
		@notice.save
		respond_with(@notice)
	end

	def prev
		respond_with(@notice)
	end

	def edit
		respond_with(@notice)
	end

	def update
		@notice.update_attributes(params[:notice])
		respond_with(@notice)
	end

	def destroy	
		@id = @notice.id
		@notice.destroy
		respond_with(@id)
	end

	def copy
		@notice = @notice.dup
		respond_with(@notice)
	end

	def preview
		unless params[:template_id].blank?
			@template = Notice.find(params[:template_id])
			t = NoticeRenderer::Template.new
    	t.template = @template.content
    	t.set_user(User.generate)
    	@content = t.render()
		end
		respond_with(@content)
  	end

  	def message_text
  		# binding.pry
  		@notice = SendedNotice.find(params[:sended_notice_id])
  		@content = @notice.text
  		respond_with(@content)
  	end

private

	def fill_variables
		@project = Project.find(params[:project_id])
	end

	def find_notice
		@notice = Notice.find(params[:id])
	end

end
