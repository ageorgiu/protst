# -*- encoding : utf-8 -*-
class Admin::FactorsController < AdminController
	respond_to :html, :js

	before_filter :find_quiz
	before_filter :fetch_collections	
	before_filter :find_factor, only: [:show, :update, :destroy]

	def index
	end

	def show
		@questions = @quiz.questions 
	end

	def new
		@factor = Factor.new
		respond_with(@factor)
	end

	def create		
		@factor = Factor.new(params[:factor])
		@factor.parent_id = 1
		@factor.quiz = @quiz
		@factor.save
		redirect_to admin_quiz_factor_path(@quiz, @factor), notice: "Фактор успешно добавлен!"
	end

	def update
		@factor.update_attributes(params[:factor])
		QuestionFactor.find(:all, :conditions => { :factor_id => @factor.id}).each { |qf| qf.destroy }
		unless params[:questions].nil?
	    params[:questions].each do |q|
	      QuestionFactor.create(:factor_id => @factor.id, :question_id => q )
	    end
	  end
    respond_with(@factor)
	end

	def destroy
		@factor.destroy
		redirect_to admin_quiz_factors_path(@quiz), notice: "Фактор успешно удален!"
	end

	private

	def find_quiz
		@quiz = Quiz.find(params[:quiz_id])
	end

	def find_factor
		@factor = Factor.find(params[:id])
	end

	def fetch_collections
		@quizzes = Quiz.all
		@factors = @quiz.factors
	end
end
