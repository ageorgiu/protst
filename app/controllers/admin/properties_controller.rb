# -*- encoding : utf-8 -*-
#encoding: utf-8
class Admin::PropertiesController < AdminController

  respond_to :html, :js

  before_filter :set_active
  before_filter :init, :only => [ :index ]
  
  def index
    @education = Property.education_levels.order :weight#.page params[:educationlevel_page]
    @education_types = Property.education_types.order :weight#.page params[:educationtype_page]
    @job_areas = Property.job_areas.order :weight#.page params[:jobarea_page]
    @job_expiriences = Property.job_expiriences.order :weight#.page params[:jobexpirience_page]
    @job_positions = Property.job_positions.order :weight#page params[:jobposition_page]
    @cities = []
  end
  
  def add    
    @property = Property.new(params[:property])    
    if @property.valid?
      expire_fragment(@property.type)
      @property.save
    end
    respond_with(@property)
  end

  def load
    @section = params[:section]    
    @collection = Property.send("#{@section.pluralize}")
    respond_with(@collection)
  end
  
  def view
 
  end
  
  def edit
    @property = Property.find(params[:id])
    respond_with(@property)
  end
  
  def update    
    @property = Property.find(params[:id])
    # binding.pry
    @property.update_attributes(params[@property.type.underscore])
    expire_fragment(@property.type)
    respond_with(@property)
  end
  
  def delete
    @property = Property.find(params[:id])
    @del_id = @property.id
    @del_type = @property.type
    @property.destroy
    expire_fragment(@property.type)
    respond_with(@property)
  end

  def reorder
    weights = params[:weights]
    weights.each do |id, weight|
      prop = Property.find(id)
      prop.weight = weight
      prop.save
    end
    # respond_with(weights)
  end

  def cities
    render :json => Proeprty.cities.map(&:title)
  end

  def ajax_list_properties
    properties = params[:id].constantize.page(params[:page])
    render :json => {
      :total        => properties.size, 
      :properties   => properties.map do |p| 
        { 
          :content     => p.title, 
          :id          => p.id
        }
      end
    }
  end
  private
    def init
      @controller_info = {
        :section => 'properties',
        :properties   => [
          { :text => 'Образование',             :id => 'EducationLevel',    :leaf => true },
          { :text => 'Тип образования',         :id => 'EducationType',     :leaf => true },
          { :text => 'Сфера деятельности',      :id => 'JobArea',           :leaf => true },
          { :text => 'Стаж работы',             :id => 'JobExpirience',     :leaf => true },
          { :text => 'Уровень позиции',         :id => 'JobPosition',       :leaf => true },
          { :text => 'Город',                   :id => 'City',               :leaf => true }
        ]
      }
    end

    def set_active
      @active = 3
    end
end
