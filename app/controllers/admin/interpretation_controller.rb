# -*- encoding : utf-8 -*-
class Admin::InterpretationController < AdminController

  def new
    factor  = Factor.find_by_id(params[:factor_id])
    report = Report.find_by_id(params[:report_id])

    Interpretation.create(
      :factor_id => factor.id, :report_id => report.id
    )
    render :update do |page|
      page.replace_html "interpretations-#{factor.id}", :partial => 'admin/interpretation/factor_interpretations',
        :locals => { :factor => factor, :report => report }
      page << "initUJS();"
    end
  end


  def update
    @interpretation = Interpretation.find_by_id(params[:id])
    @interpretation.update_attributes(params[:interpretation])
    flash[:message_saved] = "Изменения сохранены #{Time.now.strftime("%H:%M")}"

    render :update do |page|
      page.replace_html "interpretation-#{@interpretation.id}", :partial => 'editor', :locals => { :interpretation => @interpretation }
      page << "initUJS();"
    end
  end

  def destroy
    interpretation = Interpretation.find_by_id(params[:id])
    factor         = interpretation.factor
    report        = interpretation.report
    interpretation.destroy
    render :update do |page|
      page.replace_html "interpretations-#{factor.id}", :partial => 'admin/interpretation/factor_interpretations',
        :locals => { :factor => factor, :report => report }
      page << "initUJS();"
    end
  end
end
