#coding: utf-8
class Admin::QuestionsController < ApplicationController
	respond_to :html, :js

	before_filter :find_quiz
	before_filter :find_question, only: [:destroy, :buffering, :move, :edit, :update]

	def create
		@type = params[:type]
		@question = "Questions::#{@type.camelize}".constantize.create(quiz: @quiz)
		@question.instruction = "Введите текст вопроса"
		@question.content[:variants] = [] if @type != 'divider' && @type != 'instruction'
		@question.inverse_new = 0 if @type != 'divider' && @type != 'instruction'
		@question.view_type = "2" if @type != 'divider' && @type != 'instruction'
		@question.save
		respond_with(@question)
	end

	def save_weights		
		begin
			weights = params[:weights]
			weights.each do |id, weight|
				question = Questions::Question.unscoped.find(id)
				question.weight = weight
				question.save
			end
		rescue
			render nothing: true, status: 500
		else
			render nothing: true
		end
	end

	def save_buffering
		begin
			buffering = params[:buffering]
			buffering.each do |id, is_buffer|
				question = Questions::Question.unscoped.find(id)
				question.buffered = is_buffer == "true"
				question.save
			end
		rescue
			render nothing: true, status: 500
		else
			render nothing: true
		end
	end

	def remove
		@remove = params[:remove]
		begin			
			@remove.each do |id|
				question = Questions::Question.unscoped.find(id)
				question.destroy
			end
		rescue
			render nothing: true, status: 500
		else
			render :remove
		end
	end

	def buffering
		@question.update_attribute(:buffered, true)
		respond_with(@question)
	end

	def destroy
		@question.destroy
		respond_with(@question)
	end

	def move
		@count = @quiz.questions.count
		respond_with(@count)
	end

	def edit
		respond_with(@question)
	end

	def update
		# binding.pry
		
		file = @question.type.split('::').last.underscore

		@update = params["questions_#{file}"]
	
		@update[:variants] = params[:variant].join("%0\n") if file == "variant"
		@update[:variants] = params[:variant].join("\n") if file == "range"

		@question.update_attributes(@update)
		respond_with(@question)
	end

	private

	def find_quiz
		@quiz = Quiz.find(params[:quiz_id])
	end

	def find_question
		@type = Questions::Question.unscoped.find(params[:id]).type
		@question = "Questions::#{@type.split('::').last.camelize}".constantize.unscoped.find(params[:id])
	end

end
