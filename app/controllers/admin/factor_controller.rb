# -*- encoding : utf-8 -*-
class Admin::FactorController < AdminController

  def new
    quiz   = Quiz.find_by_id(params[:id])
    unless quiz.blank? then
      if params[:parent_id].blank? then
        factor = Factor.create(:quiz_id => quiz.id, :parent_id => 0, :title => "Группа факторов" )
      else
        factor = Factor.find_by_id(params[:parent_id])
        Factor.create(:quiz_id => quiz.id, :parent_id => factor.id,  :title => "Фактор" )
      end
      render :update do |page|
        page << "viewPort.itemsTree.root.findChild('id','factors-#{quiz.id}',true).reload();"
      end
    end
  end

  def edit
    @factor    = Factor.find_by_id(params[:id])
    @quiz      = @factor.quiz
    @questions = @factor.questions
    # binding.pry

    render :update do |page|
      page.replace_html 'editor-tab', @factor.admin_tab_title
      page.replace_html 'settings-area',     :partial => 'editor'
      page.replace_html 'questions-area',    :partial => 'questions'
      page.replace_html 'statistics-area',   :partial => 'statistics'

      page << "initUJS();"
    end
  end


  def reorder
    item       = Factor.find_by_id(params[:node].split('-')[1]) # id of node is "category-#{id}"
    target     = Factor.find_by_id(params[:target].split('-')[1])
    point      = params[:point]

    if(point == 'above' ) then
      rest_items = Factor.find(:all, :conditions => ['weight >= ?', target.weight], :order => 'weight' )
      item.weight = target.weight
    end

    if(point == 'below') then
      rest_items = Factor.find(:all, :conditions => ['weight > ?', target.weight], :order => 'weight' )
      item.weight = target.weight + 1
    end

    item.save
    idx = item.weight + 1
    rest_items.each do |rest|
      if item.id != rest.id then
        rest.weight = idx
        rest.save
        idx += 1
      end
    end

    render :text => ''
  end

  def update
    @factor =  Factor.find_by_id(params[:id])
    @factor.update_attributes(params[:factor])
    flash[:message_saved] = "Изменения сохранены #{Time.now.strftime("%H:%M")}"

    render :update do |page|
      page.replace_html 'settings-area', :partial => 'editor'
      page.replace_html "factor-#{@factor.id}-title", @factor.title
      page << "initUJS();"
    end
  end

  def questions
    @factor =  Factor.find_by_id(params[:id])
    QuestionFactor.find(:all, :conditions => { :factor_id => @factor.id}).each { |qf| qf.destroy }
    params[:questions].each do |q|
      QuestionFactor.create(:factor_id => @factor.id, :question_id => q )
    end
    render :text => ""
  end

  def destroy
    @factor =  Factor.find_by_id(params[:id])
    @factor.destroy
    redirect_to :controller => 'admin/quiz', :action => 'index'
  end

  def correlation
    @factor    = Factor.find_by_id(params[:id])
    @quiz      = @factor.quiz
    # render :update do |page|
    #   page.replace_html 'correlations', :partial => 'correlation'
    #   page << "initUJS();"
    # end
  end
end
