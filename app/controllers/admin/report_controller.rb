# -*- encoding : utf-8 -*-
class Admin::ReportController < AdminController

  def new
    quiz   = Quiz.find_by_id(params[:id])
    unless quiz.blank? then
      if params[:parent_id].blank? then
        factor = Report.create(:quiz_id => quiz.id, :title => "Контекст ответов" )
      end
      render :update do |page|
        page << "viewPort.itemsTree.root.findChild('id','reports-#{quiz.id}',true).reload();"
      end
    end
  end


  def edit
    @report    = Report.find_by_id(params[:id])
    @quiz       = @report.quiz

    render :update do |page|
      page.replace_html 'settings-area',    :partial => 'editor'
      page.replace_html 'editor-tab', 'Интерпретации ответов'
      page.replace_html 'questions-area',   :partial => 'factors_editor'
      page << "initUJS();"
    end
  end

  def update
    @report    = Report.find_by_id(params[:id])
    @report.update_attributes(params[:report])
    flash[:message_saved] = "Изменения сохранены #{Time.now.strftime("%H:%M")}"
    render :update do |page|
      page.replace_html 'settings-area',    :partial => 'editor'
      page << "initUJS();"
    end
  end

  def destroy
    @report    = Report.find_by_id(params[:id])
    @report.destroy
    redirect_to :controller => 'admin/quiz', :action => 'index'
  end
end
