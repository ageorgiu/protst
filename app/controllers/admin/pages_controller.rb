# -*- encoding : utf-8 -*-
#encoding: utf-8
class Admin::PagesController < AdminController
  
  respond_to :html, :js

  before_filter :set_active

  def index
    @pages = Page.all
    @aId = @pages.first.id
  end

  def update
    @page = Page.find(params[:id])
    @page.update_attributes(params[:page])
    respond_with(@page)
  end

private
 
  def set_active
    @active = 6
  end

end
