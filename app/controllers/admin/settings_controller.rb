# -*- encoding : utf-8 -*-
#encoding: utf-8
class Admin::SettingsController < AdminController

  respond_to :html, :js

  before_filter :set_active

  def index
    @settings = Settings.all
  end

  def update
    @set = Settings.find(params[:id])
    @set.update_attributes(params[:settings])
    respond_with(@set)
  end

private

  def set_active
    @active = 5
  end
 
end
