# -*- encoding : utf-8 -*-
#encoding: utf-8
class Admin::QuizzesController < AdminController
  include Admin::QuizHelper

  layout 'admin_bootstrap', except: [:export_answers, :export_report]
  
  respond_to :html, :js, :pdf

  before_filter :get_quizzes
  before_filter :find_user, only: [:reset, :show_answers, :show_statistics, :export_answers, :show_report, :export_report]
  before_filter :find_quiz, only: [:set_period, :show, :edit, :update, :settings, :destroy, :statistics, :reset, :show_answers, 
    :show_statistics, :export_answers, :show_report, :export_report, :excel_export, :excel_reports]

  def index
  end

  def show
  end

  def new
  	@quiz = Quiz.new
  	respond_with(@quiz)
  end

  def create
  	@quiz = Quiz.new(params[:quiz])
    # binding.pry
  	@quiz.save if @quiz.valid?
  	respond_with(@quiz)
  end

  def edit
  end

  def update
    # binding.pry
    @quiz.update_attributes(params[:quiz])
    respond_with(@quiz)
  end

  def destroy
    @quiz.destroy
    flash[:notice] = "Опрос успешно удален!"
    redirect_to admin_quizzes_path
  end

  def settings
  end

  def statistics
    # binding.pry
    @min_date = @quiz.user_quizzes.order('updated_at').first
    @questions = @quiz.questions
  end

  def reset    
    uq = UserQuiz.get(@user, @quiz)
    uq.destroy
    respond_with(@user)
  end

  def show_answers
    @project = Project.find(params[:project_id])
    @user_quiz = UserQuiz.get(@user, @quiz)
    respond_with(@user_quiz)
  end

  def export_answers
    @project = Project.find(params[:project_id])
    @user_quiz = UserQuiz.get(@user, @quiz)
    respond_with(@user_quiz, layout: false)
  end

  def show_statistics
    @project = Project.find(params[:project_id])
    respond_with(@quiz)
  end

  def show_report
    @project = Project.find(params[:project_id])
    @user_quiz = UserQuiz.get(@user, @quiz)
    respond_with(@user_quiz)
  end

  def export_report
    @project = Project.find(params[:project_id])
    @user_quiz = UserQuiz.get(@user, @quiz)
    respond_with(@user_quiz, layout: false)
  end

  def set_period    
    
    if @first_date = params[:param][:start_date] 
      @first_date = params[:param][:start_date].to_date
      deleted_answers = []
      if @quiz.user_quizzes.only_deleted.where("created_at > ?", @first_date).count > 0 
      
        @quiz.user_quizzes.only_deleted.where("created_at > ?", @first_date).includes(:user).each do |user_quiz|
          user_quiz.user.answers.only_deleted.each do |answer|
            if answer.updated_at > @first_date 
              deleted_answers << answer.id
            end
          end 
          Answer.only_deleted.where('id in (?)', deleted_answers).each {|ans| ans.add_hit(nil)}
          user_quiz.add_hit(nil)      
        end
        
      end
      answers = []
      # binding.pry
      @quiz.user_quizzes.completed.where("created_at < ?", @first_date).includes(:user).each do |user_quiz|
        unless user_quiz.user.nil?
          user_quiz.user.answers.each do |answer|
            if answer.updated_at < @first_date 
              answers << answer.id
            end
          end
        end
        Answer.where('id in (?)', answers).each {|ans| ans.add_hit(Time.now)}
        user_quiz.add_hit(Time.now)
      end 
      @quiz.elaborate_statistics
      redirect_to :back 
    end
  end

  def excel_export
    statistic = Statistic.create content_id: @quiz.id
    Delayed::Job.enqueue(ExportQuizzesJob.new @quiz.id, statistic.id)
    redirect_to excel_reports_admin_quiz_path(@quiz), notice: I18n.t('reports.success')
    # render layout: false
  end

  def excel_reports
    @xlss = Statistic.where(content_id: @quiz.id)
    respond_with(@xlss)
  end

  private

  def get_quizzes
  	@quizzes = Quiz.all
    @active = 4
  end

  def find_quiz
  	@quiz = Quiz.find(params[:id])
  end

  def find_user
    @user = User.find(params[:user_id])
  end
end