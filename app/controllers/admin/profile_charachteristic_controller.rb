class Admin::ProfileCharachteristicController < AdminController
	respond_to :html, :js

	before_filter :find_quiz

	def index
	end

	private
	def find_quiz
		@quiz = Quiz.find(params[:quiz_id])
	end

end
