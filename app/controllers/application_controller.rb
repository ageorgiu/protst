# -*- encoding : utf-8 -*-
#encoding: utf-8
require 'statsample'

class ApplicationController < ActionController::Base
  # current user left for real auth system
  expose(:current_person)  { session[:user_id] ? User.find(session[:user_id]) : User.new(params[:user]) }
  expose(:person_logged?)  { session[:user_id].present? }
  expose(:remote_session?) { params[:remote] }

  before_filter :store_params
  
  helper :all # include all helpers, all the time
  #protect_from_forgery # See ActionController::RequestForgeryProtection for details

  def after_sign_in_path_for(resource_or_scope)
    admin_path
  end

  def after_sign_out_path_for(resource_or_scope)
    resource_or_scope == :admin_user ? admin_login_path : root_path
  end

  private

    def store_person(user)
      session[:user_id] = user.id
    end

    def clear_person
      session[:user_id]=nil
    end

    def person_not_signed
      redirect_to(root_path) if person_logged?
    end

    def person_signed
      unless person_logged?
        unless session[:remote]
          redirect_to auth_register_path, :alert=>'Вы должны зарегистрироваться или войти под своим логином'
        else
          redirect_to remote_auth_register_path()
        end
      end
    end

    def store_params
      session[:project_id] = params[:project_id] if params[:project_id]
      session[:remote] = params[:remote] if params[:remote]
      
      @remote_params = Hash.new
      @remote_params[:type] = params[:type] if params[:type].present?
      @remote_params[:remote] = true if params[:remote].present?
      @remote_params[:show_quiz] = true if params[:show_quiz].present?
    end

end
