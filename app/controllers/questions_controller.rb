# -*- encoding : utf-8 -*-
class QuestionsController < ApplicationController
  QUESTIONS_AREA_ID = 'questions-area'

  helper_method :quiz

  def new
    @questions  = quiz.questions.editable
    params[:question_type].constantize.create(:quiz => quiz)
    render :update do |page|
      page.replace_html QUESTIONS_AREA_ID, :partial => 'questions_editor', :locals => { :editable => true }
    end
  end


  def edit
    @question = Questions::Question.find_by_id(params[:id])
    render :update do |page|
      page.replace_html "question-view-#{@question.id}", :partial => "#{@question.path}/edit"
      page << "initUJS();"
    end
  end

  def view
    @question = Questions::Question.find_by_id(params[:id])
    @quiz = @question.quiz
    render :update do |page|
      page.replace_html "question-view-#{@question.id}", :partial => "#{@question.path}/view", :locals => { :question => @question }
    end
  end


  def update
    @question = Questions::Question.find_by_id(params[:id])
    @question.update_attributes(params[:question])
    @quiz = @question.quiz
    render :update do |page|
      page.replace_html "question-view-#{@question.id}", :partial => "#{@question.path}/view", :locals => { :question => @question }
    end
  end

  def destroy
    question = Questions::Question.find_by_id(params[:id])
    @quiz       = question.quiz
    @questions  = @quiz.questions.editable
    question.destroy
    render :update do |page|
      page.replace_html QUESTIONS_AREA_ID, :partial => 'questions_editor', :locals => { :editable => true }
    end
  end


  def questions_sort
    questions = Questions::Question.find_ordered_array(params['questions-list'])
    questions.each_with_index do |q,idx|
      Questions::Question.update_all({:weight => idx}, {:id=>q.id})
    end
    render :update do |page|
      questions.each_with_index do |q,idx|
        page.replace_html "question-#{q.id}-index", "#{quiz.position(q)}" if q.has_answer?
      end
    end
  end

  private

    def quiz
      @quiz ||= Quiz.find(params[:id])
    end
end
