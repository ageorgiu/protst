# -*- encoding : utf-8 -*-
class AdminController < ActionController::Base
  layout 'admin_bootstrap'

  respond_to :html, :js

  # left for statistic report purpose
  #maybe better move statistic from admin namespace
  # before_filter :admin_auth, :except=>:report

  before_filter :is_admin_login, :except => :login

  # include ActionView::Helpers::PrototypeHelper  
  include ActionView::Helpers::JavaScriptHelper
  include ActionView::Helpers::TagHelper

  def login
    if admin_user_signed_in?
      redirect_to admin_path
    else
      render :layout => false
    end
  end

  def reset_password    
  end

  def reset_password_save
    admin_user = params[:admin_user]
    @may_reset = true
    unless current_admin_user.valid_password?(admin_user[:old_password])
      @may_reset = false
      @reason = :old_invalid
    end
    unless admin_user[:new_password] == admin_user[:new_password_confirmation]
      @may_reset = false
      @reason = :new_incorrect
    end
    if admin_user[:new_password].blank?
      @may_reset = false
      @reason = :new_blank
    end
    if @may_reset
      current_admin_user.update_attributes(password: admin_user[:new_password], password_confirmation: admin_user[:new_password_confirmation])
    end
    respond_with(@may_reset)
  end

  def index
    @projects = Project.order("created_at desc").limit 10
    @passes = UserQuiz.completed.order("created_at desc").limit 10
  end

  private

    def is_admin_login
      unless admin_user_signed_in?
        redirect_to admin_login_path
      end
    end

    def admin_auth
      if AppConfig.setting[:need_auth]
        authenticate_or_request_with_http_basic do |login, password|
          (login == AppConfig.setting[:admin_login]) and (password == AppConfig.setting[:admin_password])
        end
      else
        true
      end  
    end

end
