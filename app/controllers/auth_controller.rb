# -*- encoding : utf-8 -*-
#encoding: utf-8
class AuthController < ApplicationController
  expose(:project){session[:project_id].present? ? Project.find(session[:project_id]) : Project.default}

  before_filter :person_not_signed, :only=>[:register, :process_registration, 
    :process_registration, :process_remote_registration]

  before_filter :person_signed, :only=>[:profile, :update_profile, :logout]

  def login
    user = User.find_by_login_and_password(params['login'],params['password'])
    if user
      store_person(user)
      redirect_to quiz_root_path if user.restricted?
    else
      flash[:alert] = 'Неправильный логин или пароль'
    end
    redirect_to :back
  end

  def register;end

  def process_registration
    if project.users << current_person
      session[:user_id] = current_person.id
      redirect_to quiz_root_path, notice: 'Поздравляем с регистрацией на сайте!!!'
    else
      render action: 'register'
    end
  end

  def profile; end

  def update_profile
    if current_person.update_attributes(params[:user])
      flash[:notice] = "Изменения сохранены"
      if current_person.restricted?
        redirect_to quiz_root_path
      else
        redirect_to auth_profile_path
      end
    else
      render  :action => 'profile'
    end
  end

  def logout
    clear_person
    redirect_to root_path
  end

end
