# -*- encoding : utf-8 -*-
#encoding: utf-8
class QuizController < ApplicationController
  layout :set_layout

  before_filter :person_signed, :only => [ :show, :instruction, :run, :answer, :complete, :result ]
  before_filter :check_quiz_complete, :only=>[:instruction, :run, :answer, :complete]
  after_filter :clean_user_data, :only=>:result

  expose(:quiz) {Quiz.find(params[:id])}

  expose(:project){ current_person.project if current_person && !remote_session? }
  expose(:project){session[:project_id].present? && remote_session? ? Project.find(session[:project_id]) : Project.default}
  expose(:project_quizzes){project.project_quizzes.includes(:quiz) if project}
  expose(:project_quiz){quiz.project_quizzes.find_by_project_id(project)}

  expose(:current_page){ params[:page] ? params[:page].to_i : 0}

  expose(:user_quiz){ UserQuiz.get(current_person, quiz) }

  expose(:report) {Report.find(params[:report_id])}

  def index
    # binding.pry
  end

  def show
    redirect_to :action => :instruction, :id => quiz.id
  end

  def instruction
    # binding.pry
    redirect_to quiz_run_path(params[:id], @remote_params) if project_quiz.escape_instruction?
  end

  def download_report
    @quiz           = Quiz.find_by_id(params[:quiz_id])
    @user           = User.find_by_id(params[:user_id])
    send_data(generate_pdf(params), :filename => "#{@user.title.parameterize}-#{@quiz.title.parameterize}-#{DateTime.now.strftime('%Y %m %d %H:%M:%S:%L').parameterize}.pdf", :type => "application/pdf")
  end

  def view_report
    @quiz           = Quiz.find_by_id(params[:quiz_id])
    @user           = User.find_by_id(params[:user_id])
  end
  
  def run
    UserQuiz.run(current_person, quiz)
    @questions = quiz.page_questions(current_page)
  end

  def answer
    question = Questions::Question.find(params[:question_id])
    question.generate_answer(current_person, params[:value])
    render :text=>''
  end

  def complete
    # binding.pry 
    if (@questions = user_quiz.complete!).blank? && !params[:remote].present? || params[:remote] == false
      redirect_to quiz_result_path(quiz, @remote_params)
    elsif (@questions = user_quiz.complete!).blank? && params[:remote] = true
      redirect_to quiz_root_path(@remote_params)
    else
      flash[:alert] = "Вы не  ответили на следующие вопросы:"
      render :run
    end
  end

  def result
    @quiz = Quiz.find(params[:id]) if params.has_key?(:id)
    @user = User.find(session[:user_id])
  end

  def clear_result
    UserMail.send_clear_notification_for(current_person, quiz).deliver
    flash[:notice] = 'Ваш запрос был отправлен администратору'
    redirect_to :action=>:index, params: @remote_params
  end

  def test_mode
    @project = Project.find_by_title('Demo')
  end

private

  def generate_pdf(params)
    @quiz           = Quiz.find_by_id(params[:quiz_id])
    @user           = User.find_by_id(params[:user_id])    
    Prawn::Document.new do |pdf|
      pdf.font_families.update(
        "cyrillic" => {:normal => "#{Rails.root}/app/assets/fonts/font.ttf",
                      :bold   => "#{Rails.root}/app/assets/fonts/font-bold.ttf"}
      )
      pdf.font "cyrillic"
      pdf.text "#{@user.title}", :size=>20
      pdf.move_down 15
      pdf.text "#{@quiz.title}", :size=>20
      pdf.move_down 80
      width = 35
      height = 20
      left_margin = 200
      stens = []
      # факторы и нумерация
      pdf.draw_text 'Фактор', :at=>[0, pdf.y]
      1.upto 10 do |j|
        rx1 = j*width+left_margin-(((j%10)==0)? 10 : 5)
        pdf.draw_text j, :at=>[rx1, pdf.y]
      end
      pdf.move_down 20
      #grid & stens
      @quiz.factor_groups.each do |group|
        group.children.each do |factor|
          sten = factor.sten(@user).to_i
          stens << sten
          x1 = 5.5*width+left_margin
          x2 = sten*width+left_margin
          pdf.stroke_color = "0"*6
          pdf.line_width = 0.3
          1.upto 9 do |j|
            rx1 = j*width+left_margin
            ry1 = pdf.y + height/2
            pdf.fill_color = 'd'*6
            pdf.fill_rectangle [rx1, ry1], width, height
            pdf.fill_color '0'*6
            pdf.stroke_rectangle [rx1, ry1], width, height
          end
          pdf.stroke_color = "3"*6
          pdf.line_width = height*0.6
          pdf.stroke_line [x1,pdf.y,x2,pdf.y]
          pdf.draw_text factor.title, :at=>[0, pdf.y]
          pdf.draw_text sten, :at=>[left_margin, pdf.y]
          [4,5.5,7].each do |line|
            x1 = left_margin + line*width
            y1 = pdf.y + height/2
            y2 = pdf.y - height/2
            if line == 5.5
              pdf.line_width 3
              pdf.stroke_color "0"*6
            else
              pdf.line_width 1
              pdf.stroke_color 'cc0000'
            end
            pdf.stroke_line [x1,y1,x1,y2]
          end
          pdf.move_down height
        end
      end

      if ch = @quiz.get_profile_charachteristic(stens)
        pdf.start_new_page if pdf.y < 80
        pdf.text 'Характеристика вашего профиля', :size=>16, :style=>:bold
        pdf.move_down 5
        pdf.text ch, :size=>10, :indent_paragraphs => 20
        pdf.move_down 15
      end

      @quiz.factor_groups.each do |group|
        group.children.each do |factor|
          i = report.find_interpretation(factor.id, factor.sten(@user).to_i)
          if i.present?
            pdf.start_new_page if pdf.y < 80
            pdf.text factor.title, :size=>16, :style=>:bold
            pdf.move_down 5
            pdf.text i.content, :size=>10, :indent_paragraphs => 20
            pdf.move_down 15
          end
        end
      end
    end.render 
  end


  def set_layout
    remote_session? ? 'remote' : 'application'
  end

  def clean_user_data
    if session[:remote_user]
      session[:remote_user] = nil
      session[:user_id] = nil
    end
  end

  def check_quiz_complete
    if user_quiz
      if user_quiz.complete?
        redirect_to quiz_root_path, :alert=>"Вы уже заполнили этот опрос"
      elsif !project.quizzes.values_of(:id).include?(user_quiz.quiz_id)
        redirect_to quiz_root_path, :alert=>"Вы не подписаны на этот опрос"
      end
    end
  end
end

