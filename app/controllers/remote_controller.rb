# -*- encoding : utf-8 -*-
class RemoteController < ApplicationController

  layout 'remote'
  before_filter :set_remote
  
  def index
    # binding.pry
    # if (@type == )
    # redirect_to quiz_root_path(show_quiz: true, type: :frame)
    # binding.pry

    if @remote_params[:type].present? && @remote_params[:type] == "register"
      if person_logged?
        redirect_to quiz_root_path(@remote_params)
      else
        redirect_to remote_auth_register_path(@remote_params)
      end
    else
      redirect_to quiz_root_path(@remote_params)
    end

  end

  private
    def set_remote
      session[:remote]=true
      @remote_params[:remote] = true
      @remote_params[:show_quiz] = true
      @remote_params[:type] = params[:type] if params[:type].present?
    end
end
