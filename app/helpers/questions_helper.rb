# -*- encoding : utf-8 -*-
#encoding: utf-8
module QuestionsHelper
  def show_numeration(quiz, question)
  	# binding.pry
    end_point = quiz.position(question).blank? ? "" : "."
    "<span id=\"question-#{question.id}-index\" class=\"question-index\"> #{quiz.position(question)}</span>#{end_point}".html_safe
  end
end
