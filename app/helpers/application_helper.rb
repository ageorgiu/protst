# -*- encoding : utf-8 -*-
# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  def extjs_button(text, options)
    case options[:method]
    when :submit then
      content_tag(:table, :class => "x-btn x-btn-noicon", :cellspacing => 0) do
        content_tag(:tbody, :class => "x-btn-small x-btn-icon-small-left") do
        [
          content_tag(:tr) do
          [
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-tl"),
            content_tag(:td, "", :class => "x-btn-tc"),
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-tr"),
          ].join(" ").html_safe
          end,
          content_tag(:tr) do
          [
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-ml"),
            content_tag(:td, :class => "x-btn-mc") do
              content_tag(:em, :unselctable => "on") do
                content_tag(:button, "&nbsp;#{text}&nbsp;".html_safe, :class=> "x-btn-text", :id=>options[:submit_id], :type=> "submit",  :value => text)
              end
            end,
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-mr")
          ].join(" ").html_safe
          end,
          content_tag(:tr) do
          [
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-bl"),
            content_tag(:td, "", :class => "x-btn-bc"),
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-br"),
          ].join(" ").html_safe
          end
        ].join(" ").html_safe
        end
      end
    when :remote
      content_tag(:table, :class => "x-btn x-btn-noicon", :cellspacing => 0 ) do
        content_tag(:tbody, :class => "x-btn-small x-btn-icon-small-left") do
        [
          content_tag(:tr) do
          [
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-tl"),
            content_tag(:td, "", :class => "x-btn-tc"),
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-tr"),
          ].join(" ").html_safe
          end,
          content_tag(:tr) do
          [
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-ml"),
            content_tag(:td, :class => "x-btn-mc") do
              content_tag(:em, :unselctable => "on") do
                content_tag(:button, text, :class=> "x-btn-text", :onClick=>"callToRemote('#{options[:url]}')")
              end
            end,
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-mr")
          ].join(" ").html_safe
          end,
          content_tag(:tr) do
          [
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-bl"),
            content_tag(:td, "", :class => "x-btn-bc"),
            content_tag(:td, "<i>&nbsp;</i>".html_safe, :class => "x-btn-br"),
          ].join(" ").html_safe
          end
        ].join(" ").html_safe
        end
      end
    end
  end

  def extjs_form_item(text, &block)
    content_tag(:div, :class => "x-form-item") do
      [
        content_tag(:label, text.html_safe, :class => "x-form-item-label"),
        content_tag(:div, :class => "x-form-element" ) do
          block.call.html_safe
        end
      ].join('').html_safe
    end
  end

  # def link_to_add_fields(name, f, association, params={})
  #   new_object = f.object.class.reflect_on_association(association).klass.new
  #   fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
  #     render(association.to_s.singularize + "_fields", {:f => builder}.merge(params))
  #   end
  #   link_to_function(name, ("add_fields(this, '#{association}', '#{escape_javascript(fields)}')"), :id=>"#{association}_generator")
  # end

  def required_answer_for(question,project_quiz)
    content_tag(:sup, '*', :class=>'marked') if question.required.present? == project_quiz.mark_required.present?
  end

  def header_tab(title, path)
    tab_options = (path==request.path) ? {:class=>'current_page_item'} : {}
    content_tag :li, tab_options do
      link_to title, path
    end
  end

  def error_message_on(object, attribute)
    if object.errors[attribute].present?
      content_tag :label, object.errors[attribute].join('. '), :class=>'error-field'
    end
  end

  def has_any_section_page?(array)
    pages = [:educationlevel_page, :educationtype_page, :jobarea_page, :jobexpirience_page, :jobposition_page, :city_page]
    array.each_key do |key|
      return true if pages.include? key.to_sym
    end
    false
  end

end
