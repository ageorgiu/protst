# -*- encoding : utf-8 -*-
#encoding: utf-8
module Admin::UserquizHelper
  def user_quizzes_status(user)
    states = {}
    Quiz.all.each do |quiz|

      # uq     = UserQuiz.get(user, quiz)
      # state  = uq.blank? ? "" : uq.state
      
      # case state
      # when 'complete' then 
      #   states["quiz_#{quiz.id}"] = link_to( "Завершен #{uq.updated_at.to_date.strftime("%d.%m.%y")}", { :controller => 'statistics', :action => "user_page", :user_id => user.id, :quiz_id => quiz.id }, :remote => true )
      # when 'processing' then states["quiz_#{quiz.id}"] = 'В процессе'
      # else   states["quiz_#{quiz.id}"] = 'Назначен'

      user_quiz = UserQuiz.get(user, quiz)
      if user_quiz
        states["quiz_#{quiz.id}"] = if user_quiz.complete?
          %Q{
            <a href='#{admin_statistics_user_path(:user_id => user.id, :quiz_id => quiz.id)}' data-remote="true">
              Завершен #{user_quiz.updated_at.to_date.strftime("%d.%m.%y")}
            </a>
          }
        else
          'В процессе'
        end
      else
        states["quiz_#{quiz.id}"] = (user.project_quizzes.include?(quiz)) ? "Назначен" : ""
      end
    end
    return states
  end

  def user_quizzes_columns
    Quiz.values_of(:title, :id).map{|arr| {:header => arr.first, :name=>"quiz_#{arr.last}"}}
  end

  def json_dynamic_grid_collection(collection, fields, &block)
    result = ExtJS.json_grid_collection(collection) { |i| block.call(i) }
    result[:metaData ] = {
        :totalProperty  => 'total',
        :root           => 'items',
        :id             => 'id',
        :fields         => fields.map { |f| { :name => f[:name], :type => "string" }  }
    }
    result[:columns]    = fields.map { |f| { :header => f[:header], :dataName => f[:name], :sortable => true} }

    result
  end
end
