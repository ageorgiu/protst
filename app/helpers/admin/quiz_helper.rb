# -*- encoding : utf-8 -*-
#encoding: utf-8
module Admin::QuizHelper
  def quiz_properties(quiz_id)
    factors = {
      :text => 'Факторы', :id => "factors-#{quiz_id}",
      :meta => { :type => 'factors_list', :quiz_id => quiz_id,
        :add       => true,
        :add_label => 'Добавить группу факторов',
        :add_url   => url_for(:controller => 'admin/factor', :action => 'new', :id => quiz_id  )
      }
    }
    reports = {
      :text => 'Типы отчетов', :id => "reports-#{quiz_id}",
      :meta => { :type => 'reports_list', :quiz_id => quiz_id,
        :add => true,
        :add_label => 'Добавить тип',
        :add_url   => url_for(:controller => 'admin/report', :action => 'new', :id => quiz_id )
      }
    }
    return [factors, reports]
  end

  def factor_node(f)
    {
      :text => ExtJS.caption(f,'title'), :id => "factor-#{f.id}",  :expanded => true,
      :meta => {
        :quiz_id   => f.quiz.id,
        :add       => true,
        :add_label => 'Добавить фактор',
        :add_url   => url_for(:controller => 'admin/factor', :action => 'new', :id => f.quiz.id, :parent_id => f.id  )
      }
    }
  end

  def report_node(c)
    {
      :text => ExtJS.caption(c,'title'), :id => "report-#{c.id}",  :leaf => true,
      :meta => {
        :quiz_id   => c.quiz.id,
        :add       => false
      }
    }
  end

  def quiz_questions(quiz)
    quiz.allowed_questions-Questions::Question.partional_names.map(&:constantize)
  end
end
