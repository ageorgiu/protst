# -*- encoding : utf-8 -*-
module Admin::StatisticsHelper
  
  def bar_params(sten)
    return { :left => '50',           :width => (sten-5.5)*9  } if sten > 5.5
    return { :left => sten*9, :width => 50-sten*9  }
  end
  
  def stan_row(sten)
    classes  = []
    if sten > 5.5
      filled      = sten*2-11
      classes += [ "empty" ] * 9
      classes += ["filled"] * filled
      classes += ["empty"] * (9 -filled)
      classes[9] += " mark-left"
      classes[8] += " mark-right"
    else
      empty   =  (sten-1) * 2
      classes += [ "empty"]   * empty
      classes += [ "filled" ] * (9-empty)
      classes += ["empty"] * 9
      classes[8] += " mark-right"
    end
    classes[5]  += " orange-right"
    classes[6]  += " orange-left"
    
    classes[11] += " orange-right"
    classes[12] += " orange-left"
    
    return classes
  end
end
