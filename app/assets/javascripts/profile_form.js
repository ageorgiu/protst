$(function(){
  $('#city_title').autocomplete({
    serviceUrl:$('#city_title').data("url"),
    minChars:2,
    onSelect: function(value, data){
      $('#user_city_id').val(data);
    }
  });
  $("input.date_input").mask("99.99.9999",{placeholder:" "});
})