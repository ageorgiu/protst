// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require jquery.noty
//= require ajax_nested_form
//= require ckeditor/init
//= require dataTables/jquery.dataTables
//= require dataTables/jquery.dataTables.bootstrap
//= require jquery-ui-1.8.22.custom.min
//= require service
//= require bootstrap-datepicker


jQuery(function($) {
  document.addEventListener("page:change", function() {
	  console.log('change fired!');
	  $("a[data-remote='true'], a[href='#'], a[data-toggle]").attr("data-no-turbolink","true");
	  $('.dropdown-toggle').dropdown();
	  $(".tabbable").tab();
	});

  $('a.toggles').click(function() {
      $('a.toggles i').toggleClass('icon-chevron-left icon-chevron-right');

      $('.left-pane').animate({
          width: 'toggle'
      }, 0);
      $('.center-pane').toggleClass('full-width');
      return false;
  });

});