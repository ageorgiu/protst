window.Service = {}

class @Service 
	
	@refreshBootstrapRadio: ->
		$('div.btn-group[data-toggle-name=*]').each ->
			group = $(@)
			form = group.parents('form').eq(0)
			name = group.attr('data-toggle-name')
			hidden = $(group).next()
			$('button', group).each ->
				button = $(@)
				button.live 'click', ->
					hidden.val($(this).val())
				if button.val() is hidden.val()
					button.addClass('active')
		return

	@refreshLinkBasic: (link, url) ->
		data = $("input:checked[name='choosed[]']").serialize()
		if data.length is 0
			$(link).attr('href', "#{url}")
		else
			$(link).attr('href', "#{url}?#{data}")
		return

	@refreshMoveLink: (clean_url) ->
		link = $(".move-user-group")
		Service.refreshLinkBasic(link, clean_url)
		return

	@refreshSendLink: (clean_url) ->
		link = $(".send-to-choosed")
		Service.refreshLinkBasic(link, clean_url)
		return

	@refreshExportLink: (clean_url) ->
		link = $(".export-choosen")
		Service.refreshLinkBasic(link, clean_url)
		return

	@removeSequence: (link) ->
		well = $(link).parents(".well")
		well.remove()
		return false

	@addOption: (selector, key, value) ->
		$(selector).append($("<option></option>").attr("value", key).text(value))
		return

	#####################
	# USER METHODS
	#####################

	@getUserRowIndex: (id) ->		
		tr = $("tr[user-data='#{id}']").first()
		Service.userTableApi().fnGetPosition(tr.get(0))

	@addUser: (content, id) ->
		tr = $(content)		
		api = $("#client-list").data("api")		
		index = api.fnAddData Service.getDataArray(tr)
		tr = api.fnGetNodes(index)
		$(tr).attr("user-data", id)
		return

	@updateUser: (new_content, id) ->
		tr = $(new_content)
		ind = Service.getUserRowIndex(id)
		Service.userTableApi().fnUpdate Service.getDataArray(tr), ind
		return

	@deleteUser: (id) ->		
		Service.userTableApi().fnDeleteRow(Service.getUserRowIndex(id))
		return

	#####################
	# NOTICE METHODS
	#####################

	@getNoticeRowIndex: (id) ->		
		tr = $("tr[notice-data='#{id}']").first()
		Service.noticeTableApi().fnGetPosition(tr.get(0))

	@addNotice: (content, id) ->
		tr = $(content)				
		index = Service.noticeTableApi().fnAddData Service.getDataArray(tr)
		tr = Service.noticeTableApi().fnGetNodes(index)
		$(tr).attr("notice-data", id)
		return

	@updateNotice: (new_content, id) ->
		tr = $(new_content)
		ind = Service.getNoticeRowIndex(id)
		Service.noticeTableApi().fnUpdate Service.getDataArray(tr), ind
		
		return

	@deleteNotice: (id) ->		
		Service.noticeTableApi().fnDeleteRow(Service.getNoticeRowIndex(id))
		return

	#####################
	# GENERAL METHODS
	#####################

	@showNoty: (message, type = "success", timeout = 1500) ->
		noty
			layout: 'topRight'
			text: message
			type: type
			closeWith: ['hover', 'click']
			timeout: timeout

	@userTableApi: ->
		$("#client-list").data("api")

	@noticeTableApi: ->
		$("#notice-list").data("api")

	@getDataArray: (tr) ->
		data = []
		count = tr.find("td").length
		for i in [0..count-1]
			data.push tr.find("td").get(i).innerHTML
		data
