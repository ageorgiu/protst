$j(function() {
  var instruction_original, instruction_editable;
  
  function init_dialog() {
   	$j("#dialog:ui-dialog").dialog("destroy");
		
		$j("#belbin-item-edit-dialog").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				"Сохранить": function() {
				    instruction_original.val(instruction_editable.val());
						$j(this).dialog("close");
					},
			
				"Отмена": function() {
					$j(this).dialog("close");
				}
			}
		}); 
  }
	$j(".belbin_edit_image").live('click', function(event) {
	  instruction_original = $j($j(this).next());
	  instruction_editable = $j("#belbin-item-instruction");
	  instruction_editable.val(instruction_original.val());
    init_dialog();
    $j("#belbin-item-edit-dialog").dialog("open");
		return event.preventDefault();
	});

});