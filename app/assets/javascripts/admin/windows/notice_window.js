Admin.NoticeWindow = Ext.extend(Ext.Window, {
  border: false,

  initComponent:function() {
    Ext.apply(this, {
      layout:'fit',
      width:960,
      height: 450,
      closeAction: 'hide',
      title : 'Отправка уведомлений',
      top: 200
    });

    Admin.NoticeWindow.superclass.initComponent.apply(this, arguments);

  },

  onRender:function() {
    Admin.NoticeWindow.superclass.onRender.apply(this, arguments);
  }
  
});

Admin.NoticeWindow.clearAll = function() {
  $j(".visible-item .checkbox").attr('checked', false);
}

Admin.NoticeWindow.selectAll = function() {
  $j(".visible-item .checkbox").attr('checked', true);
}

Admin.NoticeWindow.selectIncomplete = function() {
  $j(".visible-item .checkbox.incomplete").attr('checked', true);
}
