window.QuestionEditor = {}


# Class that represents JS functionality for quiz question editor
#
# @author Sergey sergio Gernyak <sergeg1990@gmail.com>
# @copyright 2012 AlterEGO
# @version 1.0
#

class @QuestionEditor
	sourceId: ""
	bufferId: ""
	savePositionsUrl: ""
	saveBufferingUrl: ""
	removeGroupUrl: ""

	# Construct a new editor
	#
	# @param source [String] the source list identifier
	# @param buffer [String] the buffer list identifier
	#
	constructor: (source, buffer) ->
		@sourceId = source
		@bufferId = buffer
		@initLists()
		return

	# Initialized sortable lists
	#
	initLists: ->
		$(@bufferId).sortable
			receive: (e, ui) =>
				@afterReorderCallback(false)
			out: =>
				@afterReorderCallback(false)
			update: =>
				@afterReorderCallback(false)
			over: ->
				$("li.empty").hide()

		$(@sourceId).sortable
			update: =>
				@afterReorderCallback()

		$("#{@bufferId}, #{@sourceId}").sortable(
			connectWith: ".connectedSortable"
			placeholder: "draggable-highlight"
			cursor: "move"
			cancel: ".empty"
			start: (e, info) ->
				info.item.siblings(".selected").not(".empty").appendTo(info.item)
				return
			stop: (e, info) ->
				info.item.after(info.item.find("li").removeClass('selected'))
				return
		).disableSelection()

		#$("#{@bufferId} li, #{@sourceId} li").live 'click', ->
		#	$(@).toggleClass('selected')
		#	$(@).find(".check-quiz-item").toggleClass('active')
		#	return

		$(".check-quiz-item").live 'click', (ev) ->
			ev.stopPropagation()
			$(@).toggleClass('active')
			$(@).parents("li").toggleClass('selected')
			return false

		return


	# Called after order is changed or buffering/unbuffering questions
	#
	#	@param save [Boolean] Save or not positions and buffering changes on server
	#
	afterReorderCallback: (save = true) ->
		@setEmptyBuffer()
		if save
			@savePositions()
			@saveBuffering()
		return

	# Add new question to source list
	#
	# @param source [String] the raw html of adding question
	#
	addNewQuestion: (source) ->
		fLi = $(@sourceId).find("li:in-viewport")		
		if fLi.length
			el = $(fLi).first()
			$(el).before(source)
			el = $(el).prev()
		else
			$(@sourceId).append(source)
			el = $(@sourceId).find("li").first()
		pos = $(el).position()
		$(window).scrollTop(pos.top - 100) if ($(window).scrollTop() > 117)
		@preselectNew(el)
		@savePositions()
		return el

	# Preselect added question
	#
	#	@param el [LINode] the reference to li node
	#
	preselectNew: (el) ->
		$(el).find(".question-item").addClass('new')
		return el

	# Remove question from list
	#
	# @param id [Integer] Id of destroyed question
	#
	removeQuestion: (id) ->
		$(@sourceId).find("li[question-data='#{id}']").first().remove()		
		return

	# Move to buffer selected questions
	#
	bufferingSelected: ->
		if $("li.selected", @sourceId).length
			$(@sourceId).find("li.selected").each (index, el) =>
				$(el).removeClass('selected')
				$(el).prependTo(@bufferId)
				return
			@afterReorderCallback()
		else
			Service.showNoty('Нет выделенных вопросов!', 'warning')		
		return

	# Move question to buffer
	#
	# @param id [Integer] Id of moved question
	#
	bufferQuestion: (id) ->
		el = $(@sourceId).find("li[question-data='#{id}']").first()
		$(el).prependTo(@bufferId)		
		@setEmptyBuffer()
		return

	# "Change buffer view dependent from it's content"
	#	
	setEmptyBuffer: ->
		if $(@bufferId).find("li").not("li.empty").length == 0
			$(@bufferId).addClass("empty")
			$("li.empty").show()
		else
			$(@bufferId).removeClass("empty")
			$("li.empty").hide()
		return

	# Mark all source questions as unselected
	#
	sourceUnselectAll: ->
		$(@sourceId).find("li.selected").removeClass("selected").find('.check-quiz-item').removeClass('active')		
		return

	# Mark all buffered questions as selected
	#
	bufferSelectAll: ->
		$(@bufferId).find("li:not(.empty)").addClass('selected')
		return

	# Mark all buffered questions as unselected
	#
	bufferUnselectAll: ->
		$(@bufferId).find("li").removeClass('selected')
		return

	# Sending ajax request
	#
	# @param url [String] A string containing the URL to which the request is sent
	# @param data [Array] Data to be sent to the server
	# @param method [String] The type of request to make
	# @param showMessage [Boolean] if true the success message will be shown
	# @param successMsg [String] The success message
	# @param errorMsg [String] The error message
	#
	sendAjax: (url, data, method = "POST", showMessage = true, successMsg = "", errorMsg = "", successCallback = null) ->
		$.ajax
			url: url
			type: method
			data: data
			success: =>
				Service.showNoty(successMsg) if showMessage				
				return
			error: =>
				Service.showNoty(errorMsg, 'error')
				return
		return

	# "Saving question's positions"
	#
	# @param showMessage [Boolean] if true the success message will be shown
	#
	savePositions: (showMessage = true) ->
		startWeight = 0
		ul = $(@sourceId)
		res = $("li", ul).map( ->
			id = $(this).attr("question-data");			
			return "weights[" + id + "]=" + startWeight++
		).get().join("&")
		console.log(res)
		@sendAjax(@savePositionsUrl, res, "POST", showMessage, 'Позиции успешно сохранены!', 'Ошибка при сохранении позиций!')
		@refreshQuestionNumbers()
		return

	# "Saving question's buffering"
	#
	# @param showMessage [Boolean] if true the success message will be shown
	#
	saveBuffering: (showMessage = true) ->		
		ul = $(@sourceId)
		res = $("li", ul).map( ->
			id = $(this).attr("question-data");			
			return "buffering[" + id + "]=false";
		).get().join("&")
		ul = $(@bufferId)
		res2 = $("li:not(.empty)", ul).map( ->
			id = $(this).attr("question-data");			
			return "buffering[" + id + "]=true";
		).get().join("&")
		data = "#{res}&#{res2}"
		console.log(data)
		@sendAjax(@saveBufferingUrl, data, "POST", showMessage, 'Буферизация вопросов успешно сохранены!', 'Ошибка при буферизации!')
		return

	# Remove selected questions
	#
	# @param showMessage [Boolean] if true the success message will be shown
	#
	removeQuestionGroup: (showMessage = true) ->
		ul = $(@sourceId)
		if $("li.selected", ul).length
			res = $("li.selected", ul).map( ->
				id = $(this).attr("question-data");			
				return "remove[]=#{id}";
			).get().join("&")
			@sendAjax(@removeGroupUrl, res, "POST", showMessage, 'Вопросы успешно удалены!', 'Ошибка при удалении!')
		else
			Service.showNoty('Нет выделенных вопросов!', 'warning')
		return

	# Move question to specify position
	#
	# @param id [Integer] Id of moved question
	# @param newPosition [Integer] New position index
	#
	moveQuestion: (id, newPosition) -> 
		moved = $(@sourceId).find("li[question-data='#{id}']")
		sel = "#{@sourceId} li:nth-child(#{newPosition})"
		$(sel).after(moved)
		@savePositions()
		return

	# Insert edit form for specify question
	#
	# @param id [Integer] Id of the question
	#	@param source [String] Source of the edit form
	#
	insertEditForm: (id, source) ->
		el = $(@sourceId).find("li[question-data='#{id}']").first()
		$(el).find(".q-edit").html(source)
		return

	# Update question view
	# 
	# @param id [Integer] Is of the question
	# @param source [String] Source of updated question
	#
	updateQuestion: (id, source) ->		
		el = $(@sourceId).find("li[question-data='#{id}']").first()
		$(el).html($(source).html())
		return

	# Show edit form and hide question view
	#
	# @param id [Integer] Id of the question
	#
	showQuestionEditForm: (id) ->
		el = $(@sourceId).find("li[question-data='#{id}']").first()
		$(el).find('.question-item').addClass('active')
		$(el).find(".q-view").css('display', 'none')
		$(el).find(".q-control").css('display', 'none')
		$(el).find(".q-edit").css('display', 'block')
		@setSortableStatus(false)
		return

	# Show question view and hide edit form
	# 
	# @param id [Integer] id of the question
	#
	showQuestionView: (id) ->
		el = $(@sourceId).find("li[question-data='#{id}']").first()
		$(el).find(".q-view").css('display', '')
		$(el).find(".q-control").css('display', '')
		$(el).find(".q-edit").css('display', 'none').html('')		
		$(el).find('.question-item').removeClass('active').removeClass('new')
		@setSortableStatus(true)
		return

	# Refresh question numeration
	#
	refreshQuestionNumbers: ->
		index = 1
		$("li", @sourceId).each (ind, el) ->
			if $(".numeration", el).length isnt 0
				num = $(".numeration", el).first()
				$(num).text("#{index}.")
				index++
			return
		return

	# Enable or disable sortable for source list
	#
	# @param value [Boolean] The value that specified sortable status
	#
	setSortableStatus: (value) ->
		$(@sourceId).sortable(if value then "enable" else "disable")
		return