Admin.AddButtonTree = Ext.extend(Admin.GeneralTree, {
  border:true,
  dataName: 'stuff',
  
  initComponent: function() {
    this.addButton = new Ext.Toolbar.Button({ text: this.addButtonTitle,  ownerTree : this,
      handler : function(btn) {
        document.location = "/admin/" + btn.ownerTree.childDataName + "/new/" + btn.ownerTree.sel;
      }
    });
    
    
    Ext.apply(this,{
      tbar : [ this.addButton ]
    });
    
    Admin.AddButtonTree.superclass.initComponent.apply(this, arguments);
  }
});