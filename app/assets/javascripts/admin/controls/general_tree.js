Admin.GeneralTree = Ext.extend(Ext.tree.TreePanel, {
  border:true,
  dataName: 'stuff',

  initComponent:function() {
    root = new Ext.tree.TreeNode({
      text: this.rootNode ? this.rootNode.title : 'root',
      draggable:true,
      id: this.dataName,
      expanded: true
    });

    this.root = root;


    Ext.apply(this, {
      rootVisible:  this.rootNode  ? true : false,
      animate:false,
      width: 300,
      root: root,
      enableDD: this.enableDD,
      autoScroll : true,
      loader: new Ext.tree.TreeLoader(), // Note: no dataurl, register a TreeLoader to make use of createNode()
      lines: true,
      containerScroll: true
    });

    dataName = this.dataName;
    
    if( this.enableDD) {
      this.on("nodedragover", this.dragOver );                                                                                 
      this.on("nodedrop",     this.dragDrop );    
    }

    for(var i = 0; i < controllerInfo[dataName].length; i++) {
      root.appendChild(this.getLoader().createNode(controllerInfo[dataName][i]));
    }
    Admin.GeneralTree.superclass.initComponent.apply(this, arguments);
  },


  onRender:function() {
    Admin.GeneralTree.superclass.onRender.apply(this, arguments);
  },
  
  dragOver: function(e) {
      return ((e.data.node.parentNode == e.target.parentNode) && (e.point != "append"));
  },

  dragDrop: function(e) {
    callToRemote('/admin/' + this.dataName + '/reorder/?node=' + e.dropNode.id + "&point=" + e.point + "&target=" + e.target.id);
  }
});

