Admin.NavBar = Ext.extend(Ext.Panel, {
  initComponent:function() {
    Ext.apply(this, {
      region: 'north',
      border: false,
      height: 'auto',
      items: [
        new Ext.Toolbar({
          style : { border : 0 },
          items: [
            new Ext.Toolbar.SplitButton({
              text : 'Проекты',
              handler : function() { document.location = "/admin/projects"; },
              pressed : controllerInfo.section == "projects",
              menu : [
                { text : 'Добавить проект', handler : function() { document.location = "/admin/projects/new" ;}}
              ]
            }),
            new Ext.Toolbar.SplitButton({
              text : 'Кандидаты',
              handler : function() { document.location = "/admin/users"; },
              pressed : controllerInfo.section == "users",
              menu : [
                { text : 'Добавить кандидата', handler : function() { document.location = "/admin/users/new" ;}}
              ]
            }),
            new Ext.Toolbar.SplitButton({
              text : 'Опросы',
              handler : function() { document.location = "/admin/quiz"; },
              pressed : controllerInfo.section == "quiz",
              menu : [
                { text : 'Добавить опрос', handler : function() { document.location = "/admin/quiz/new" ;}}
              ]
            }),
            new Ext.Toolbar.Button({
              text : 'Статистика',
              handler : function() { document.location = "/admin/statistics"; },
              pressed : controllerInfo.section == "statistics"
            }),
            new Ext.Toolbar.Button({
              text : 'Страницы',
              handler : function() { document.location = "/admin/pages"; },
              pressed : controllerInfo.section == "pages"
            }),  
            new Ext.Toolbar.Button({
              text : 'Персонализация',
              handler : function() { document.location = "/admin/properties"; },
              pressed : controllerInfo.section == "properties"
            }),          
            new Ext.Toolbar.Button({
              text : "Настройки",
              pressed : controllerInfo.section == "settings",
              handler : function() { document.location  = "/admin/settings"; }
            }),
          	new Ext.Toolbar.Spacer(),
            new Ext.Toolbar.TextItem("<span id=\"spinner\" style=\"display:none;\"><img alt=\"spinner\" src=\"../images/spinner.gif\" align= \"bottom\"/></span>")
          ]
        })
      ]    
    });
    Admin.NavBar.superclass.initComponent.apply(this, arguments);
  },    

  onRender:function() {
    Admin.NavBar.superclass.onRender.apply(this, arguments);
  }
});

Ext.reg('navbar', Admin.NavBar);
