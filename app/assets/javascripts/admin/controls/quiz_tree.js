Admin.QuizTree = Ext.extend(Ext.tree.TreePanel, {
  border:true,
  dataName: 'stuff',

  initComponent:function() {
    root = new Ext.tree.AsyncTreeNode({
      text: 'Все опросы',
      draggable: false,
      id: '0',
      expanded: true
    });

    loader = new Ext.tree.TreeLoader({
      clearOnLoad : true,
      url : "/admin/quiz/list/"
    });

    this.root = root;


    this.addButton = new Ext.Toolbar.Button({ 
      text: '',  ownerTree : this, handler : this.addQuizItem, disabled: true
    });

    Ext.apply(this, {
      rootVisible: false,
      animate:false,
      width: 300,
      root: root,
      enableDD: true,
      autoScroll : true,
      tbar : [ this.addButton ],
      loader: loader, // Note: no dataurl, register a TreeLoader to make use of createNode()
      lines: true,
      containerScroll: true
    });

    dataName = this.dataName;
   
    this.on("nodedragover", this.dragOver );                                                                                 
    this.on("nodedrop", this.dragDrop );
    
    Admin.QuizTree.superclass.initComponent.apply(this, arguments);
    
   
  },


  onRender:function() {
    Admin.QuizTree.superclass.onRender.apply(this, arguments);
  },

  elaborateAddButton: function(node) {
    if (node.attributes.meta && node.attributes.meta.add) {
      node.ownerTree.addButton.setText(node.attributes.meta.add_label);
      node.ownerTree.addButton.addItemUrl = node.attributes.meta.add_url;
      node.ownerTree.addButton.nodeSelected = node;
      node.ownerTree.addButton.enable();
    } else {
      node.ownerTree.addButton.setText('');
      node.ownerTree.addButton.disable();
    }
  },

  addQuizItem : function(btn) {
    callToRemote(btn.addItemUrl);
  },
  
  dragOver: function(e) {
    return ((e.data.node.parentNode == e.target.parentNode) && (e.point != "append") && e.data.node.text.match('factor-'));
  },
  
  dragDrop: function(e) {
    callToRemote('/admin/factor/reorder/?node=' + e.dropNode.id + "&point=" + e.point + "&target=" + e.target.id);
  }

});

