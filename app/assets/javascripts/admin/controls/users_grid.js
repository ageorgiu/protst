Admin.UsersGrid = Ext.extend(Ext.ux.DynamicGridPanel, {
  PAGE_SIZE: 20,
  border:false,


  initComponent:function() {

    this.searchItems  = new Ext.form.TextField({ width: 250, id : 'items-word-filter', ownerGrid : this, enableKeyEvents : true });
    this.searchItems.on('keyup',  this.filterChange );
    this.searchItems.on('specialkey',  this.filterChange );
    
    usersGrid = this;
    
    this.notifier    = new Ext.Toolbar.Button({
      usersGrid : this,
      text : 'Отправить уведомление...',
      handler : function() {  usersGrid.sendNotifictions(this.usersGrid); }
    });
    
    this.excelExport = new Ext.Toolbar.Button({
        usersGrid : this,
        text : 'Excel',
        handler : function() { document.location = "/admin/userquiz/excel_export.xls?" +  Ext.urlEncode(this.usersGrid.store.baseParams); }
    })

    Ext.apply(this, {
      stripeRows: true,
      selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
      border: false,
      columns: this.columns,
      view: new Ext.grid.GridView({
        forceFit: true,
        autoFill: true
      }),
      tbar : [ this.notifier,  this.excelExport ,'-', this.searchItems]
    });

    Admin.UsersGrid.superclass.initComponent.apply(this, arguments);
  },

  filterChange: function() {
    this.ownerGrid.store.baseParams['word'] = this.getValue();
    this.ownerGrid.store.reload();
  },
  
  
  sendNotifictions : function(usersGrid) {
    if(noticeWindow == null) {
      noticeWindow = new Admin.NoticeWindow();
    }
    
    noticeWindow.show();
    noticeWindow.load(
      {
        url : '/admin/notice/notice_window_content/?' + Ext.urlEncode(usersGrid.store.baseParams),
        scripts : true,
        callback : function() { 
          grid = new Ext.ux.grid.TableGrid("notify-users-table", {  width: 330,  height: 300, applyTo: 'notifiers-list', autoScroll : true, layout : 'fit', viewConfig : { forceFit : true, autoHeight: true },listeners: {
							viewready: function(){show_all();}
					} });
          grid.render();
        }
      }
    );
  },
  
  
  onRender:function() {
    Admin.UsersGrid.superclass.onRender.apply(this, arguments);
  }
});