Admin.GeneralGrid = Ext.extend(Ext.grid.GridPanel, {
  PAGE_SIZE: 20,
  border:false,


  initComponent:function() {
    var store = new Ext.data.Store({
      url: '/admin/' + this.dataName + '/list',
      baseParams: {
        done : false
      },
      remoteSort: false,
      reader: new Ext.data.JsonReader({
          root: 'items',
          totalProperty: 'total'
        }, 
        this.columns.map(function(column) { 
          return column.dataIndex
        }) 
      )
    });

    
    Ext.apply(this, {
      stripeRows: true,
      store: store,
      selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
      border: false,
      columns: this.columns,
      view: new Ext.grid.GridView({
        forceFit: true,
        autoFill: true
      })
    });
        
    if(this.searchField) {
      this.searchItems  = new Ext.form.TextField({ width: 250, id : 'items-word-filter', ownerGrid : this, enableKeyEvents : true });
      this.searchItems.on('keyup',  this.filterChange );
      this.searchItems.on('specialkey',  this.filterChange );
      Ext.apply(this,{
        tbar : [ '-', this.searchItems ]
      });
    }
    
    Admin.GeneralGrid.superclass.initComponent.apply(this, arguments);
  },

  filterChange: function() {
    this.ownerGrid.store.baseParams['word'] = this.getValue();
    this.ownerGrid.store.reload();
  },
  
  onRender:function() {
    Admin.GeneralGrid.superclass.onRender.apply(this, arguments);
  }
});
