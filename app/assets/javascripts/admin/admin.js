// ExtJS Setup
Ext.BLANK_IMAGE_URL = '/extjs/resources/images/default/s.gif';
Ext.ns('Admin');

Ajax.Responders.register({
onCreate: function(){ Element.show('spinner')},
onComplete: function(){Element.hide('spinner')}
});

// Wrapper for prototype Ajax Call
function callToRemote(url) {
  new Ajax.Request(url, {asynchronous:true, evalScripts:true});
}

function initUJS() {
  $$('.x-btn').each(function(button) {
    $(button).observe('mouseover', function() {$(button).addClassName("x-btn-over");});
    $(button).observe('mouseout',  function() {$(button).removeClassName("x-btn-over");});
    $(button).observe('mousedown', function() {$(button).addClassName("x-btn-click");});
    $(button).observe('mouseup',   function() {$(button).removeClassName("x-btn-click");});
  });
  
  $$('.upload').each(function(upload) {
    var fu =  new Ext.ux.form.FileUploadField({ el: upload.id, name: upload.name, autoCreate: true, width: '250px' });
    fu.render();
  });
  
  
  $$('.wysiwyg').each(function(textarea) {
    var oFCKeditor = new FCKeditor(textarea.id, textarea.getStyle('width'), 400);                                                                                              
    oFCKeditor.BasePath = "/javascripts/fckeditor/";                                                                                                                                             
    oFCKeditor.ToolbarSet = 'Admin';                                                                                                                                                             
    oFCKeditor.ReplaceTextarea() ;                                                                                                                                                               
  });
}

function getEditorsData() {                                                                                                                                                                        
  $$('.wysiwyg').each(function(editor) {                                                                                                                                                         
    editor.value =  FCKeditorAPI.GetInstance(editor.id).GetXHTML();                                                                                                                                
  });                                                                                                                                                                                           
}                                                                                                                                                                                                  