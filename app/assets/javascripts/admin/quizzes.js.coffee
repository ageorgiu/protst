#= require admin/question_editor

$ ->
	
	questionEditor = new QuestionEditor("#quiz-source", "#quiz-buffer")	

	$("#question-editor").data('api', questionEditor)

	$(window).scroll ->
		marginTo = 0
		if $(window).scrollTop() <= 64 
			marginTo = 0
		else 
			marginTo = $(window).scrollTop() - 64
		$("#quiz-buffer-wrapper").stop().animate({"marginTop": (marginTo + 0) + "px"}, "slow" )  if $("#quiz-buffer-wrapper").length
		if $(window).scrollTop() <= 64 
			marginTo = 0
		else 
			marginTo = $(window).scrollTop() - 64
		$(".add-panel").stop().animate({"marginTop": (marginTo + 0) + "px"}, "slow" ) if $(".add-panel").length

	$("#buffer-select-all").click ->
		questionEditor.bufferSelectAll()
		return false

	$("#buffer-unselect-all").click ->
		questionEditor.bufferUnselectAll()
		return false

	$("#source-unselect-all").click ->
		questionEditor.sourceUnselectAll()
		return false

	$("#remove-selected").click ->
		questionEditor.removeQuestionGroup()
		return false

	$("#buffered-selected").click ->
		questionEditor.bufferingSelected()
		return false

	questionEditor.setEmptyBuffer()

	$(".nav-header").click ->
		data = $(this).attr('data-toggle')
		$(this).toggleClass('rolled expanded')
		$("[data-element="+data+"]").toggle()