Admin.AddButtonPage = Ext.extend(Ext.Viewport, {
  border: false,

  initComponent:function() {
    this.itemsTree = new Admin.AddButtonTree({region:'west', split: true, dataName : this.dataName, childDataName : this.childDataName, enableDD : this.enableDD });
    this.itemsTree.on('click', this.chooseItem);
    
    Ext.apply(this, {
      layout: 'border',
      items: [
        new Ext.Panel({ items: null, region: 'center', split : true, border: false,  baseCls : 'form', contentEl:'content', autoScroll : true}),
        this.itemsTree,
        new Admin.NavBar()
      ]
    });
    Admin.AddButtonPage.superclass.initComponent.apply(this, arguments);
  },

  onRender:function() {
    Admin.AddButtonPage.superclass.onRender.apply(this, arguments);
  },
  
  postInit: function() {
    
  },
  
  chooseItem:function(node,e){
    node.ownerTree.sel = node.id;
    callToRemote('/admin/' + node.ownerTree.dataName + '/edit/' + node.id);
  }
});
