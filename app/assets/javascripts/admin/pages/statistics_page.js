Admin.StatisticsPage = Ext.extend(Ext.Viewport, {
  border: false,

  initComponent:function() {
  
    this.projectsTree = new Admin.GeneralTree({region:'west', rootNode : { visible : true, title : 'Проекты' } , title : 'Проекты', height: 350, split: true, dataName: 'project', enableDD : false });
    this.projectsTree.owner = this;

    this.usersGrid  =  new Admin.UsersGrid({region:'north',  split: true, height: 300, 
      storeUrl: '/admin/userquiz/list_json/',  sm: new Ext.grid.CheckboxSelectionModel()    });
    
    
    Ext.apply(this, {
      layout: 'border',
      items: [
        new Ext.Panel({
          layout : 'border',
          region: 'center',
          items : [
            this.usersGrid,
            
            new Ext.TabPanel({ 
              items: [
                { title : 'Ответы',       contentEl : 'answers-area',      baseCls : 'form', autoScroll :true },
                { title : 'Статистика',   contentEl : 'statistics-area',   baseCls : 'form', autoScroll :true},
                { title : 'Отчет',        contentEl : 'report-area',       baseCls : 'form', autoScroll :true }
              ],
              region: 'center',
              activeTab : 0, 
              split : true,  border: true,
              baseCls : 'form'
            })
          ]
        }),
        this.projectsTree,
        { xtype: 'navbar', region: 'north', height: 'auto' }
      ]
    });
    this.projectsTree.rootVisible = true;
    this.projectsTree.getRootNode().text = "Все проекты";
    this.projectsTree.on('click', this.selectProject, this);
    this.usersGrid.store.load();
    Admin.StatisticsPage.superclass.initComponent.apply(this, arguments);
  },

  onRender:function() {
    Admin.StatisticsPage.superclass.onRender.apply(this, arguments);
  },

   
  selectProject: function(node,e) { 
    this.projectId = node.id;
    this.usersGrid.store.baseParams.project_id = node.id;
    this.usersGrid.store.load();
  },

  
  
  showQuizStatistics: function(grid, index, e) {
    if(this.quizId !=0 && this.projectId != 0 ) {
      callToRemote("/admin/statistics/user_page/?quiz_id="+this.quizId + '&project_id=' + this.projectId + 
        '&user_id=' + grid.getSelectionModel().getSelected().data.id);
    }
  }
});
