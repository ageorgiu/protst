Admin.LeftTreePage = Ext.extend(Ext.Viewport, {
  border: false,

  initComponent:function() {
    this.itemsTree = new Admin.GeneralTree({region:'west', split: true, dataName : this.dataName, enableDD : this.enableDD });
    this.itemsTree.on('click', this.chooseItem);
    
    Ext.apply(this, {
      layout: 'border',
      items: [
        new Ext.Panel({ items: null, region: 'center', split : true, border: false,  baseCls : 'form', contentEl:'content', autoScroll : true}),
        this.itemsTree,
        { xtype: 'navbar', region: 'north', height: 'auto' }
      ]
    });
    Admin.LeftTreePage.superclass.initComponent.apply(this, arguments);
  },

  onRender:function() {
    Admin.LeftTreePage.superclass.onRender.apply(this, arguments);
  },
  
  postInit: function() {
    
  },
  
  chooseItem:function(node,e){
    callToRemote('/admin/' + node.ownerTree.dataName + '/edit/' + node.id);
  }
});
