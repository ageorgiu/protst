Admin.QuizConstructorPage = Ext.extend(Ext.Viewport, {
  border: false,

  initComponent:function() {
    this.itemsTree = new Admin.QuizTree({region:'west', split: true, enableDD : false });
    this.itemsTree.on('click', this.chooseItem);

    Ext.apply(this, {
      layout: 'border',
      items: [
        new Ext.TabPanel({
          items: [
            { title : '<span id="editor-tab">Вопросы</span>',         contentEl : 'questions-area',  baseCls : 'form', autoScroll :true },
            { title : 'Параметры', contentEl : 'settings-area',        baseCls : 'form', autoScroll :true },
            { title : 'Статистика', contentEl : 'statistics-area',        baseCls : 'form', autoScroll :true }
          ],
          region: 'center',
          activeTab : 0,
          split : true,  border: true,
          baseCls : 'form'
        }),
        this.itemsTree,
        { xtype: 'navbar', region: 'north', height: 'auto' }
      ]
    });
    Admin.QuizConstructorPage.superclass.initComponent.apply(this, arguments);
  },

  onRender:function() {
    Admin.QuizConstructorPage.superclass.onRender.apply(this, arguments);
  },

  chooseItem:function(node,e) {
    node.ownerTree.elaborateAddButton(node);

    if (String(node.id).match(/f-list/) || String(node.id).match(/c-list/)) {
      return; // Выбрали просто узел
    } else if(String(node.id).match(/factor/)) {
      callToRemote('/admin/factor/edit/' + node.id.split('-')[1]);
    } else if (String(node.id).match(/report/)){
      callToRemote('/admin/report/edit/' + node.id.split('-')[1]);
    } else {
      node.ownerTree.sel = node.id;
      callToRemote('/admin/quiz/edit/' + node.id);
    }
  }
});
