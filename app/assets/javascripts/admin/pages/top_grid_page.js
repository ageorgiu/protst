Admin.TopGridPage = Ext.extend(Ext.Viewport, {
  border: false,

  initComponent:function() {
    this.itemsGrid = new Admin.GeneralGrid({region:'north',  split: true, height: 300, 
      columns : this.columns, dataName : this.dataName, searchField : this.searchField });
    
    Ext.apply(this, {
      layout: 'border', region: 'center',
      items: [
        { xtype: 'navbar', region: 'north', height: 'auto' },
        new Ext.Panel({
          region : 'center',
          layout: 'border',
          border: false,   
          items: [
            this.itemsGrid,
            new Ext.Panel({ region: 'center', split: true, border: false, baseCls : 'form', contentEl: 'content', autoScroll : true})              
          ]
        })
      ]
    });

    this.itemsGrid.store.load();
    this.itemsGrid.on('rowclick', this.selectItem);
    Admin.TopGridPage.superclass.initComponent.apply(this, arguments);
  },

  onRender:function() {
    Admin.TopGridPage.superclass.onRender.apply(this, arguments);
  },

  selectItem: function(grid, index, e) {
    itemId = grid.getSelectionModel().getSelected().data.id;
    callToRemote('/admin/' + this.dataName + '/edit/' + itemId );
  }
});
