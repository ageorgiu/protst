function send_answer(url, answer){
  var full_url = url + "?value="+answer;
  $("#navigation").hide();
  $.ajax({
    url:full_url,
    cache: false,
    success : function(){$("#navigation").show();},
    error : function(){
      $("#navigation").show();
      alert("Последний ответ не обработался. Попробуйте ответить заново.");
    }
  });
}
$(function(){
    $(".observer input[type=radio]").click(function(){
      send_answer($(this).parents(".observer:first").data("url"), $(this).val());
    });
    $(".observer select").change(function() {
      send_answer($(this).parents(".observer:first").data("url"), $(this).val());
    });
    $(".observer textarea").mouseleave(function(){
      send_answer($(this).parents(".observer:first").data("url"), $(this).val());
    });
    $(".observer ul.sortable-list").each(function(index, range){
      $(range).sortable({
        stop: function(event, ui){
          var url = $(range).parents(".observer:first").data("url");
          var answer = "";
          $(range).children("li").each(function(index, item){
            if(answer!="")
            {
              answer +=",";
            }
            answer +=$(item).data("value");
          });
          send_answer(url, answer);
        }
      });
    });
  }
);