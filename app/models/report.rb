# -*- encoding : utf-8 -*-
class Report < ActiveRecord::Base
  belongs_to :quiz
  has_many   :interpretations, :dependent => :destroy
  has_many   :project_quizzes, :dependent => :destroy
  has_many :factors, through: :quiz

  accepts_nested_attributes_for :factors, :interpretations, allow_destroy: true

  def find_interpretation(factor, sten)
    interpretations.by_factor_and_sten(factor, sten).first
  end

  def to_s
    title
  end
end
# == Schema Information
#
# Table name: reports
#
#  id         :integer(4)      not null, primary key
#  quiz_id    :integer(4)      default(0), not null
#  title      :string(255)     not null
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#

