# -*- encoding : utf-8 -*-
#encoding: utf-8
class Page < ActiveRecord::Base
  
  acts_as_tree :order => 'weight'
  
  scope :visible, :conditions => { :visible => true}
  
  def url(user)
    case self.special 
    when 'home'               then return { :controller => 'home', :action => 'index'  }
    when 'quiz'               then return { :controller => 'quiz', :action => 'index' }
    when 'thank_for_register' then return { :controller => 'auth', :action => 'thanks_for_register' }
    end
    
    if self.special == 'register' 
      url = user.blank? ? { :controller => 'auth', :action => 'register' } : { :controller => 'auth', :action => 'profile' }
      return url
    end
    
  end
  
  def site_title(user)
    if self.special == 'register' 
      title = user.blank? ? 'Регистрация' : 'Анкета'
      return title
    end
    return self.title
  end
  
end

# == Schema Information
#
# Table name: pages
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)     not null
#  weight     :integer(4)      default(0), not null
#  parent_id  :integer(4)      default(0), not null
#  special    :string(255)     not null
#  visible    :boolean(1)      default(TRUE), not null
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#

