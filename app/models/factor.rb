# -*- encoding : utf-8 -*-
class Factor < ActiveRecord::Base
  belongs_to :quiz
  has_many   :question_factors, :dependent => :destroy
  has_many   :questions, :through => :question_factors
  has_many :interpretations

  accepts_nested_attributes_for :interpretations, allow_destroy: true

  default_scope order('factors.weight asc')

  scope :groups, where("factors.parent_id=0 OR factors.parent_id IS NULL")

  acts_as_tree

  validates_presence_of :quiz, :title
  
  def admin_tab_title
    quiz.belbin? ? 'Утверждения на этот фактор' : 'Вопросы на этот фактор'
  end

  # Юзеры, завершившие опрос и ответившие на вопросы по этому фактору
  # Фишка в том, что факторы и вопросы могут быть добавлены уже после того, как какие-то юзеры
  # начали проходить опрос, поэтому ориентироваться на статистику по "прошедшим" опрос неправильно
  def respondents
    # Должен быть хоть один вопрос на этот фактор :)
    if question  = questions.first
      # Ищем всех отвечавших на вопросы по этому фактору

      uid_list        = question.answers.order(:user_id).values_of(:user_id)
      # Ищем всех завершивших опрос
      completed_uids  = UserQuiz.completed.where(:quiz_id => quiz_id).order(:user_id).values_of(:user_id)

      # Упорядоченность пользователей нужна для корректных подсчетов корреляции

      User.where(:id => (uid_list & completed_uids))
    else
      []

    end
  end



  # Количество баллов по фактору
  def value(user)
    Answer.where(:user_id=>user, :question_id=>question_ids).sum(:value)
  end

  def values(users)
    Answer.where(:user_id=>users.map(&:id), :question_id=>question_ids).group(:user_id).order(:user_id).sum(:value).map { |v| v[1].to_f }
  end

  # Z-баллы
  def zvalue(user)
    Calculation.zvalue(value(user), variance, mean)
  end

  # Стэны

  def sten(user)
    Calculation.sten(zvalue(user))

  end

  def alpha
    questions_count = questions.count
    sum_answers_variances =  questions.map(&:variance).sum
    sum_answers_square_variances =  questions.map(&:variance).map {|e| e ** 2}.sum
    return 0 if questions_count < 2
    # binding.pry
    # (questions_count.to_f/(questions_count - 1))*(1-sum_answers_variances/(variance ** 2))
    (questions_count.to_f/(questions_count - 1))*((variance ** 2 - sum_answers_square_variances)/(variance ** 2))
  end

  # Какие значания сырых баллов для данного стена
  
  def value_by_sten(sten)
    variance*((sten-5.5)/2) + mean
  end

  def elaborate_statistics
    # binding.pry
    self.mean, self.variance = Calculation.mean_and_variance(values(respondents))
  end
end
# == Schema Information
#
# Table name: factors
#
#  id         :integer(4)      not null, primary key
#  quiz_id    :integer(4)      default(0), not null
#  title      :string(255)     not null
#  parent_id  :integer(4)      default(0), not null
#  lft        :integer(4)      default(0), not null
#  rgt        :integer(4)      default(0), not null
#  kind       :integer(4)
#  created_at :datetime
#  updated_at :datetime
#  mean       :float           default(0.0), not null
#  variance   :float           default(0.0), not null
#  weight     :integer(4)      default(0), not null
#

