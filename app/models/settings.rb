# -*- encoding : utf-8 -*-
#encoding: utf-8
class Settings < ActiveRecord::Base
  has_attached_file :data, 
    :styles => { :cms =>  '120x120>', :thumb => '210x210#' },
    :url   => "/system/data/:id/:style/data.:extension",  
    :path  => ":rails_root/public/system/data/:id/:style/data.:extension",
    :convert_options => {
      :thumb => '-frame 1x1'
    }
  
  def self.get(key)
    setting = Settings.find(:first, :conditions => [ 'setting = ?', key.to_s ])
    unless setting.nil? then
      return (setting.value == '1') if (setting.special == 'checkbox')
      return setting.value
    else
      return "Ошибка настроек: " + key.to_s
    end
  end  
end

# == Schema Information
#
# Table name: settings
#
#  id                :integer(4)      not null, primary key
#  title             :string(255)     not null
#  setting           :string(255)     not null
#  comment           :text
#  value             :text
#  special           :string(255)     default("string"), not null
#  data_file_name    :string(255)
#  data_content_type :string(255)
#  data_file_size    :integer(4)
#  data_updates_at   :datetime
#  created_at        :datetime
#  updated_at        :datetime
#

