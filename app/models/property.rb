# -*- encoding : utf-8 -*-
class Property < ActiveRecord::Base
  
  default_scope order(:weight)

  scope :education_levels, where(type: "EducationLevel")
  scope :education_types, where(type: "EducationType")
  scope :cities, where(type: "City")
  scope :job_areas, where(type: "JobArea")
  scope :job_expiriences, where(type: "JobExpirience")
  scope :job_positions, where(type: "JobPosition")

  scope :title_like, lambda{|query| where("title like ?", "#{query}%")}
  
  validates_length_of :title,
    :minimum=>2

  attr_accessible :title, :type

  def self.inherited(child_class)
    super(child_class)
    child_class.class_eval do
      has_many :users, :dependent=>:nullify
    end
  end
  
  # Возвращает id свойства по его роли и заголовку. Актуально при импорте данных
  def self.find_or_create_id_by_title(title, role)
    return 0 if title.blank?
    property = Property.find(:first, :conditions => ['title = ? AND role = ?', title.capitalize(:ru), role] )
    return property.id unless property.blank?
    return Property.create(:title => title.capitalize(:ru), :role => role ).id
  end

  def to_s
    title
  end

end


# == Schema Information
#
# Table name: properties
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)     not null
#  weight     :integer(4)      default(0), not null
#  created_at :datetime
#  updated_at :datetime
#  type       :string(255)
#

