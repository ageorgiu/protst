# -*- encoding : utf-8 -*-
#encoding: utf-8
class Quiz < ActiveRecord::Base

  include Import  # Код для импорта из excel
  include QuizKinds

  has_many :factors, :dependent=>:destroy, :conditions=>"factors.parent_id!=0 and factors.parent_id is not null"
  has_many :factor_groups, :dependent=>:destroy, :class_name=>"Factor", :conditions=>"factors.parent_id=0 or factors.parent_id is null"
  has_many :reports
  has_many :user_quizzes, :dependent=>:destroy
  has_many :completed_user_quizzes, :class_name=>'UserQuiz', :conditions => {:complete=>true}
  has_many :completed_users, :through=>:completed_user_quizzes, :source=>:user
  has_many :profile_charachteristics, :dependent=>:destroy

  has_many :questions, :class_name => 'Questions::Question', :order => 'weight',
    :dependent => :destroy, :before_add=>:set_weight

  has_many :project_quizzes, :dependent=>:destroy
  has_many :projects, :through=>:project_quizzes

  accepts_nested_attributes_for :profile_charachteristics, :allow_destroy=>true

  validates_presence_of :title, :kind

  validates_inclusion_of :kind, :in=>kind_values

  before_save :elaborate_statistics, :if=>:kind_changed?
  after_initialize :set_defaults, :unless=>:persisted?

  def answerable_questions
    @answerable_questions ||= questions.answerable
  end
  
  def belbin_quesion_items
    @belbin_quesion_items ||= questions.where(:type => "Questions::BelbinItem")
  end
  
  def factor_view_questions
    belbin? ? belbin_quesion_items : answerable_questions
  end

  def profile_questions
    questions.where(:profile=>true)
  end

  def viewed_questions
    questions.viewed
  end

  def answerable_question_ids
    @answerable_question_ids ||= answerable_questions.values_of(:id)
  end

  def index(question)
     answerable_question_ids.index(question.id)
  end

  def position(question)
    custom_numeration ? question.numeration : index(question) + 1
  end

  def answers_for(user)    
    Answer.joins(:question).where(:user_id=>user, :question_id=>answerable_question_ids)
  end

  def get_profile_charachteristic(stens)
    result = profile_charachteristics.detect do |charachteristic|
      stens.group_by do |sten|
        (charachteristic.min_value..charachteristic.max_value).to_a.include? sten
      end[true].try(:size).to_i >= charachteristic.value
    end
    result.interpretation if result
  end

  def num_quiz_pages
    if limited_in_time?
      viewed_questions.count
    elsif custom_pagination?
      questions.where(:type => 'Questions::Divider').count + 1
    else
      (viewed_questions.count.to_f / page_size).ceil
    end
  end

  def page_questions(page=0)
    return [] if page < 0 or page >= num_quiz_pages
    if limited_in_time?
      [viewed_questions.offset(page).first]
    elsif custom_pagination?
      questions.editable.split{|q| Questions::Question.delimiter_names.include?(q.class.to_s)}[page]
    else
      viewed_questions.limit(page_size).offset(page*page_size)
    end
  end

  def elaborate_statistics
    if !interview? and calculating?
      factors.each do |factor|
        factor.elaborate_statistics
        factor.save
      end
    end
  end

  def forward
    limited_in_time ? true : super
  end

  alias_method :forward?, :forward

  def to_s
    title
  end

  private

    def set_weight(question)
      question.weight = questions.count
    end

    def set_defaults
      {
        :title=>"Новый опрос", :kind=> "interview",  :calculating=>true,
        :custom_pagination=> false, :page_size=> 5
      }.each do |attr_name, value|
        self.send("#{attr_name}=", value) if send("#{attr_name}").blank?
      end
    end

end
# == Schema Information
#
# Table name: quizzes
#
#  id                :integer(4)      not null, primary key
#  title             :string(255)     not null
#  created_at        :datetime
#  updated_at        :datetime
#  custom_pagination :boolean(1)      default(FALSE), not null
#  page_size         :integer(4)      default(5), not null
#  forward           :boolean(1)
#  kind              :string(255)
#  limited_in_time   :integer(4)
#  custom_numeration :boolean(1)
#  calculating       :boolean(1)      default(TRUE)
#

