# -*- encoding : utf-8 -*-
#encoding: utf-8
require 'digest/md5'

class User < ActiveRecord::Base

  [:project, :city, :education_level, :job_area, :job_expirience, 
    :job_position, :education_type].each do |master_model|

    belongs_to master_model
    delegate :title, :to=>master_model, :prefix=>true, :allow_nil=>true

  end

  has_many :answers, :dependent => :destroy
  has_many :answered_questions, :through=>:answers, :source=>:question
  has_many :user_quizzes, :dependent => :destroy
  has_many :quizzes, :through=>:user_quizzes

  before_validation :check_project

  validates_size_of         :login, :password, :within => 3..25, :message => ": длинна должна быть от 3 до 25 символов"
  validates_size_of         :name, :surname, :minimum => 3, :message => ": неправдоподобно короткое"
  validates_format_of :email, :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/, :message => ": неверный формат"
  validates :login, :uniqueness=>true, :presence=>true, :format=>{:with => /[A-Za-z0-9-_]+$/i}
  validates :password, :confirmation => true, :presence=>true

  with_options unless: :quick_registration? do |u|
    u.validates_presence_of :city, :job_expirience, :education_level, :birthday, :unless=>:custom?
    u.validates_inclusion_of :birthday, :in => Date.civil(1920, 1, 1)..Date.today, :message => "Неправдоподобная дата рождения"
    u.validates :gender, :presence=>true, :inclusion=>AppConfig.user_genders
  end

  default_scope order("users.id desc")

  scope :order_by_name, unscoped.order('surname, name')
  scope :by_project,   lambda { |project| where(:project_id => project) }
  scope :find_by_word, lambda {|word| includes(:project).where('LOWER(users.name) LIKE :word OR LOWER(users.surname) LIKE :word OR
   LOWER(users.patronymic) LIKE :word OR LOWER(projects.title) LIKE :word',:word=>"#{word}%") }

  def self.export_csv(options = {})
    CSV.generate(options) do |csv|
      csv << [:login, :password, :project_id, :surname, :name, :patronymic, :birthday, :email, :phone, :job_area_id, :job_expirience_id, :job_position_id, :city_id, :education_level_id, :education_type_id, :gender, :custom, :role]
      all.each do |user|
        csv << [user.login, user.password, user.project_id, user.surname, user.name, user.patronymic, user.birthday, user.email, user.phone, user.job_area, user.job_expirience, user.job_position, user.city, user.education_level, user.education_type, user.gender, user.custom, user.role]
      end
    end
  end

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      @update = row.to_hash

      # required
      @job_expirience = JobExpirience.where("title = ?", @update["job_expirience_id"])
      @city = City.where("title = ?", @update["city_id"])
      @education_level = EducationLevel.where("title = ?", @update["education_level_id"])

      # optional
      @job_area = JobArea.where("title = ?", @update["job_area_id"])
      @job_position = JobPosition.where("title = ?", @update["job_position_id"])
      @education_type = EducationType.where("title = ?", @update["education_type_id"])
      
      # binding.pry
      
      if !@job_expirience.blank? && !@city.blank? && !@education_level.blank?
        # required
        @update["job_expirience_id"] = @job_expirience.first.id
        @update["city_id"] = @city.first.id
        @update["education_level_id"] = @education_level.first.id

        # optional
        !@job_area.blank? ? @update["job_area_id"] = @job_area.first.id : nil
        !@job_position.blank? ? @update["job_position_id"] = @job_position.first.id : nil
        !@education_type.blank? ? @update["education_type_id"] = @education_type.first.id : nil

        @user = User.new(@update)
        @user.save
      end
    end
  end

  def self.gender_collection
    AppConfig.user_genders.map{|gender| [human_attribute_name(gender), gender]}
  end

  def project_quizzes
    project.quizzes
  end

  def humanized_gender
    self.class.human_attribute_name(gender)
  end

  def key
    Digest::MD5.hexdigest("#{surname} #{name} #{login} #{password} #{created_at}")[1..16]
  end

  def has_incomplete_quizzes?
    user_quizzes.count!=user_quizzes.completed.count
  end

  def self.generate(arg={}, user=nil)
    if user
      user.attributes = arg
    else
      user = new(arg)
    end
    suffix = Digest::MD5.hexdigest(Time.now.to_s)[0..12]
    {
      :login => 'user_' + suffix[0..9], 
      :password => suffix[4..12], 
      :password_confirmation => suffix[4..12],
      :project=> Project.default,
      :name=>'Иван', 
      :surname=> "Иванов", 
      :patronymic=> "Иванович", 
      :gender=>AppConfig.user_genders.first, 
      :birthday=>30.years.ago.to_date, 
      :email=>"ivanov@mail.com",
      :phone=> "",
      :restricted=>false, 
      :custom=>true
    }.each do |key, value|
      user.send("#{key}=", value) if user.send(key).nil? and arg[key].nil?
    end
    user
  end

  def age
    ((DateTime.now.to_date-birthday.to_date)/365).to_i if birthday
  end

  def to_json
    {
        :project         => project.blank? ? '' : project.title,
        :title           => title,
        :email           => email,
        :job_area        => job_area_title,
        :job_expirience  => job_expirience_title,
        :id=> id,
        :age=> age,
        :login => login
    }
  end

  def to_s
    "#{surname} #{name}"
  end  

  alias_method :title, :to_s

private

  def check_project
    self.project ||= Project.default
  end

end
# == Schema Information
#
# Table name: users
#
#  id                 :integer(4)      not null, primary key
#  login              :string(255)
#  password           :string(255)
#  project_id         :integer(4)      default(0)
#  name               :string(255)
#  surname            :string(255)
#  patronymic         :string(255)     not null
#  birthday           :date
#  email              :string(255)
#  phone              :string(255)
#  job_area_id        :integer(4)      default(0)
#  job_expirience_id  :integer(4)      default(0)
#  job_position_id    :integer(4)      default(0)
#  city_id            :integer(4)      default(0)
#  education_level_id :integer(4)      default(0)
#  education_type_id  :integer(4)      default(0)
#  created_at         :datetime
#  updated_at         :datetime
#  restricted         :boolean(1)      default(FALSE)
#  gender             :string(255)
#  custom             :boolean(1)
#

