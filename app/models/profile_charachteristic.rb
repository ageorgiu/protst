# -*- encoding : utf-8 -*-
class ProfileCharachteristic < ActiveRecord::Base
  belongs_to :quiz

  validates_presence_of :quiz_id, :min_value, :max_value, :value, :interpretation
  validate :edge_values

  def self.gup
    (1..10).to_a
  end

  private

    def edge_values
      errors[:edge] << '"До" должно быть больше "От"' unless max_value.to_i > min_value.to_i
    end

end

# == Schema Information
#
# Table name: profile_charachteristics
#
#  id             :integer(4)      not null, primary key
#  quiz_id        :integer(4)
#  min_value      :integer(4)
#  max_value      :integer(4)
#  interpretation :text
#  created_at     :datetime
#  updated_at     :datetime
#  value          :integer(4)      default(9)
#

