# -*- encoding : utf-8 -*-
class Notice < ActiveRecord::Base
  belongs_to :project
  
  # Этот аттрибут не сохраняется, нужен только для формы выбора шаблона
  attr_accessor :template
  
  def template
    return self.id
  end

end

# == Schema Information
#
# Table name: notices
#
#  id         :integer(4)      not null, primary key
#  owner_id   :integer(4)      default(0), not null
#  title      :string(255)     not null
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#

