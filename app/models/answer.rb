# -*- encoding : utf-8 -*-
class Answer < ActiveRecord::Base
  include UpdateWithoutTimestamping
  include HitsCounter
  acts_as_paranoid :column => 'deleted_at', :column_type => 'time'

  belongs_to :question,  :class_name => 'Questions::Question'
  belongs_to :user

  scope :scored, where("answers.value is not null")

  validates_presence_of :user, :question
  
  before_save :elaborate_value

  def full_answer
    content ? content : question.value_title(value)
  end

  def raw
    read_attribute(:raw) ? read_attribute(:raw) : content
  end

  def elaborate_value
    if raw!=0 and raw.present?
      if question.inverse?
        self.value = question.list_variants.size - raw.to_i + 1
      else
        self.value = raw.to_i
      end
    end
  end

  def belbin_answer_array
    result = []
    belbin_answers = view.split("\n")
    belbin_answers.each do |answer|
      answer_text, anwer_position = answer.split(" - ")
      result[anwer_position.to_i] = answer_text
    end
    result
  end
  
  def belbin_points_array
    points = []
    belbin_answers = view.split("\n")
    belbin_answers.each do |answer|
      answer_text, anwer_position = answer.split(" - ")
      points << anwer_position.to_i
    end
    points
  end
  
end
# == Schema Information
#
# Table name: answers
#
#  id          :integer(4)      not null, primary key
#  user_id     :integer(4)      default(0), not null
#  question_id :integer(4)      default(0), not null
#  value       :integer(4)
#  content     :text
#  created_at  :datetime
#  updated_at  :datetime
#  view        :text
#

