# -*- encoding : utf-8 -*-
class QuestionFactor < ActiveRecord::Base
  belongs_to :question,  class_name: 'Questions::Question'
  belongs_to :factor

  validates_uniqueness_of :question_id, scope: 'factor_id'

  def self.correlation(question, factor)
    users           = factor.respondents
    factor_values   = factor.values(users)
    question_values = question.answers.where(:user_id =>users).order('answers.user_id').values_of(:value)
    max = [factor_values.count, question_values.count].min

    # binding.pry
    begin
      return Statsample::Bivariate.pearson(question_values[0,max].to_scale, factor_values[0,max].to_scale)
    rescue Exception => e
      # binding.pry    
    end
  end

  def correlation(factor)
    self.class.correlation(question, factor)
  end
end

# == Schema Information
#
# Table name: question_factors
#
#  id          :integer(4)      not null, primary key
#  question_id :integer(4)      default(1)
#  factor_id   :integer(4)      default(1)
#  role        :integer(4)      default(1), not null
#  created_at  :datetime
#  updated_at  :datetime
#

