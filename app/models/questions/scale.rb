# -*- encoding : utf-8 -*-
class Questions::Scale < Questions::Question

  serialized_accessor :left_instruction, :right_instruction, :parity, :length, :in=>:content

  validates_presence_of :right_instruction, :left_instruction, :parity, :length

  validates_inclusion_of :parity, :in=>AppConfig.scale_parity.keys
  validates_numericality_of :length, :greater_than=>0

  def parity=(value)
    content[:parity] = value.to_i
  end

  def length=(value)
    content[:length] = value.to_i
  end

  def instruction
    "Если #{left_instruction} -#{length}, а #{right_instruction} #{length}"
  end

  def score_answer(answer)
    answer.present? ? answer.to_i : super(answer)
  end

  def value_title(raw)
    raw
  end

  def has_answer?
    return true
  end

  def inverse_new
    content[:inverse]
  end

  def inverse_new=(value)
    content[:inverse] = value.to_s
  end

private

  def set_defaults
    self.left_instruction ||= "Лево"
    self.right_instruction ||= "Право"
    self.length ||= 3
    self.parity ||= 1
  end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

