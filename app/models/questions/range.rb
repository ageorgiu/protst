# -*- encoding : utf-8 -*-
class Questions::Range < Questions::Question
  include QuestionVariants
  include QuestionRanges

  serialized_reader :right_sequences, :in=>:content

  def value_title(content)
    content
  end

  def has_answer?
    return true
  end

  def right_sequences=(sequences_hash={})
    content[:right_sequences] = sequences_hash.values.map do |item|
      next if item['sequence'].blank? or item['value'].blank?
      sequence = process_variant_input(item['sequence'])
      value = item['value'].to_i
      {:sequence=>sequence, :value=>value, :real_sequence=>item['sequence']}
    end.compact
  end

  def score_answer(answer)
    if answer =~/\A[\d,]+$/
      answer = answer.split(',').map(&:to_i).map{|index| variants[index]}
      right_sequences.each do |right_sequence|
        return right_sequence[:value] if answer == right_sequence[:sequence]
      end
    end
    super(answer)
  end

  def inverse_new
    content[:inverse]
  end

  def inverse_new=(value)
    content[:inverse] = value.to_s
  end

  private

    def set_defaults
      self.variants ||= %w[один три два].join(AppConfig.variant_splitter)
      self.instruction ||= 'В порядке возрастания'
      self.right_sequences ||= {:one=>{'sequence'=>%w[один два три].join(AppConfig.variant_splitter), 'value'=>4}}
    end

end

# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

