# -*- encoding : utf-8 -*-
class Questions::Open < Questions::Question
  include QuestionSizes

  has_one :answer, :foreign_key=>'question_id'

  def value_title(something=nil)
    answer.content
  end
  
  def inverse_new
    content[:inverse]
  end

  def inverse_new=(value)
    content[:inverse] = value.to_s
  end

  private

    def set_defaults
      self.instruction ||= 'Текст вопроса'
    end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

