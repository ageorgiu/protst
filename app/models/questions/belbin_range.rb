# -*- encoding : utf-8 -*-
class Questions::BelbinRange < Questions::Question

  has_many :belbin_items, :dependent=>:destroy, :foreign_key=>'parent_id', :before_add=>:sync_relation
  accepts_nested_attributes_for :belbin_items, :allow_destroy=>true

  def humanize_answer(input)
    if input.present?
      belbin_items.map{|belbin_item| belbin_item.humanize_answer(input)}.uniq.join("\n")
    else
      super input
    end
  end
  
  def factor_by_text(text)
    belbin_items.where(:instruction => text).first.try(:factor).try(:title)
  end

  def generate_answer(user, input=nil)
    # binding.pry
    belbin_items.each {|belbin_item| belbin_item.generate_answer(user, input)}
    super(user, input)
  end

  def inverse_new
    content[:inverse]
  end

  def inverse_new=(value)
    content[:inverse] = value.to_s
  end

  private

    def set_defaults
      self.instruction ||="Добавьте варианты и свяжите их с факторами"
    end

    def sync_relation(belbin_item)
      belbin_item.quiz = quiz
      true
    end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

