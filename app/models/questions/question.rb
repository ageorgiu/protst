# -*- encoding : utf-8 -*-
class Questions::Question < ActiveRecord::Base
  set_table_name "questions"

  scope :unbuffered, where(buffered: false)
  scope :buffered, where(buffered: true)

  include UpdateWithoutTimestamping
  include QuestionTypes

  belongs_to :quiz

  validates_presence_of :quiz

  after_initialize :set_defaults, :unless=>:persisted?

  def self.inherited(subklass)
    super
    return unless self == Questions::Question
    subklass.class_eval do
      include RelatedQuestionModels
    end
  end

  def inverse?
   self.inverse_new == '1' or self.inverse_new == 'true' or self.inverse_new == true
  end

  def self.path
    self.to_s.underscore.split('/').last
  end

  def number
    quiz.index(self)
  end

  def path
    self.class.path
  end

  private

    def set_defaults;end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

