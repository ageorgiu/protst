# -*- encoding : utf-8 -*-
class Questions::BelbinItem < Questions::Question
  belongs_to :belbin_range, :foreign_key=>'parent_id'


  def humanize_answer(input)
    score = score_answer(input)
    score ? "#{instruction} - #{score}" : super(input)
  end
  
  def factor
    factors.first
  end

  def score_answer(input)
    if input.is_a? String
      ids = input.split ','
      ids.each_with_index do |id, index|
        if id =~ /\d+/
          id = id.to_i
          return (ids.size - index) if id == self.id
        end
      end
    end
    super input
  end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

