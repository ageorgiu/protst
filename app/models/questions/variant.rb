# -*- encoding : utf-8 -*-
class Questions::Variant < Questions::Question

  include UpdateWithoutTimestamping

  attr_accessor :variants, :view_type, :numeration
  attr_accessor :variant # Заглушка для админства, не сохраняется

  WEIGHT_SPLITER = '%'
  VARIANT_SPLITER = "\n"

  WEIGHT_REGEXP = Regexp.new("#{WEIGHT_SPLITER}\\d+")

  module ViewType
    LIST         = '2'
    RADIOBUTTONS = '1'
  end

  DEFAULT_VALUES = {
    variants: 'Укажите список вариантов', view_type: ViewType::RADIOBUTTONS
  }


  def inverse_new
    content[:inverse]
  end

  def inverse_new=(value)
    content[:inverse] = value.to_s
  end

  def list?
    self.view_type_new.to_s == '1'
  end

  def radio?
    self.view_type_new.to_s == '2'
  end

  def has_answer?
    return true
  end

  def variants
    # binding.pry
    # @variants ||=  extract_custom_value(:variants)
    self.content[:variants].collect { |v| "#{v[:value]}%#{v[:weight]}" }.join("\n")
  end

  def numeration
    extract_custom_value(:numeration)
  end

  def variants=(value)
    self.content[:variants] = value.split(VARIANT_SPLITER).compact.collect do |item| 
      {value: item.gsub(WEIGHT_REGEXP, '').strip, weight: item[WEIGHT_REGEXP].to_s.delete(WEIGHT_SPLITER).to_i}
    end
  end

  def view_type=(value)
    content[:view_type] = value
  end

  def view_type_new=(value)
    content[:view_type] = value
  end

  def view_type
    self.content[:view_type]
  end

  def view_type_new
    self.content[:view_type]
  end

  def list_variants
    self.content[:variants].nil? ? [] : self.content[:variants].map{|item| item[:value]}
  end

  def list_variants_with_weight
    variants.split(VARIANT_SPLITER).compact.collect {|item| {value: item.gsub(WEIGHT_REGEXP, '').strip,
     weight: item[WEIGHT_REGEXP].to_s.delete(WEIGHT_SPLITER).to_i}}
  end

  def variant_value(variant)
    self.content[:variants].each_with_index do |item, index|
      if item[:value].downcase_ru == variant.downcase_ru
        return (item[:weight]==0) ? index+1 : item[:weight]
      end
    end
    0
  end

  def value_title(raw_value)
    raw_value ? list_variants[raw_value-1] : "Не успел ответить"
  end

private

  def extract_custom_value(value)
    # binding.pry
    # begin
    #   data = YAML::load(self.content)
    # rescue
    #   data = {}
    # end
    # data[value] || DEFAULT_VALUES[value]
    self.content[value]
  end
end