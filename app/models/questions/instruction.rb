# -*- encoding : utf-8 -*-
class Questions::Instruction < Questions::Question

  validates :instruction, :presence=>true, :length=>{:minimum=>20}

  def store(params)
    update_attributes(params)
  end
  
  def has_answer?
    return false
  end

  private

    def set_defaults
      self.instruction ||= "Описание перед опросами"
    end

end
# == Schema Information
#
# Table name: questions
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)     not null
#  weight      :integer(4)      default(0), not null
#  quiz_id     :integer(4)      default(0), not null
#  content     :text
#  instruction :text
#  created_at  :datetime
#  updated_at  :datetime
#  required    :boolean(1)      default(TRUE)
#  profile     :boolean(1)      default(FALSE)
#  view_type   :string(255)
#  numeration  :string(255)
#  parent_id   :integer(4)
#

