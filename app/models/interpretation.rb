# -*- encoding : utf-8 -*-
class Interpretation < ActiveRecord::Base
  belongs_to :report
  belongs_to :factor, :foreign_key=>'factor_id'

  scope :by_factor_and_sten, lambda { |factor, sten| where("factor_id = :factor and :sten >= sten_from and :sten <= sten_to", {:factor=>factor, :sten=>sten}) }
	scope :by_report_and_factor, lambda{ |report, factor| where(report_id: report, factor_id:factor) }  

  validates_numericality_of :sten_from, :sten_to
  validate :sten_range, :if=>'sten_to and sten_from'


  private

    def sten_range
      errors.add(:sten_from, :bigger_than_sten_to) if sten_from.to_i > sten_to.to_i
    end

end
# == Schema Information
#
# Table name: interpretations
#
#  id         :integer(4)      not null, primary key
#  factor_id  :integer(4)      default(0), not null
#  report_id  :integer(4)      default(0), not null
#  sten_from  :integer(4)      default(0), not null
#  sten_to    :integer(4)      default(0), not null
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#

