# -*- encoding : utf-8 -*-
#encoding: utf-8
class ProjectQuiz < ActiveRecord::Base

  include UpdateWithoutTimestamping

  [:quiz, :project, :report].each do |master_model|
    belongs_to master_model
    validates_presence_of master_model
    delegate :title, :to=>master_model, :prefix=>true
  end

  after_initialize :set_defaults, :unless=>:persisted?

  # Один опрос может встречаться в любом проекте только один раз
  validates_uniqueness_of :quiz_id, scope: :project_id, message: "Custom message"
  validate :different_quiz, :if=>"quiz and report"

  private

    def different_quiz
      errors.add(:report, :different_quiz) if report.quiz != quiz
    end

    def set_defaults
      self.announce ||= 'Краткое описание опросника'
      self.instruction ||= 'Инструкции по прохожению опросника'
      self.epilog ||= "Текст после окончания опросника"
    end

end
# == Schema Information
#
# Table name: project_quizzes
#
#  id                 :integer(4)      not null, primary key
#  project_id         :integer(4)
#  quiz_id            :integer(4)
#  report_id          :integer(4)
#  created_at         :datetime
#  updated_at         :datetime
#  show_report        :boolean(1)
#  show_quiz_title    :boolean(1)
#  escape_instruction :boolean(1)
#  announce           :text
#  instruction        :text
#  mark_required      :boolean(1)
#  epilog             :text
#

