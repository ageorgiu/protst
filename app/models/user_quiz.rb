# -*- encoding : utf-8 -*-
class UserQuiz < ActiveRecord::Base

  include HitsCounter
  acts_as_paranoid :column => 'deleted_at', :column_type => 'time'
  
  belongs_to :user
  belongs_to :quiz

  scope :completed, where(complete: true)
  scope :uncompleted, where(complete: nil)

  before_save :elaborate_statistics, :if=>:complete_changed?

  class << self


    def not_completed?(user, quiz)
      completed.where(:quiz_id=>quiz, :user_id=>user).blank?
    end

    def is_started?(user, quiz)
      where(quiz_id: quiz, user_id: user).any?
    end

    def get(user, quiz)
      find_by_user_id_and_quiz_id(user, quiz)
    end

    def run(user, quiz)
      get(user, quiz) || create(:user => user, :quiz => quiz)
    end

  end

  def unanswered_questions
    quiz.questions.not_answered_by(user)
  end

  def complete!
    # answer unrequired question with nil value
    # required for statistic
    unanswered_questions.unrequired.each do |question|
      question.generate_answer(user)
    end

    questions = unanswered_questions.required
    if quiz.limited_in_time
      questions.each do |question|
        question.generate_answer(user)
      end
    else
      if questions.present?
        return questions
      end
    end
    update_attributes(:complete=>true)
    []
  end

  def complete_date
    updated_at.strftime("%d.%m.%y") if complete?
  end

  def elaborate_statistics
    if complete?
      quiz.elaborate_statistics
      unless ENV["RAILS_ENV"] == "test"
        UserMail.complete_quiz_message(user).deliver
        UserMail.send_notification_to_admin(user, quiz).deliver
      end
    end
  end
end
# == Schema Information
#
# Table name: user_quizzes
#
#  id         :integer(4)      not null, primary key
#  user_id    :integer(4)      default(0), not null
#  quiz_id    :integer(4)      default(0), not null
#  created_at :datetime
#  updated_at :datetime
#  complete   :boolean(1)
#

