# -*- encoding : utf-8 -*-
class Project < ActiveRecord::Base
  has_many :users, :order => 'surname', :dependent=>:destroy
  has_many :notices
  has_many :project_quizzes, :dependent=>:destroy
  has_many :quizzes, :through=>:project_quizzes
  has_many :user_quizzes, :through=>:users
  has_many :sended_notices

  validates :title, :presence => {:message => "Заголовок опроса не может быть пустым"}

  before_destroy{|project| false if project.default?}

  accepts_nested_attributes_for :project_quizzes, :allow_destroy => true

  def to_s
    title
  end

  def show_report_for(quiz)
    project_quizzes.find_by_quiz_id(quiz.id).try(:show_report)
  end

  def find_report(quiz)
  	project_quizzes.find_by_quiz_id(quiz.id).try(:report)
  end

  def user_quizzes_by(quiz)
    user_quizzes.where(:quiz_id=>quiz)
  end

  def statistic
    people_count = users.size
    quizzes.map{|quiz| {:title=>quiz.title, :all=>user_quizzes_by(quiz).size,
      :completed=>user_quizzes_by(quiz).completed.size, :people => people_count}}
  end

  def self.default
    find_by_default(true)
  end

end

# == Schema Information
#
# Table name: projects
#
#  id           :integer(4)      not null, primary key
#  title        :string(255)     not null
#  created_at   :datetime
#  updated_at   :datetime
#  allow_anonim :boolean(1)
#  default      :boolean(1)      default(FALSE)
#  style        :text
#

