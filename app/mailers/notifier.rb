# -*- encoding : utf-8 -*-
class Notifier < ActionMailer::Base
  default :from => AppConfig.setting[:mail_robot]

  def invite(user, subject, content, template, project)
    # binding.pry
    @user = user
    @content = content
    @template = template
    @project = project
    mail(:to=>user.email, :content_type=> "text/html", :subject=> subject)
    SendedNotice.create(user: @user, project: @project, notice: @template, text: mail.body.to_s)
  end
  
  def invite_copy(user, subject, content)
    @user = user
    @content = content
    mail(:to=>user.email, :content_type=> "text/html", :subject=> subject)
  end

  def invite_copy_admin(email, subject, content)    
    @content = content
    mail(to: email, content_type: "text/html", subject: subject)
  end
end
