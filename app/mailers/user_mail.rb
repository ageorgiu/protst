# -*- encoding : utf-8 -*-
class UserMail < ActionMailer::Base
  default :from => AppConfig.setting[:mail_robot]

  def complete_quiz_message(user)
    @user = user
    @text = Settings.find_by_setting('report_will_be_email').try(:value) || ''
    mail(:to=> user.email, :subject=>"Вы прошли тест", :content_type => "text/html")
  end

  def send_notification_to_admin(user, quiz)
  	@user = user
  	@quiz = quiz
    @project_quiz = ProjectQuiz.find_by_quiz_id_and_project_id(@quiz.id, @user.project_id)
  	mail(:to=>AppConfig.setting[:admin_email], :subject=>"#{user.title} прошел тест", :content_type=>'text/html') if @project_quiz and @user and @quiz
  end

  def send_clear_notification_for(user, quiz)
    @user = user
    @quiz = quiz
    mail(:to=>AppConfig.setting[:admin_email], :subject=>"#{user.title} желает пройти '#{quiz.title}' заново", :content_type=>'text/html')
  end

  def register(user, subject, content)
    @content = content
    mail(:to=>user.email, :content_type=> "text/html", :subject=> subject)
  end
end
