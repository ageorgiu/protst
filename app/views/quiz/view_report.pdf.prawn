prawn_document do |pdf|
  pdf.font_families.update(
    "cyrillic" => {:normal => "#{Rails.root}/app/assets/fonts/font.ttf",
                  :bold   => "#{Rails.root}/app/assets/fonts/font-bold.ttf"}
  )
  pdf.font "cyrillic"
  pdf.text "#{@user.title}", :size=>20
  pdf.move_down 15
  pdf.text "#{@quiz.title}", :size=>20
  pdf.move_down 80
  width = 35
  height = 20
  left_margin = 200
  stens = []
  # факторы и нумерация
  pdf.draw_text 'Фактор', :at=>[0, pdf.y]
  1.upto 10 do |j|
    rx1 = j*width+left_margin-(((j%10)==0)? 10 : 5)
    pdf.draw_text j, :at=>[rx1, pdf.y]
  end
  pdf.move_down 20
  #grid & stens
  @quiz.factor_groups.each do |group|
    group.children.each do |factor|
      sten = factor.sten(@user).to_i
      stens << sten
      x1 = 5.5*width+left_margin
      x2 = sten*width+left_margin
      pdf.stroke_color = "0"*6
      pdf.line_width = 0.3
      1.upto 9 do |j|
        rx1 = j*width+left_margin
        ry1 = pdf.y + height/2
        pdf.fill_color = 'd'*6
        pdf.fill_rectangle [rx1, ry1], width, height
        pdf.fill_color '0'*6
        pdf.stroke_rectangle [rx1, ry1], width, height
      end
      pdf.stroke_color = "3"*6
      pdf.line_width = height*0.6
      pdf.stroke_line [x1,pdf.y,x2,pdf.y]
      pdf.draw_text factor.title, :at=>[0, pdf.y]
      pdf.draw_text sten, :at=>[left_margin, pdf.y]
      [4,5.5,7].each do |line|
        x1 = left_margin + line*width
        y1 = pdf.y + height/2
        y2 = pdf.y - height/2
        if line == 5.5
          pdf.line_width 3
          pdf.stroke_color "0"*6
        else
          pdf.line_width 1
          pdf.stroke_color 'cc0000'
        end
        pdf.stroke_line [x1,y1,x1,y2]
      end
      pdf.move_down height
    end
  end

  if ch = @quiz.get_profile_charachteristic(stens)
    pdf.start_new_page if pdf.y < 80
    pdf.text 'Характеристика вашего профиля', :size=>16, :style=>:bold
    pdf.move_down 5
    pdf.text ch, :size=>10, :indent_paragraphs => 20
    pdf.move_down 15
  end

  @quiz.factor_groups.each do |group|
    group.children.each do |factor|
      i = report.find_interpretation(factor.id, factor.sten(@user).to_i)
      if i.present?
        pdf.start_new_page if pdf.y < 80
        pdf.text factor.title, :size=>16, :style=>:bold
        pdf.move_down 5
        pdf.text i.content, :size=>10, :indent_paragraphs => 20
        pdf.move_down 15
      end
    end
  end
end
