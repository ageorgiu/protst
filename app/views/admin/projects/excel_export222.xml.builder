xml.instruct! :xml, :version=>"1.0", :encoding=>"UTF-8"
xml.Workbook({
  'xmlns'      => "urn:schemas-microsoft-com:office:spreadsheet",
  'xmlns:o'    => "urn:schemas-microsoft-com:office:office",
  'xmlns:x'    => "urn:schemas-microsoft-com:office:excel",
  'xmlns:html' => "http://www.w3.org/TR/REC-html40",
  'xmlns:ss'   => "urn:schemas-microsoft-com:office:spreadsheet"
  }) do

    xml.Styles do
      xml.Style 'ss:ID' => 'Default', 'ss:Name' => 'Normal' do
        xml.Alignment 'ss:Vertical' => 'Bottom'
        xml.Borders
        xml.Font 'ss:FontName' => 'Verdana'
        xml.Interior
        xml.NumberFormat
        xml.Protection
      end
      xml.Style 'ss:ID' => 's22' do
        xml.NumberFormat 'ss:Format' => 'General Date'
      end
    end

    #
    @quizzes.each do |quiz|
      xml.Worksheet 'ss:Name' => truncate(quiz.title, :length => 30) do
        xml.Table do
          # Header
          xml.Row do
            xml.Cell { xml.Data "Имя",                           'ss:Type' => 'String' }
            xml.Cell { xml.Data "Проект",                        'ss:Type' => 'String'}

            xml.Cell { xml.Data 'Пол',                           'ss:Type' => 'String'}
            xml.Cell { xml.Data 'Дата рождения',                 'ss:Type' => 'String'}


            xml.Cell { xml.Data 'Образование',                   'ss:Type' => 'String'}
            xml.Cell { xml.Data 'Тип образования',               'ss:Type' => 'String'}

            xml.Cell { xml.Data 'Уровень позиции',               'ss:Type' => 'String'}
            xml.Cell { xml.Data 'Сфера деятельности',            'ss:Type' => 'String'}
            xml.Cell { xml.Data 'Стаж работы',                   'ss:Type' => 'String'}

            xml.Cell { xml.Data 'Город',                         'ss:Type' => 'String'}
            xml.Cell { xml.Data 'email',                         'ss:Type' => 'String'}
            xml.Cell { xml.Data 'Контактный телефон',            'ss:Type' => 'String'}
            xml.Cell { xml.Data 'Дата прохождения теста',            'ss:Type' => 'String'}

            quiz.answerable_questions.each_with_index do |question,i|
              xml.Cell { xml.Data "#{i+1}.#{question.instruction}", 'ss:Type' => 'String' }
            end
          end
          quiz.completed_users.each_with_index do |u,idx|
            if @users.include?(u)
              xml.Row do
                xml.Cell { xml.Data u.title ,                                  'ss:Type' => 'String' }
                xml.Cell { xml.Data u.project.blank? ? '' : u.project.title ,  'ss:Type' => 'String' }

                xml.Cell { xml.Data u.gender ,             'ss:Type' => 'String' }
                xml.Cell { xml.Data u.birthday,            'ss:Type' => 'String' }

                xml.Cell { xml.Data u.education_level.to_s,     'ss:Type' => 'String' }
                xml.Cell { xml.Data u.education_type.to_s,      'ss:Type' => 'String' }

                xml.Cell { xml.Data u.job_position.to_s,        'ss:Type' => 'String' }
                xml.Cell { xml.Data u.job_area.to_s,            'ss:Type' => 'String' }
                xml.Cell { xml.Data u.job_expirience.to_s,      'ss:Type' => 'String' }

                xml.Cell { xml.Data u.city.to_s,                'ss:Type' => 'String' }
                xml.Cell { xml.Data u.email,               'ss:Type' => 'String' }
                xml.Cell { xml.Data u.phone,               'ss:Type' => 'String' }
                xml.Cell { xml.Data u.user_quizzes.find_by_quiz_id(quiz.id).complete_date, 'ss:Type' => 'String' }
                
                quiz.answers_for(u).each do  |a|
                  xml.Cell { xml.Data a.view ,  'ss:Type' => 'String' }
                end
              end
            end
          end
        end
      end
    end
  end

