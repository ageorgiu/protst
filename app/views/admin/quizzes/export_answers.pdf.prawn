# encoding: utf-8

prawn_document() do |pdf|
  pdf.font_families.update(
    "cyrillic" => {:normal => "#{Rails.root}/app/assets/fonts/font.ttf",
                  :bold   => "#{Rails.root}/app/assets/fonts/font-bold.ttf"}
  )
  pdf.font "cyrillic"
  pdf.text @user.title, :style=>:bold, :size=>30
  pdf.move_down(20)
  pdf.text @quiz.title, :style=>:bold, :size=>20
  pdf.move_down(15)
  content = [["Вопрос", "Ответ"]]
  if params[:values] == 'true'
    content[0] << "Бал"
    colums = {0=> 270, 1=>190, 2=>50}
  else
    colums = {0=> 300, 1=>215}
  end
  i = 0
  content += @quiz.answerable_questions.map do |question|
    if question.has_answer_for?(@user)
      array = ["#{i+=1}. #{question.instruction}", question.answer_for(@user).view]
      params[:values] == 'true' ? (array<<question.answer_for(@user).value) : array
    else
      array = [question.instruction, 'N/A']
      params[:values] == 'true' ? (array<<"") : array
    end
  end
  pdf.table content, :row_colors => ["FFFFFF","DDDDDD"],
    :header => true, :column_widths=>colums, :cell_style=>{:size=>10}
end