# encoding: utf-8

prawn_document do |pdf|

  pdf.font_families.update(
    "cyrillic" => {:normal => "#{Rails.root}/app/assets/fonts/font.ttf",
                  :bold   => "#{Rails.root}/app/assets/fonts/font-bold.ttf"})

  pdf.font "cyrillic"
  pdf.text @user.title, :style=>:bold, :size=>30
  pdf.move_down(20)
  pdf.text @quiz.title, :style=>:bold, :size=>20
  pdf.move_down(15)

  content = [["Вопрос", "Ответ"]]
  if params[:values]
    content[0] << "Бал"
    colums = {0=> 270, 1=>190, 2=>50}

  else
    colums = {0=> 300, 1=>215}
  end

  @answers.each_with_index do |answer, index|
    content << if params[:values]
      ["#{index+1}. #{answer.question.instruction}", answer.view, answer.value]
    else
      ["#{index+1}. #{answer.question.instruction}", answer.view]
    end
  end

  @unanswered_questions.each_with_index do |unanswered_question, index|
    content << if params[:values]
      ["#{@answers.size + index+1}. #{unanswered_question.instruction}", "Не был отвечен", "-"]
    else
      ["#{@answers.size + index+1}. #{unanswered_question.instruction}", "Не был отвечен"]
    end
  end

  pdf.table(content, :row_colors => ["FFFFFF","DDDDDD"], :header => true, :column_widths=>colums, :cell_style=>{:size=>10})
end
